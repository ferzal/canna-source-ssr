import { FC } from "react";
import classNames, { Argument } from "classnames";

import { Spinner } from "components/Spinner";

import styles from "./panel2.module.scss";

export interface Panel2Props {
  className?: Argument;
  loading?: boolean;
}

export const Panel2: FC<Panel2Props> = ({ children, className, loading }) => {
  return <div className={classNames(styles['panel2'], className)}>
    {loading ? <Spinner overlay /> : <>{children}</>}
  </div>;
};
