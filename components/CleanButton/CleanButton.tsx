import { FC } from "react";
import classNames, { Argument } from "classnames";

import styles from "./cleanButton.module.scss";

export interface CleanButtonProps
  extends React.DetailedHTMLProps<
  React.ButtonHTMLAttributes<HTMLButtonElement>,
  HTMLButtonElement
  > {
  icon: FC;
  color?: string;
}

export const CleanButton: FC<CleanButtonProps> = ({
  children,
  icon: Icon,
  onClick,
  color,
  disabled,
  className
}) => {
  return (
    <button
      className={classNames(
        styles["clean-button"],
        { [styles[color as string]]: color },
        className
      )}
      type="button"
      onClick={onClick}
      disabled={disabled}
    >
      <Icon />
      {children}
    </button>
  );
};
