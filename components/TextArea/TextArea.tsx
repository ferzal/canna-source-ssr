import React, { useState, useEffect, FC } from "react";

import classes from "utils/classNames";

import styles from "./textarea.module.scss";

export interface TextAreaProps
  extends React.InputHTMLAttributes<HTMLTextAreaElement> {
  id: string;
  fieldName?: string;
  value?: string;
  onChange?: React.ChangeEventHandler<HTMLTextAreaElement>;
  errors?: object;
  disabled?: boolean;
}

export const TextArea: FC<TextAreaProps> = ({
  id,
  fieldName,
  value,
  onChange,
  errors,
  disabled,
  className
}) => {
  const [filled, setFilled] = useState(value ? true : false);

  useEffect(() => {
    setFilled(value ? true : false);
  }, [value]);

  const onFocus = () => {
    setFilled(true);
  };
  const onBlur = () => {
    setFilled(value ? true : false);
  };

  const getError = () => {
    if(!errors) return false;

    type Iterator = keyof typeof errors;

    return errors[id as Iterator];
  };

  return (
    <div
      className={classes(
        styles.formfield,
        styles.textarea,
        filled && styles.filled,
        getError() && styles.error,
        className
      )}
    >
      <span className={styles["textarea__label-bg"]}>{fieldName}</span>
      <label htmlFor={id} className={styles.textarea__fieldname}>
        {fieldName}
      </label>

      <textarea
        id={id}
        name={id}
        onFocus={onFocus}
        onBlur={onBlur}
        onChange={onChange}
        value={value ? value : ""}
        disabled={disabled}
      />

      <span className="error">{getError()}</span>
    </div>
  );
};
