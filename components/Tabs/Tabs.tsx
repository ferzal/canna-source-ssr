import { FC, Children } from "react";

import classNames from "classnames";

import styles from "./tabs.module.scss";

interface TabsProps {
  tabs: string[];
  onChangeTab?: (tab: number) => void;
  selectedTabId?: number;
}

export const Tabs: FC<TabsProps> = ({
  children,
  tabs,
  onChangeTab,
  selectedTabId = 0,
}) => {
  const countChildren = Children.count(children);

  const renderContent = () => {
    if(countChildren === 1) return children;

    return Children.map(
      children,
      (child, i) =>
        +selectedTabId === i && <div className={styles.pane}>{child}</div>
    );
  };

  const renderTabs = () => {
    return tabs?.map((t, i) => {
      const handleClick = () => {
        if(!onChangeTab) return;
        onChangeTab(i);
      };

      return (
        <li
          key={i}
          onClick={handleClick}
          className={classNames({ [styles.active]: +selectedTabId === i })}
        >
          {t}
        </li>
      );
    });
  };

  return (
    <div className={styles.tabs}>
      <ul className={styles.nav}>{renderTabs()}</ul>
      <div className={styles.content}>{renderContent()}</div>
    </div>
  );
};
