import { FC } from "react";

import classNames, { Argument } from "classnames";

import styles from "./panel.module.scss";

export interface PanelProps {
  className?: Argument;
}

export const Panel: FC<PanelProps> = ({ children, className }) => {
  return <div className={classNames(styles["panel"], className)}>{children}</div>;
};
