import React, { FC } from "react";

import classes from "utils/classNames";

import styles from "./button.module.scss";

export interface ButtonProps
  extends React.DetailedHTMLProps<
    React.ButtonHTMLAttributes<HTMLButtonElement>,
    HTMLButtonElement
  > {
  width?: string;
  color?: string;
}

export const Button: FC<ButtonProps> = ({
  children,
  type,
  color,
  onClick,
  disabled,
  width,
  className,
}) => {
  return (
    <button
      className={classes(
        styles.button,
        color && styles[color],
        color,
        width && styles["width"],
        className
      )}
      type={type ? type : "button"}
      onClick={onClick}
      disabled={disabled}
    >
      {children}
    </button>
  );
};
