import React, { FC } from "react";
import classNames from "classnames";

import styles from "./switchingView.module.scss";
export interface SwitchingViewProps {
  type: string;
  setType: (type: string) => void;
}

export const SwitchingView: FC<SwitchingViewProps> = ({ type, setType }) => (
  <div className={styles["switching-view"]}>
    <button
      className={classNames(
        styles["switching-view__list"],
        { [styles.active]: type === "list" }
      )}
      onClick={() => setType("list")}
    ></button>
    <button
      className={classNames(
        styles["switching-view__grid"],
        { [styles.active]: type === "grid" }
      )}
      onClick={() => setType("grid")}
    ></button>
  </div>
);
