import React, { FC } from "react";

import styles from "./quantityChange.module.scss";
export interface QuantityChangeProps {
  quantity: number;
  setQuantity: (quantity: number) => void;
}

export const QuantityChange: FC<QuantityChangeProps> = ({
  quantity = 1,
  setQuantity,
}) => {
  return (
    <div className={styles["quantity-change"]}>
      <button
        type="button"
        onClick={() => setQuantity(quantity === 1 ? 1 : quantity - 1)}
        disabled={quantity === 1}
        className={styles['quantity-change__minus']}
      />
      <input
        type="text"
        name="quantity"
        value={quantity}
        onChange={(e) =>
          setQuantity(
            parseInt(e.target.value) > 1 ? parseInt(e.target.value) : 1
          )
        }
      />
      <button
        type="button"
        onClick={() => setQuantity(quantity + 1)}
        className={styles['quantity-change__plus']}
      />
    </div>
  );
};
