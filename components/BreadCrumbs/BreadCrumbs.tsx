import { FC, Fragment } from 'react';
import Link from 'next/link';

import DelimeterIcon from './delimeter.svg';
import styles from './breadCrumbs.module.scss';

export interface BreadCrumbsType {
	breadcrumbs: {
		link?: string;
		title: string;
	}[]
}

export const BreadCrumbs: FC<BreadCrumbsType> = ({ breadcrumbs }) =>
	<ul className={styles.breadcrumbs}>
		<li>
			<Link href="/" passHref><a className={styles['breadcrumbs__link']}>Home</a></Link>
		</li>

		{breadcrumbs && breadcrumbs.map((item, i) =>
			<Fragment key={i}>
				<li>
					<DelimeterIcon />
				</li>
				<li>
					{item.link
						? <Link href={item.link} passHref><a className={styles['breadcrumbs__link']}>{item.title}</a></Link>
						: <span className={styles['breadcrumbs__link']}>{item.title}</span>
					}
				</li>
			</Fragment>
		)}
	</ul>;