import { FC, useState, useEffect } from 'react';
import DatePicker from 'react-datepicker';
import InputMask from 'react-input-mask';
import classNames, { Argument } from "classnames";
import 'react-datepicker/dist/react-datepicker.css';

import { dateFormat } from 'utils/format';

import styles from './datefield.module.scss';

export interface DateFieldProps {
  id: string,
  fieldName: string,
  value: string,
  onChange: any,
  errors: {
    [key: string]: string;
  },
  disabled: boolean,
  mask: string
  className?: Argument;
}

export const DateField: FC<DateFieldProps> = ({ id, fieldName, value, onChange, errors, disabled, mask, className }) => {

  const [startDate, setStartDate] = useState<Date>(value ? new Date(value) : new Date());

  const [filled, setFilled] = useState(startDate ? true : false);

  const onFocus = () => {
    setFilled(true);
  };
  const onBlur = () => {
    setFilled(value ? true : false);
  };

  useEffect(() => {
    onChange(id, dateFormat(startDate));
  }, [startDate]);

  return (
    <div className={classNames(
      styles.formfield, styles.datefield,
      { [styles.filled]: filled },
      { [styles.error]: errors[id] },
      className
    )}>
      <DatePicker
        id={id}
        disabled={disabled}
        onFocus={onFocus}
        onBlur={onBlur}
        selected={startDate}
        onChange={(date: Date) => setStartDate(date)}
        className={styles.datefield}
        calendarClassName="datefield"
        customInput={<InputMask type="text" mask={mask} />}
      />
      <span className={styles["datefield__label-bg"]}>{fieldName}</span>
      <label
        htmlFor={id}
        className={styles.datefield__fieldname}
      >{fieldName}</label>
      <span className={styles.error}>{errors[id]}</span>
    </div>
  );
};