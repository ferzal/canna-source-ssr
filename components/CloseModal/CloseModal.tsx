import { FC } from 'react';

import { CloseIcon } from "assets/img/icons";

import styles from './close.module.scss';

interface CloseModalProps {
  onClick: () => void;
}

export const CloseModal: FC<CloseModalProps> = ({ onClick }) => {
  return (
    <button className={styles.close} onClick={onClick}>
      <CloseIcon />
    </button>
  );
};