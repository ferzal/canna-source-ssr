import { FC, useState, useEffect } from 'react';
import Select, { components, StylesConfig } from 'react-select';
import classNames, { Argument } from "classnames";

import ArrowDownIcon from 'assets/img/icons/ArrowDown.svg';
import { OptionType } from 'components/SelectFieldSmall';

import styles from './selectfield.module.scss';

export interface SelectFieldProps {
  id: string,
  type?: string,
  fieldName: string,
  value?: string,
  multiValue?: string[],
  options: OptionType[],
  isMulti?: boolean,
  onChange?: any,
  errors?: {
    [key: string]: string;
  };
  disabled?: boolean;
  className?: Argument;
}

export interface ColorOption {
  value: string;
  label: string;
  color: string;
  isFixed?: boolean;
  isDisabled?: boolean;
}

const colorStyles: StylesConfig<OptionType | null | true> = {
  option: (styles, { _, isFocused, isSelected }) => {
    return {
      ...styles,
      backgroundColor: isFocused || isSelected ? '#30303D' : '#212129',
      color: '#fff',
    };
  },
  singleValue: (styles) => ({ ...styles, color: '#fff' }),
};


export const SelectField: FC<SelectFieldProps> = ({ id, fieldName, value, multiValue, options, onChange, isMulti, errors, disabled, className }) => {
  const [filled, setFilled] = useState(value || (multiValue && multiValue.length) ? true : false);

  useEffect(() => {
    setFilled(value || (multiValue && multiValue.length) ? true : false);
  }, [multiValue, value]);

  const DropdownIndicator = (props: any) => {
    return (
      <components.DropdownIndicator {...props}>
        <ArrowDownIcon />
      </components.DropdownIndicator>
    );
  };

  const onMenuOpen = () => {
    setFilled(true);
  };

  const onMenuClose = () => {
    setFilled(value || (multiValue && multiValue.length) ? true : false);
  };

  let defaultValue;
  if(isMulti) {
    defaultValue = multiValue ? multiValue.map(v => options.find(i => i.value === v) || null) : [];
  } else {
    defaultValue = value ? options.find(i => i.value === value) : null;
  }
  return (
    <div className={classNames(
      styles.formfield, styles.selectfield,
      filled && styles.filled,
      disabled && styles.disabled,
      { [styles.error]: errors && errors[id] },
      className
    )}>

      {isMulti
        ? <Select
          instanceId={id}
          placeholder={''}
          isMulti={true}
          closeMenuOnSelect={isMulti ? false : true}
          menuPlacement="auto"
          isClearable
          components={{ DropdownIndicator }}
          className={styles.selectfield__container}
          classNamePrefix="react-select"
          defaultValue={defaultValue}
          value={defaultValue}
          onChange={selected => {
            onChange(id, selected ? selected.map((v: any) => v.value) : []);
          }}
          onMenuOpen={onMenuOpen}
          onMenuClose={onMenuClose}
          options={options}
          styles={colorStyles}
        />
        : <Select
          instanceId={id}
          placeholder={''}
          closeMenuOnSelect={isMulti ? false : true}
          menuPlacement="auto"
          isClearable
          components={{ DropdownIndicator }}
          className={styles.selectfield__container}
          classNamePrefix={styles["react-select"]}
          defaultValue={defaultValue}
          value={defaultValue}
          onChange={selected => {
            onChange(id, selected ? selected.value : '');
          }}
          onMenuOpen={onMenuOpen}
          onMenuClose={onMenuClose}
          options={options}
          styles={colorStyles}
        />
      }

      <span className={styles["selectfield__label-bg"]} >{fieldName}</span>
      <label
        htmlFor={id}
        className={styles.selectfield__fieldname}
      >{fieldName}</label>
      <span className={styles.error}>{errors && errors[id]}</span>
    </div>
  );
};
