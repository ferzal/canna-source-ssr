import { FC, DetailedHTMLProps, InputHTMLAttributes } from 'react';
import classes from 'utils/classNames';

import styles from './checkbox.module.scss';

export interface CheckboxProps extends DetailedHTMLProps<InputHTMLAttributes<HTMLInputElement>, HTMLInputElement> {
	id: string;
}

export const Checkbox: FC<CheckboxProps> = ({ className, title, id, value, onChange, disabled, checked }) =>
	<div className={classes(
		styles['formfield'],
		styles['checkbox'],
		className
	)}
	>
		<input
			type="checkbox"
			id={`checkbox_${id}`}
			name={id}
			value={value || 'on'}
			onChange={onChange}
			disabled={disabled}
			checked={checked}
		/>
		<label htmlFor={`checkbox_${id}`}>{title}</label>
	</div >;
