import { FC } from 'react';
import { useRouter } from "next/router";

import { CircleButton } from "components/CircleButton";
import { ArrowLeftIcon } from "assets/img/icons";

import styles from './goback.module.scss';

export const GoBack: FC = () => {
  const router = useRouter();
  return (
    <CircleButton
      icon={ArrowLeftIcon}
      onClick={() => router.back()}
      className={styles['goback']}
    >
      Go Back
    </CircleButton>
  );
};