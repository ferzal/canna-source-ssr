import { FC, useState, useEffect } from 'react';
import Select, { components } from 'react-select';

import classes from 'utils/classNames';

import ArrowDownIcon from 'assets/img/icons/ArrowDown.svg';
import styles from './selectfieldSmall.module.scss';

export interface OptionType {
  value: string,
  label: string
}

export interface SelectFieldSmallProps {
  id: string;
  width?: number;
  fieldName: string;
  value?: string;
  options: OptionType[];
  onChange: (id: string, select?: string) => void;
  errors?: {
    [key: string]: string;
  };
  disabled?: boolean;
  className?: string;
}

export const SelectFieldSmall: FC<SelectFieldSmallProps> = ({ id, width, fieldName, value, options, onChange, errors, disabled, className }) => {
  const [filled, setFilled] = useState(value ? true : false);

  useEffect(() => {
    setFilled(value ? true : false);
  }, [value]);

  const DropdownIndicator = props => {
    return (
      <components.DropdownIndicator {...props}>
        <ArrowDownIcon />
      </components.DropdownIndicator>
    );
  };

  const defaultValue = value ? options.find(i => i.value === value) : null;

  return (
    <div
      className={classes(
        styles['formfield'],
        styles['selectfieldsmall'],
        filled && styles['filled'],
        disabled && styles['disabled'],
        errors && errors[id] && styles['error'],
        className
      )}
      style={{
        width: width && `${width}px`
      }}
    >
      <label
        htmlFor={id}
        className={styles['selectfieldsmall__fieldname']}
      >
        {fieldName}
      </label>
      <Select
        instanceId={id}
        isClearable
        className={styles.selectfieldsmall__container}
        classNamePrefix={styles['react-select']}
        components={{ DropdownIndicator }}
        defaultValue={defaultValue}
        value={defaultValue}
        onChange={selected => onChange(id, selected ? selected.value : '')}
        options={options}
      />
      {errors && <span className={styles['error']}>{errors[id]}</span>}
    </div>
  );
};