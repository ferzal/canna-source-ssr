import { ChangeEventHandler, FC } from 'react';
import Slider from 'rc-slider';
import 'rc-slider/assets/index.css';

import styles from './rangeSlider.module.scss';

export interface ExtremeProps {
  max: number;
  min: number;
}

export interface RangeSliderProps {
  id: string;
  fieldName: string;
  value: number[];
  onChange: any;
  extreme: ExtremeProps;
  unitsBefore?: string;
  unitsAfter?: string;
}

//onChange: ChangeEventHandler<HTMLInputElement>;

export const RangeSlider: FC<RangeSliderProps> = ({ id, fieldName, value, onChange, extreme, unitsBefore = '', unitsAfter = '' }) => {
  const { createSliderWithTooltip } = Slider;
  const Range = createSliderWithTooltip(Slider.Range);

  const defaultValue = [
    value && value[0] || extreme?.min,
    value && value[1] || extreme?.max
  ];

  return (
    <div className={styles['rangeslider']}>
      <label
        htmlFor={id}
        className={styles['rangeslider__fieldname']}
      >
        {fieldName}{(unitsBefore || unitsAfter) && ','} {unitsBefore}{unitsAfter}
      </label>
      <Range
        className={styles['rc-slider']}
        min={extreme?.min || 0}
        max={extreme?.max || 200}
        defaultValue={defaultValue}
        tipFormatter={v => `${unitsBefore} ${v} ${unitsAfter}`}
        onAfterChange={v => onChange(id, v.join())}
      />
      <div className={styles['rangeslider__bottom']}>
        <div className={styles['rangeslider__input']}>
          {unitsBefore} <input
            type="text"
            value={value && value[0] || extreme?.min}
            onChange={e => onChange(id, [+e.target.value, value[1]].join())}
          /> {unitsAfter}
        </div>
        <div className={styles['rangeslider__input']}>
          {unitsBefore} <input
            type="text"
            value={value && value[1] || extreme?.max}
            onChange={e => onChange(id, [value[0], +e.target.value].join())}
          /> {unitsAfter}
        </div>
      </div>
    </div>
  );
};