import { ChangeEventHandler, FC } from 'react';

import classes from 'utils/classNames';

import styles from './switch.module.scss';

export interface SwitchFieldProps {
  id: string,
  value?: string,
  onChange: ChangeEventHandler<HTMLInputElement>,
  checked?: boolean,
  disabled?: boolean,
  className?: string
}

export const Switch: FC<SwitchFieldProps> = ({ id, value, onChange, checked, disabled, className }) => {
  return (
    <div className={classes(
      styles['switch'],
      className && styles[className]
    )}
    >
      <input
        type="checkbox"
        name={id}
        id={`checkbox_${id}`}
        value={value || 'on'}
        onChange={onChange}
        disabled={disabled}
        checked={checked}
      />
      <label htmlFor={`checkbox_${id}`}></label>
    </div>
  );
};