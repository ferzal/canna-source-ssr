import { FC } from 'react';

import { EmailIcon, PhoneIcon } from "assets/img/icons";
import { Button } from "components/Button";

import styles from './contactUs.module.scss';

export const ContactUs: FC = () => {
  return (
    <div className={styles.contact}>
      <a
        href="tel:+79160276336"
        target="_blank"
        rel="noreferrer"
        className={styles.contact__link}
      >
        <Button>
          <PhoneIcon /><br />
          +7(916) 027-63-36
        </Button>
      </a>
      <a
        href="mailto:sales@cannasourcedirect.com"
        target="_blank"
        rel="noreferrer"
        className={styles.contact__link}
      >
        <Button>
          <EmailIcon /><br />
          support@cannasourcedirect.com
        </Button>
      </a>
    </div>
  );
};