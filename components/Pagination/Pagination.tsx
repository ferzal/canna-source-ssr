import { FC } from 'react';
import ReactPaginate from 'react-paginate';

import styles from './pagination.module.scss';

interface PaginationProps {
  totalCount: number;
  currentPage: number;
  pageSize: number;
  setCurrentPage: any;
  onChangePageSize: any;
}

export const Pagination: FC<PaginationProps> = ({ totalCount, currentPage, pageSize, setCurrentPage, onChangePageSize }) => {
  const pageCount = Math.ceil(totalCount / pageSize);
  return (
    <div className={styles.paginate}>
      <ReactPaginate
        pageCount={pageCount}
        pageRangeDisplayed={5}
        marginPagesDisplayed={1}
        initialPage={currentPage - 1}
        onPageChange={page => setCurrentPage(page.selected + 1)}
        containerClassName={styles['paginate__list']}
        pageLinkClassName={styles['paginate__item-link']}
        activeLinkClassName={styles['active']}
        previousLinkClassName={styles['paginate__item-link']}
        nextLinkClassName={styles['paginate__item-link']}
        breakLinkClassName={styles['paginate__item-link']}
        previousLabel="<"
        nextLabel=">"
      />
      <div className={styles['paginate__count']}>
        <div className={styles['paginate__count-title']}>count per page</div>
        <select
          className={styles['paginate__count-select']}
          value={pageSize}
          onChange={onChangePageSize}
        >
          <option value={10}>10</option>
          <option value={30}>30</option>
          <option value={100}>100</option>
        </select>
      </div>
    </div>
  );
};