import React, { FC } from "react";
import classNames from "classnames";

import styles from "./spinner.module.scss";

export interface SpinnerProps {
  overlay?: boolean;
}

export const Spinner: FC<SpinnerProps> = ({ overlay }) => (
  <div className={
    classNames(
      styles.spinner,
      { [styles.overlay]: overlay }
    )}>
    <div className={styles["spinner__block"]}>
      <div className={styles["spinner__side"]}></div>
      <div className={styles["spinner__side"]}></div>
      <div className={styles["spinner__side"]}></div>
      <div className={styles["spinner__side"]}></div>
      <div className={styles["spinner__side"]}></div>
      <div className={styles["spinner__side"]}></div>
      <div className={styles["spinner__side"]}></div>
      <div className={styles["spinner__side"]}></div>
    </div>
  </div>
);
