import { ReactNode, FC } from 'react';

import classNames, { Argument } from "classnames";

import styles from './circleButton.module.scss';

export interface CircleButtonProps extends React.DetailedHTMLProps<React.ButtonHTMLAttributes<HTMLButtonElement>, HTMLButtonElement> {
  icon: FC;
}

export const CircleButton: FC<CircleButtonProps> = ({ icon: Icon, children, type, color, onClick, disabled, className }) => {
  return (
    <button
      className={classNames(
        styles['circle-button'],
        { [styles[color as string]]: color },
        className
      )}
      type={type}
      onClick={onClick}
      disabled={disabled}
    >
      <div className={styles["circle-button__icon"]}>
        <Icon />
      </div>
      {children}
    </button>
  );
};
