import React from 'react';

import styles from './passDescription.module.scss';

export const PassDescription = () => (
  <div className={styles["pass-descripton"]}>
    Password must contain at least 8 characters, 1 capital letter, 1 number and 1 special character.
  </div>
);
