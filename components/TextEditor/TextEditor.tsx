import { FC, useEffect, useRef } from 'react';
import classNames from "classnames";
import styles from './textEditor.module.scss';

export interface TextEditorProps {
  id: string;
  fieldName: string;
  value: string;
  onChange: any;
  disabled?: boolean;
  editorLoaded: boolean;
}

export const TextEditor: FC<TextEditorProps> = ({ id, fieldName, value, onChange, disabled, editorLoaded }) => {
  const editorRef = useRef();
  const { CKEditor, ClassicEditor } = editorRef.current || {};


  useEffect(() => {
    editorRef.current = {
      CKEditor: import("@ckeditor/ckeditor5-react").CKEditor, // v3+
      ClassicEditor: import("@ckeditor/ckeditor5-build-classic")
    };
  }, []);

  return (
    <div className={classNames(
      styles.formfield,
      styles['reach-text-editor'],
      { [styles.disabled]: disabled }
    )}>
      <p>{fieldName}</p>

      {editorLoaded &&
        <CKEditor
          editor={ClassicEditor}
          data={value}
          disabled={disabled}
          onChange={(_, editor) => {
            const data = editor.getData();
            onChange(id, data);
          }}
        />
      }


    </div>
  );
};