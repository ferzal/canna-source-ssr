import { FC } from 'react';

import classes from 'utils/classNames';

import styles from './radiofield.module.scss';

export interface RadioButtonProps extends React.DetailedHTMLProps<React.InputHTMLAttributes<HTMLInputElement>, HTMLInputElement> {
  radio: any[];
  id: string;
  errors?: {
    [key: string]: string;
  };
}

export const RadioField: FC<RadioButtonProps> = ({ id, radio, value, onChange, disabled, errors, className }) => {

  const renderRadio = () => {
    if(!radio) return;
    return radio.map(item => {
      const isChecked = value === item.value;
      return (
        <div key={item.value} className={styles['radiofield__item']}>
          <input
            type="radio"
            id={`radio_${item.value}`}
            name={id}
            value={item.value}
            onChange={onChange}
            disabled={disabled}
            checked={isChecked}
          />
          <label htmlFor={`radio_${item.value}`}>{item.label}</label>
        </div>
      );
    }
    );
  };

  return (
    <div className={classes(
      styles['formfield'],
      styles['radiofield'],
      disabled && styles['disabled'],
      errors && errors[id] && 'error',
      className
    )}>
      {renderRadio()}
    </div>
  );
};
