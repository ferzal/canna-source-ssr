import { FC } from "react";

import { ArrowLeftIcon } from "assets/img/icons";

import styles from "./MobileTitle.module.scss";
import { useRouter } from "next/router";

export interface MobileTitleProps {
  backRoute?: string;
}

export const MobileTitle: FC<MobileTitleProps> = ({ children, backRoute }) => {
  const router = useRouter();

  const handleBackButtonClick = () => {
    if (backRoute) {
      router.push(backRoute);
      return;
    }

    router.back();
  };

  return (
    <div className={styles.wrapper}>
      <div className={styles.back_button} onClick={handleBackButtonClick}>
        <ArrowLeftIcon />
      </div>
      <h1>{children}</h1>
    </div>
  );
};
