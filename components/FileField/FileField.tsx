import { ChangeEventHandler, FC } from 'react';

import classes from 'utils/classNames';

import styles from './filefield.module.scss';

export interface FileFieldProps {
  id: string,
  fieldName: string,
  file: File,
  onChange: (e: ChangeEventHandler<HTMLInputElement>) => void,
  errors: {
    [key: string]: string;
  };
  disabled: boolean;
}

export const FileField: FC<FileFieldProps> = ({ id, fieldName, file, onChange, errors }) => {
  return (
    <div className={classes(
      styles.formfield, styles.filefield,
      errors[id] && styles.error,
      file && styles.filled,
    )}>
      <label
        htmlFor={id}
        className={styles.filefield__label}
      >
        <span className={styles.filefield__filename}>
          {file ? file.name : fieldName}
        </span>
        <span className={styles.filefield__button}>
          Choose file
        </span>
      </label>
      <span className={styles["filefield__label-bg"]} >{fieldName}</span>
      <input
        id={id}
        name={id}
        type="file"
        onChange={onChange}
      />
      <span className={styles.error}>{errors[id]}</span>
    </div>
  );
};
