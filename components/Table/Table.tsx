import { ChangeEvent, FC } from 'react';

import classes from 'utils/classNames';
import { Checkbox } from 'components/Checkbox';

import styles from './table.module.scss';

export interface ColumnType {
  title: string;
  subtitle?: string;
  sortable?: boolean;
  sortBy?: string;
}

interface TableProps {
  columns: ColumnType[];
  selectedItems?: string[];
  onSelectAll?: (e: ChangeEvent<HTMLInputElement>) => void;
  sortBy?: string;
  sortDir?: string;
  onSort?: (sortBy: string) => void;
  columnsWidth: number[];
}

export const Table: FC<TableProps> = ({ children, columns, selectedItems, onSelectAll, sortBy, sortDir, onSort, columnsWidth }) => {

  const renderTitle = (v: ColumnType) => {
    if(v.sortable && onSort) {
      return <span onClick={() => onSort(v.sortBy)}>
        {v.title}{' '}
        {sortBy === v.sortBy && sortDir === 'DESC' && '↑'}
        {sortBy === v.sortBy && sortDir === 'ASC' && '↓'}
      </span>;
    }
    if(v.subtitle) return <>{v.title} <span className={styles.subtitle}>{v.subtitle}</span></>;

    return v.title;
  };

  const rendeCheckbox = (i: number) => {
    if(i === 0 && selectedItems && onSelectAll) return <Checkbox
      id="check_all"
      checked={selectedItems.length > 0}
      onChange={onSelectAll}
    />;
  };

  const renderColumn = (v: any, i: number) => <div
    key={v.title}
    className={classes(
      styles['th'],
      v.sortable && styles['sortable']
    )}
    style={{ width: `${columnsWidth[i]}%` }}
  >
    {rendeCheckbox(i)}

    {renderTitle(v)}

  </div>;

  return (

    <div className={styles['table']}>
      <div className={styles['tr']} >
        {columns.map((v: any, i: number) => renderColumn(v, i))}
      </div>

      {children}

    </div>
  );
};