import { FC, Children } from 'react';

import classes from 'utils/classNames';

import styles from './tableItem.module.scss';

interface TableItemProps {
	columnsWidth: number[];
	className?: string;
}

export const TableItem: FC<TableItemProps> = ({ children, columnsWidth, className }) => {

	const renderChildren = () => {
		return Children.map(children, (child, i) =>
			<div className={styles['td']} style={{ width: `${columnsWidth[i]}%` }}>{child}</div>
		);
	};

	return (
		<div className={classes(styles['tr'], className)}>
			{renderChildren()}
		</div>
	);
};
