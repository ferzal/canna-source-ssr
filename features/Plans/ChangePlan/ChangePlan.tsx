import { Button } from "components/Button";
import { FC } from 'react';

import { priceFormat } from "utils/format";

import styles from './changePlan.module.scss';

export interface ChangePlanProps {
  selectedPlan: any;
  setIsOpen: any;
}

export const ChangePlan: FC<ChangePlanProps> = ({ selectedPlan, setIsOpen }) => {
  return (
    <div className={styles['change-plan']}>
      <div className={styles['change-plan__title']}>Confirmation</div>
      {selectedPlan &&
        <>
          <div className={styles['change-plan__subtitle']}>Change to the {selectedPlan.title} plan?</div>
          <div className={styles['change-plan__subtitle']}>You will be billed ${priceFormat(selectedPlan.price)} monthly.</div>
        </>
      }
      <div className={styles['change-plan__description']}>By clicking confirm you assert that you are authorized to make the decision on behalf of your company. Your Subscription Plan is invoiced at the beginning of every month. Your plan will automatically renew monthly. To prevent renewal notify us by the 15th of the month to cancel for the subsequent month. Charges will appear as Canna Source Direct LLC on your statements.</div>

      <div className={styles['change-plan__bottom']}>
        <Button onClick={() => setIsOpen(false)}>Cancel</Button>
        <Button color="green" onClick={() => setIsOpen(false)}>Confirm</Button>
      </div>
    </div>
  );
};