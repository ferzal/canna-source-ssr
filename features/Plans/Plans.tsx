import { FC, useState, useEffect } from "react";
import { useSelector } from 'react-redux';

import { Spinner } from "components/Spinner";
import { Switch } from "components/Switch";
import { PLANS } from "utils/constants";
import { useModal } from 'utils/useModal';
import { ContactUs } from 'components/ContactUs';
import { AppState } from "redux/store";

import { Plan } from "./Plan";
import { ChangePlan } from "./ChangePlan";
import styles from './plans.module.scss';

export interface PlansProps {
  upgrade?: true;
}

export const Plans: FC<PlansProps> = ({ upgrade }) => {
  const user = useSelector<AppState>(state => state.user);
  const [loading, setLoading] = useState(true);
  const [billingYearly, setBillingYearly] = useState(false);
  const [selectedPlan, setSelectedPlan] = useState({});
  const [setModalContactOpened, ModalContact] = useModal();
  const [setModalChangePlanOpened, ModalChangePlan] = useModal();
  const [setModalAddCardOpened, ModalAddCard] = useModal();

  useEffect(() => {
    if(user) {
      setLoading(false);
    }
  }, [user]);

  const selectPlan = (id) => {
    setSelectedPlan(PLANS.find(plan => plan.id == id));
    if(upgrade) {
      setModalChangePlanOpened(true);
    } else {
      setModalAddCardOpened(true);
    }
  };

  if(loading) return <Spinner />;

  return (
    <>
      <div className={styles['plans__switch-billed']}>
        <div className={styles['plans__switch-label']}>
          Billed Monthly
        </div>
        <Switch
          id="billed"
          ckecked={billingYearly}
          onChange={e => setBillingYearly(e.target.checked)}
        />
        <div className={styles['plans__switch-label']}>
          Billed Yearly
          {billingYearly &&
            <div className={styles['plans__switch-saving']}>
              17% saving
            </div>
          }
        </div>
      </div>
      <div className={styles['plans__wrapper']}>
        {PLANS.map(plan => <Plan
          key={plan.id}
          billingYearly={billingYearly}
          user={user}
          plan={plan}
          selectPlan={selectPlan}
          openModalContact={() => setModalContactOpened(true)}
        />)}
      </div>
      <div className={styles['plans__bottom']}>

        <span className="link" onClick={() => setModalContactOpened(true)}>Contact Sales</span>

      </div>

      <ModalContact>
        <ContactUs />
      </ModalContact>

      <ModalChangePlan>
        <ChangePlan selectedPlan={selectedPlan} setIsOpen={setModalChangePlanOpened} />
      </ModalChangePlan>

      <ModalAddCard>
        {selectedPlan &&
          <p>Selected plan: {selectedPlan.title}</p>
        }
      </ModalAddCard>
    </>
  );
};
