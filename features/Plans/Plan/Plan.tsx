import { FC } from 'react';
import { Button } from "components/Button";
import { Panel } from "components/Panel";
import classNames, { Argument } from "classnames";

import { useModal } from 'utils/useModal';

import styles from './plan.module.scss';
import { priceFormat } from "utils/format";

export interface PlanProps {
  user: any;
  plan: any;
  billingYearly: any;
  upgrade?: any;
  selectPlan: any;
  className?: Argument;
  openModalContact?: any;
  openModalAddCard?: any;
}

export const Plan: FC<PlanProps> = ({ user, plan, billingYearly, upgrade, selectPlan, className, openModalContact, openModalAddCard }) => {

  const [openModal, Modal] = useModal();

  return (
    <Panel
      key={plan.id}
      className={classNames(
        styles.plan,
        { [styles.current]: user.plan && user.plan == plan.id },
        className
      )}
    >
      {user.plan && user.plan == plan.id &&
        <div className={styles['plan__current']}>
          Current Plan
        </div>
      }

      <div className={styles['plan__title']}>
        {plan.title}
      </div>
      <div className={styles['plan__price']}>
        {plan.price === 0
          ? 'Free'
          : <>
            {billingYearly
              ? <>
                <span className={styles['plan__price-old_price']}>${priceFormat(plan.price)}</span>
                <span className={styles['plan__price-new_price']}>${priceFormat(plan.priceYearly / 12)}</span>
              </>
              : <>$ {priceFormat(plan.price)}</>
            }
            <span className={styles['plan__price-period']}>/month</span>
          </>
        }
      </div>
      <div className={styles['plan__billed']}>
        {plan.price > 0 &&
          <>
            {billingYearly
              ? <>${priceFormat(plan.priceYearly)} billed yearly</>
              : <>Billed mountly</>
            }
          </>
        }
      </div>

      <div className={styles['plan__text']}>
        Included features:
      </div>

      <ul className={styles['plan__features']}>
        {plan.features.map(f => <li key={f}>{f}</li>)}
      </ul>

      {user.plan != plan.id &&
        <div className={styles['plan__bottom']}>
          {user.plan < plan.id && <Button
            color="green"
            onClick={() => selectPlan(plan.id)}
          >
            {upgrade ? 'Upgrade plan' : 'Select plan'}
          </Button>
          }

          <Button
            color="green"
            onClick={() => selectPlan(plan.id)}
          >
            {upgrade ? 'Upgrade plan' : 'Select plan'}
          </Button>


          {/* {user.plan > plan.id &&*/}
          <Button
            onClick={openModalContact}
          >
            Contact sales
          </Button>

          {/* }*/}
        </div>
      }

      <a href="#" onClick={openModalAddCard}>add card</a>
    </Panel>
  );
};