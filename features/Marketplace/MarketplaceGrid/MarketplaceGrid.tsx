import React, { FC } from "react";
import Link from "next/link";

import { CleanButton } from "components/CleanButton";
import { AddToCartIcon, EditIcon } from "assets/img/icons";

import { MarketplaceItem } from "../types";
import styles from "./marketplaceGrid.module.scss";
import NoPhotoIcon from "./assets/NoPhoto.svg";

export interface MarketplaceGridProps {
  item: MarketplaceItem;
  addToCard: (inventoryId: string, quantity: number) => void;
}

export const MarketplaceGrid: FC<MarketplaceGridProps> = ({
  item,
  addToCard,
}) => (
  <div data-id={item.id} className={styles["marketplace-grid"]}>
    {item.my && (
      <Link href={`/inventory/${item.id}`} passHref>
        <a className={styles["marketplace-grid__edit"]}>
          <EditIcon />
        </a>
      </Link>
    )}
    <Link href={`/marketplace/${item.id}`} passHref>
      <a>
        <div className={styles["marketplace-grid__image"]}>
          {!item.media && (
            <span className={styles["marketplace-grid__nophoto"]}>
              <NoPhotoIcon /> No Photo
            </span>
          )}
          {item.media &&
            item.media
              .slice(0, 1)
              .map((file) => (
                <img key={file.previewBig} src={file.previewBig} alt="image" />
              ))}
        </div>
        <div className={styles["marketplace-grid__name"]}>
          {item.marketplaceName || item.name}
          <br />
          <span className={styles.small_text}>{item.category}</span>
        </div>
        <div className={styles["marketplace-grid__characteristics"]}>
          <ul>
            <li>
              <span>THC: {item.thc} %</span>
            </li>
            {item.growType && (
              <li>
                <span>Flower Growing Operation: {item.growType}</span>
              </li>
            )}
            {item.sizeCategory && (
              <li>
                <span>Size Category: {item.sizeCategory}</span>
              </li>
            )}
          </ul>
        </div>
        {item.showQuantity && (
          <div className={styles["marketplace-grid__units"]}>
            {item.quantity} {item.unitOfMeasureName} Available
          </div>
        )}
      </a>
    </Link>
    <div className={styles["marketplace-grid__bottom"]}>
      <div className={styles["marketplace-grid__price"]}>
        {item.oldPrice ? (
          <>
            <span className={styles.old_price}>$ {item.oldPrice}</span>${" "}
            {item.price}
          </>
        ) : (
          <>$ {item.price}</>
        )}{" "}
        {item.negotiable && <>Negotiable</>}
      </div>

      <CleanButton
        icon={AddToCartIcon}
        className={styles["marketplace-grid__card"]}
        onClick={() => addToCard(item.id, 1)}
      />
    </div>
  </div>
);
