import { FC } from "react";
import Link from "next/link";
import classNames from 'classnames';


import {
  AddToCartIcon,
  EditIcon,
  Img2Icon,
  NoPhotoIcon,
} from "assets/img/icons";
import { CleanButton } from "components/CleanButton";

import { MarketplaceItem } from "../types";
import styles from "./marketplaceList.module.scss";

export interface MarketplaceListProps {
  item: MarketplaceItem;
  addToCard: (inventoryId: string, quantity: number) => void;
}

export const MarketplaceList: FC<MarketplaceListProps> = ({
  item,
  addToCard,
}) => {
  return (
    <div data-id={item.id} className={styles["marketplace-row"]}>
      <Link href={`marketplace/${item.id}`} passHref>
        <a className={styles["marketplace-row__images"]}>
          {!item.media && (
            <span
              className={styles["marketplace-row__nophoto"]}
            >
              <NoPhotoIcon />
              No Photo
            </span>
          )}
          {item.media &&
            item.media.slice(0, 3).map((file, i) => (
              <div
                key={file.preview}
                className={classNames(
                  styles["marketplace-row__image"],
                  { [styles.more]: item.media.length > 3 && i == 2 }
                )}
              >
                <img src={file.preview} alt="image" />
                {item.media.length > 3 && i == 2 && (
                  <span>
                    +{item.media.length - 3} <Img2Icon />
                  </span>
                )}
              </div>
            ))}
        </a>
      </Link>
      <div className={styles["marketplace-row__names"]}>
        <Link href={`marketplace/${item.id}`} passHref>
          <a>
            {item.marketplaceName || item.name}
            <br />
            <span className={styles.small_text}>{item.category}</span>
          </a>
        </Link>
      </div>
      {
        item.showQuantity && (
          <div className={styles["marketplace-row__units"]}>
            <Link href={`marketplace/${item.id}`} passHref>
              <span>
                {item.quantity} {item.unitOfMeasureName} Available
              </span>
            </Link>
          </div>
        )
      }
      <div className={styles["marketplace-row__characteristics"]}>
        <Link href={`marketplace/${item.id}`} passHref>
          <a>
            <span>THC: {item.thc}%</span>
            {item.growType && (
              <span>Flower Growing Operation: {item.growType}</span>
            )}
            {item.sizeCategory && (
              <span>Size Category: {item.sizeCategory}</span>
            )}
          </a>
        </Link>
      </div>
      <div className={styles["marketplace-row__price"]}>
        {item.oldPrice ? (
          <>
            <span className={styles.old_price}>$ {item.oldPrice}</span>${" "}
            {item.price}
          </>
        ) : (
          <>$ {item.price}</>
        )}{" "}
        {item.negotiable && (
          <>
            <br />
            Negotiable
          </>
        )}
      </div>
      <div className={styles["marketplace-row__actions"]}>
        <CleanButton
          icon={AddToCartIcon}
          className={styles["marketplace-row__card"]}
          onClick={() => addToCard(item.id, 1)}
        />
        {item.my && (
          <Link href={`/inventory/${item.id}`} passHref>
            <a className={styles["marketplace-row__edit"]}>
              <EditIcon />
            </a>
          </Link>
        )}
      </div>
    </div >
  );
};
