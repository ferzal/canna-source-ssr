import { MediaProps } from "reactstrap/es/Media";

export type MarketplaceItem = {
  id: string;
  media: MediaProps[];
  name: string;
  marketplaceName: string;
  showQuantity: boolean;
  quantity: number;
  unitOfMeasureName: string;
  category: string;
  growType: string;
  thc: number;
  sizeCategory: string;
  oldPrice: number;
  price: number;
  negotiable: boolean;
  my?: boolean;
};
