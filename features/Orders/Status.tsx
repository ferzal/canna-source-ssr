import { FC } from 'react';

export const ORDER_STATUS = [
  { id: 1, title: 'New' },
  { id: 2, title: 'Accepted' },
  { id: 3, title: 'Paid' },
  { id: 4, title: 'Delivered' }
];

export interface StatusProps {
  status: number;
}

export const Status: FC<StatusProps> = ({ status }) => {
  const s = ORDER_STATUS.find(s => s.id == status);
  return <>
    {s && s.title}
  </>;
};