import { Avatar, AvatarProps } from "features/Messages/Avatar";
import { FC } from "react";

import styles from "./UserCard.module.scss";

export interface UserCardProps extends AvatarProps {
  name: string;
  info: string;
}

export const UserCard: FC<UserCardProps> = ({ src, online, name, info }) => {
  return (
    <div className={styles.wrapper}>
      {src &&
        <Avatar src={src} online={online} />
      }
      <div className={styles.content}>
        <div className={styles.name}>{name}</div>
        <div className={styles.message}>{info}</div>
      </div>
    </div>
  );
};
