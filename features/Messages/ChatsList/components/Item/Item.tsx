import { FC } from "react";
import cn from "classnames";
import Image from "next/image";

import { UserCardProps, UserCard } from "features/Messages/UserCard";

import styles from "./Item.module.scss";
import { getMessageTime } from "utils/getMessageTime";
import { dateTimeFormat } from "utils/format";

export interface ItemProps extends UserCardProps {
  active?: boolean;
  id: string;
  avatar: string;
  name: string;
  lastMessage: string;
  lastDate: string;
  onSelect?: (id: string) => void;
}

export const Item: FC<ItemProps> = ({
  active,
  avatar,
  online,
  name,
  lastMessage,
  lastDate,
  id,
  onSelect,
}) => {
  const selectItem = () => {
    if(onSelect) onSelect(id);
  };

  const lastMessageTime = getMessageTime(lastDate);

  return (
    <div
      className={cn(styles.wrapper, { [styles.active]: active })}
      onClick={selectItem}
    >
      <UserCard src={avatar} online={online} name={name} info={lastMessage} />
      <div className={styles.date}>{lastMessageTime}</div>
    </div>
  );
};
