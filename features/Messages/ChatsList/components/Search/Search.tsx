import { Button } from "components/Button";
import { TextField } from "components/TextField";
import Add from "assets/img/icons/Add.svg";
import { SearchIcon } from "assets/img/icons";

import styles from "./Search.module.scss";

export const Search = () => {
  return (
    <div className={styles.wrapper}>
      <TextField
        Icon={SearchIcon}
        id="search"
        type="text"
        fieldName="Search..."
        // value={search}
        // onChange={searchHandler}
      />
      <Button color={"green"}>
        <Add />
      </Button>
    </div>
  );
};
