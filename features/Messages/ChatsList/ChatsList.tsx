import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import { useDispatch, useSelector } from "react-redux";

import { getChats } from "services/api";
import { messagesSetChatId } from "redux/actions/messagesActions";
import { AppState } from "redux/store";

import styles from "./ChatsList.module.scss";
import { Search } from "./components/Search";
import { Item } from "./components/Item";


export const ChatsList = () => {
  const [chats, setChats] = useState([]);
  const chatId = useSelector<AppState>((state) => state.messages && state.messages.chatId);
  const dispatch = useDispatch();
  const router = useRouter();

  const setChatId = (chatId: string) => {
    router.push(chatId ? `/messages?chatId=${chatId}` : '/messages');
    dispatch(messagesSetChatId(chatId));
  };

  useEffect(() => {
    getChats()
      .then((res: any) => {
        setChats(res);
      })
      .catch(e => console.error(e));
  }, [chatId]);

  const renderChats = chats.map((chat: any) => <Item key={chat.id} {...chat} active={chat.id === chatId} onSelect={() => setChatId(chat.id)} />);

  return (
    <div className={styles.wrapper}>
      {/* <Search /> */}
      {/* <Tabs tabs={["General Info", "Media"]}>
        <div>123</div>
        <div>321</div>
      </Tabs> */}
      <div className={styles.items}>{renderChats}</div>
    </div>
  );
};
