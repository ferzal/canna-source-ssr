import { FC } from "react";
import { useRouter } from "next/router";

import { UserCard } from "features/Messages/UserCard";
import Multi from "assets/img/icons/Multi.svg";
import Favorite from "assets/img/icons/Favorite.svg";
import AddUser from "assets/img/icons/AddUser.svg";

import styles from "./Header.module.scss";

export interface HeaderProps {
  name: string,
  avatar: string
}

export const Header: FC<HeaderProps> = ({ name, avatar }) => {

  const isOnline = true;

  return (
    <div className={styles.header}>
      <UserCard
        src={avatar}
        name={name}
        online={isOnline}
        info={isOnline ? "online" : "offline"}
      />
      {/* <div className={styles.icons}>
        <AddUser />
        <Favorite />
        <Multi />
      </div> */}
    </div>
  );
};
