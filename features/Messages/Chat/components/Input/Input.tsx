import { FC, ChangeEvent, useState, EventHandler } from "react";

import { TextField } from "components/TextField";
import { Button } from "components/Button";
import Attach from "assets/img/icons/Attach.svg";
//import Smile from "assets/img/icons/Smile.svg";
import Send from "assets/img/icons/Send.svg";
import { getUrlForUpload, putFileOnAmazon } from "services/api";
import { fileSizeType } from "utils/fileSizeType";

import styles from "./Input.module.scss";
import { Spinner } from "components/Spinner";


export interface AttachedType {
  id?: string,
  name: string,
  file: string,
  size: number,
  mime: string
}
export interface InputProps {
  message: string,
  handleChange: (e: ChangeEvent<HTMLInputElement>) => void,
  sendMessage: () => void,
  lockForm: boolean,
  attached: AttachedType[],
  setAttached: (attached: any) => void,
  files: File[],
  setFiles: (files: File[]) => void
}

export const Input: FC<InputProps> = ({ message, handleChange, sendMessage, lockForm, setAttached, attached, files, setFiles }) => {

  const [loading, setLoading] = useState(0);
  const [fileError, setFileError] = useState('');

  const uploadFiles = (files: any) => {
    const resFiles = files.filter((file: File) => file.size < 5000000);
    setLoading(resFiles.length);
    setFiles(resFiles);
    files.forEach((file: File, i: number) => {
      if(file.size > 5000000) {
        setFileError('File must be less than 5 MB');
        return;
      }

      let fileKey: string;
      getUrlForUpload({
        contentType: file.type,
        fileName: file.name
      })
        .then((res: any) => {
          fileKey = res.key;
          return putFileOnAmazon({ url: res.uploadUrl, file });
        })
        .then((res) => {
          console.log(res);
          setAttached((prev: any) => [...prev, { file: fileKey, name: file.name, mime: file.type, size: file.size }]);
        })
        .catch(e => {
          console.error(e);
        })
        .finally(() => {
          setLoading(prev => prev - 1);
        });
    });
  };

  const handleUpload = (e: ChangeEvent<HTMLInputElement>) => {
    const fileList: FileList = e.target.files;
    uploadFiles([...fileList]);
  };

  const renderAttachFiles = () => {
    return files.map((file: File) => <div key={file.name}>{file.name} {fileSizeType(file.size)}</div>);
  };

  return (
    <>
      {loading > 0 &&
        <Spinner overlay />
      }

      <div className={styles.wrapper}>

        <label className={styles.label}>
          <input
            type="file"
            multiple
            id="uploadFiles"
            name="uploadFiles"
            onChange={handleUpload}
          />
          <Attach />
        </label>

        <TextField
          id="message"
          className={styles.input}
          placeholder="Your message..."
          value={message}
          onChange={handleChange}
          disabled={lockForm}
        />
        {/* <Smile /> */}
        <Button
          color="green"
          className={styles.send}
          onClick={sendMessage}
          disabled={lockForm || !message}
        >
          <Send />
        </Button>
      </div>

      {renderAttachFiles()}

      <span className="error">{fileError}</span>
    </>
  );
};
