import { FC } from "react";
import cn from "classnames";
import Image from "next/image";

import { getMessageTime } from "utils/getMessageTime";
import { fileSizeType } from "utils/fileSizeType";

import styles from "./Message.module.scss";
import { AttachedType } from "../Input";

export interface MessageProps {
  messageFiles: AttachedType[];
  message: string;
  name: string;
  date: string;
  avatar?: string;
  isMine?: boolean;
}

export const Message: FC<MessageProps> = ({
  messageFiles,
  message,
  name,
  date,
  avatar,
  isMine
}) => {

  const messageTime = getMessageTime(date);

  const renderAvatar = () => {
    if(!avatar || isMine) return;

    return (
      <div className={styles.avatar}>
        <Image src={avatar} alt="" width="32" height="32" layout="responsive" />
      </div>
    );
  };

  const minFileName = (name: string) => {
    if(name.length < 15) return name;
    return `${name.substr(0, 8)}...${name.substr(-8)}`;
  };

  const renderAttached = () => {
    return messageFiles.map((file: AttachedType) => <div key={file.id}><a href={file.file} target="_blank" rel="noreferrer">
      {minFileName(file.name)}</a> {fileSizeType(file.size)}
    </div>);
  };

  return (
    <div className={cn(styles.wrapper, { [styles.mine]: isMine })}>
      {renderAvatar()}
      <div className={cn(styles.container, { [styles.mine]: isMine })}>
        <div className={styles.content}>
          <div className={styles.name}>{name}</div>
          <div className={styles.message}>{message}</div>
        </div>
        <div className={styles.date}>{messageTime}</div>
        <div className={styles.attached}>
          {renderAttached()}
        </div>
      </div>


    </div>
  );
};
