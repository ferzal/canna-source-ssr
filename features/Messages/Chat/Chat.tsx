import { useEffect, useState, ChangeEvent, useRef, RefObject } from "react";
import { useRouter } from "next/router";
import { useSelector } from "react-redux";

import { Spinner } from "components/Spinner";
import { addMessage, getMessages } from "services/api";
import { AppState } from "redux/store";

import { Header } from "./components/Header";
import { Message } from "./components/Message";
import { AttachedType, Input } from "./components/Input";
import styles from "./Chat.module.scss";


export const Chat = () => {
  const chatContainer: RefObject<HTMLDivElement> = useRef(null);
  const chatId = useSelector<AppState>((state) => state.messages && state.messages.chatId);
  const [loading, setLoading] = useState(false);
  const [messages, setMessages] = useState([]);
  const [user, setUser] = useState({ name: '', avatar: '' });
  const [message, setMessage] = useState<string>("");
  const [files, setFiles] = useState([]);
  const [attached, setAttached] = useState<AttachedType[]>([]);
  const [lockForm, setLockForm] = useState<boolean>(false);

  const router = useRouter();

  const getAndSetMessages = () => {
    getMessages(chatId)
      .then((res: any) => {
        setMessages(res.messages);
        setUser(res.user);
        setMessage('');
        setAttached([]);
        setFiles([]);
        scrollToMyRef();
      })
      .catch(e => console.error(e))
      .finally(() => {
        setLoading(false);
        setLockForm(false);
      });
  };

  useEffect(() => {
    if(!chatId) return;
    setLoading(true);
    getAndSetMessages();
  }, [chatId]);

  const handleChangeMessage = (e: ChangeEvent<HTMLInputElement>) => {
    setMessage(e.currentTarget.value);
  };

  const sendMessage = () => {
    setLockForm(true);
    if(message.length < 1) return;
    addMessage({ chatId: router.query.chatId, message, attached })
      .then((res: any) => getAndSetMessages())
      .catch(e => console.error(e))
      .finally(() => {

      });

  };

  const scrollToMyRef = () => {
    const scroll = chatContainer.current!.scrollHeight - chatContainer.current!.clientHeight;
    chatContainer.current!.scrollTo(0, scroll);
  };

  const renderMessages = messages.map((message: any) => (
    <Message {...message} key={message.id} />
  ));

  return (
    <div className={styles.wrapper}>
      {loading && <Spinner overlay />}
      <Header {...user} />
      <div className={styles.messages} ref={chatContainer}>{renderMessages}</div>
      <Input
        message={message}
        attached={attached}
        setAttached={setAttached}
        handleChange={handleChangeMessage}
        sendMessage={sendMessage}
        lockForm={lockForm}
        files={files}
        setFiles={setFiles}
      />
    </div>
  );
};
