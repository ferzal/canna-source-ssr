import { FC } from "react";
import Image from "next/image";
import cn from "classnames";

import styles from "./Avatar.module.scss";


export interface AvatarProps {
  src: string;
  online?: boolean;
  size?: number | string;
}

export const Avatar: FC<AvatarProps> = ({ src, online, size = 48 }) => (
  <div className={styles.avatar}>
    <Image src={src} alt="" width={size} height={size} layout="fixed" />
    <div className={cn(styles.status, { [styles.online]: online })} />
  </div>
);
