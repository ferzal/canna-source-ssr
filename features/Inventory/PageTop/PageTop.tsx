import { FC } from 'react';

import styles from './pagetop.module.scss';

export const PageTop: FC = () => {
  return (
    <div className={styles['page-top']}>
      <div className={styles['page-top__left']}>
        <div>
          <h1>Inventory</h1>
          <p className="subtitle">Add your products to the marketplace</p>
        </div>
        {/* {showAlertSuccesSync &&
          <AlertSmall
            title="Your inventory has successfully synced"
            onHide={onHideAlert}
          />
        } */}
      </div>
      <div className={styles['page-top__right']}>
        <div className={styles['page-top__right-update']}>
          <div>
            Full Update from Metrc
            {/* <span className="inventory__block-update-last">Last update at: {dateTimeFormat(user.metrcUpdatedAt)}</span> */}
          </div>
          <div>
            {/* <Button
              type="submit"
              width="width"
              disabled={processUpdate}
              onClick={updateHandler}
            >
              Update
            </Button> */}
          </div>
          <div>
            {/* {processUpdate && <p>Update inventory in progress. Complete: {processStatus}% Please wait!</p>} */}
          </div>
        </div>
      </div>
    </div>
  );
};