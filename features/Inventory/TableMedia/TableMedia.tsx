import { FC, useState } from 'react';
import 'react-image-lightbox/style.css';
import Lightbox from 'react-image-lightbox';

//import useModalAddImage from '../../hooks/useModalAddImage';
import { AddMediaIcon } from 'assets/img/icons';
import classes from 'utils/classNames';

import styles from './tableMedia.module.scss';

interface TableMediaProps {
	id: string;
	media: any;
	onSavedMedia: any;
	classNameLink: string;
}

export const TableMedia: FC<TableMediaProps> = ({ id, media, onSavedMedia, classNameLink }) => {
	//	const {openModal, ModalAddImage} = useModalAddImage(onSavedMedia);

	const openModal = (e, id) => {

	};

	const [isOpen, setIsOpen] = useState(false);
	const [photoIndex, setPhotoIndex] = useState(0);

	const onOpen = i => {
		setPhotoIndex(i);
		setIsOpen(true);
	};

	return (
		<div className={styles['table-media']}>
			<div className={styles['table-media__left']}>

				<a href="#" className={classes(styles['table-media__link'], classNameLink)} onClick={e => openModal(e, id)}>
					<AddMediaIcon />
				</a>

				{media && media.slice(0, 3).map((file, i) =>
					<div key={file.preview} className={styles['table-media__image']} onClick={() => onOpen(i)}>
						<img src={file.preview} alt="image" />
					</div>
				)}

				{media && media.length > 3 &&
					<div className={styles['table-media__image']}>
						+ {media.length - 3}
					</div>
				}

			</div>
			{/* <ModalAddImage /> */}
			{isOpen && (
				<Lightbox
					mainSrc={media[photoIndex].file}
					nextSrc={media[(photoIndex + 1) % media.length].file}
					prevSrc={media[(photoIndex + media.length - 1) % media.length].file}
					onCloseRequest={() => setIsOpen(false)}
					onMovePrevRequest={() => setPhotoIndex((photoIndex + media.length - 1) % media.length)}
					onMoveNextRequest={() => setPhotoIndex((photoIndex + 1) % media.length)}
				/>
			)}
		</div>
	);
};