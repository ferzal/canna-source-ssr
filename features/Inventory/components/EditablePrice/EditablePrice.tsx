import { FC, useState } from 'react';

import { setInventoryPrice } from 'services/api';
import classes from 'utils/classNames';
import { priceFormat } from 'utils/format';

import styles from './editablePrice.module.scss';

interface EditablePriceProps {
  id: string;
  value: string;
}

export const EditablePrice: FC<EditablePriceProps> = ({ id, value }) => {
  const [edit, setEdit] = useState(false);
  const [load, setLoad] = useState(false);
  const [newPrice, setNewPrice] = useState(value);

  const saveData = () => {
    if(newPrice != value) {
      setLoad(true);
      setInventoryPrice({ id, price: newPrice })
        .then(() => {
        })
        .catch(e => {
          console.error(e);
        })
        .finally(() => {
          setLoad(false);
        });
    }
  };

  const onKeyPress = (e: any) => {
    if(e && e.charCode === 13) {
      setEdit(false);
      saveData();
    }
  };

  const onBlur = () => {
    setEdit(false);
    saveData();
  };

  return (
    <>
      {edit &&
        <input
          className={classes(
            styles['edit-price'],
            load && styles['load']
          )}
          maxLength={6}
          type="text"
          id={`price_${id}`}
          name={`price_${id}`}
          value={newPrice}
          onChange={e => setNewPrice(e.target.value)}
          onBlur={onBlur}
          autoFocus
          onKeyPress={onKeyPress}
        />
      }
      <span
        className={classes(
          styles['edit-price'],
          load && styles['load']
        )}
        hidden={edit}
        onClick={() => setEdit(prev => !prev)}
      >
        $ {' '}{newPrice ? priceFormat(newPrice) : '0.00'}
      </span>
    </>
  );
};

