import { FC, useState, useEffect, ChangeEvent, ChangeEventHandler } from 'react';

import { FileIcon, PdfIcon } from "assets/img/icons";
import { getUrlForUpload, putFileOnAmazon, setInventoryUpdate } from "services/api";

export interface CoaInfoProps {
  fields: any;
  setLoading: any;
  setLockForm: any;
  styles: any;
}

export const CoaInfo: FC<CoaInfoProps> = ({ fields, setLoading, setLockForm, styles }) => {

  const fileInputHandler = e => {
    e.preventDefault();
    if(!e.target.files[0]) return false;
    setLoading(true);
    const onChange = async (file) => {
      let fileName;
      getUrlForUpload({
        contentType: file.type,
        fileName: file.name
      })
        .then(res => {
          fileName = res.fileName;
          return putFileOnAmazon({ url: res.uploadUrl, file });
        })
        .then(() => {
          setInventoryUpdate({
            'id': fields.id,
            'coa': fileName
          })
            .then(() => {
              setLockForm(false);
              setLoading(false);
            });
        })
        .then(() => {
          setLoading(false);
        });

    };
    onChange(e.target.files[0]);
  };


  return (
    <div className={styles['input-file']}>
      <input type="file" id="licenseFile" onChange={fileInputHandler} />
      <div className={styles['input-file__title']}>Сertificate of analysis (COA)</div>
      <label htmlFor={styles['licenseFile']}>
        <div className={styles['input-file__text']}>
          <div className={styles['input-file__img']}>
            {fields.coa ? <PdfIcon /> : <FileIcon />}
            <div className={styles['input-file__name']}>
              {fields.coa &&
                <a
                  href={fields.coa}
                  target="_blank"
                  rel="noreferrer"
                >
                  {fields.coa.split('/')[4]}
                </a>
              }
            </div>
          </div>
          Choose file COA
        </div>
      </label>
    </div>
  );
};