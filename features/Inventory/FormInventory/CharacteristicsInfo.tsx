import { RadioField } from "components/RadioField";
import { TextField } from "components/TextField";
import { FC, useState, useEffect, ChangeEvent } from 'react';
import { STRAIN_CLASSIFICATIONS } from "utils/constants";


export interface CharacteristicsInfoProps {
  fields: any;
  errors: any;
  lockForm: boolean;
  changeHandler: (e: ChangeEvent) => void;
  styles: any;
}

export const CharacteristicsInfo: FC<CharacteristicsInfoProps> = ({ fields, errors, lockForm, changeHandler, styles }) => {
  return (
    <>
      <div className={styles['form-inventory__title']}>
        Characteristics
      </div>

      <div className="row">
        <div className="col3">
          <TextField
            id="thc"
            type="text"
            fieldName="THC, %"
            value={String(fields['thc'])}
            errors={errors}
            onChange={changeHandler}
            disabled={lockForm}
            className={styles.formfield}
          />
        </div>
      </div>

      {/* <div className={styles['form-inventory__title']}>
        Units Of Measure
      </div>
      <div className="row">
        <div className="col3">
          <TextField
            id="thc"
            type="text"
            fieldName="1"
            value=""
            errors={errors}
            onChange={changeHandler}
            disabled={lockForm}
            className={styles.formfield}
          />
        </div>
        <div className="col3">
          <TextField
            id="thc"
            type="text"
            fieldName="Unit"
            value=""
            errors={errors}
            onChange={changeHandler}
            disabled={lockForm}
            className={styles.formfield}
          />
        </div>
      </div> */}

      <div className={styles['form-inventory__title']}>
        Strain Classification
      </div>
      <RadioField
        id="strainClassification"
        radio={STRAIN_CLASSIFICATIONS}
        value={fields['strainClassification']}
        onChange={changeHandler}
        className={styles.formfield}
      />
    </>
  );
};