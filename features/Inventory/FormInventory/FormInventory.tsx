import React, { FC, useState, useEffect } from 'react';

import { validHandler } from 'utils/validation';
import { Button } from 'components/Button';
import { Spinner } from 'components/Spinner';
import { setInventoryUpdate, getCategory, createInventory } from 'services/api';
import { TextEditor } from "components/TextEditor";

import styles from './formInventory.module.scss';
import { InventoryProps } from "../types";
import { GeneralInfo } from "./GeneralInfo";
import { CategoryInfo } from "./CategoryInfo";
import { CharacteristicsInfo } from "./CharacteristicsInfo";
import { CoaInfo } from "./CoaInfo";
import { MetrcInfo } from "./MetrcInfo";

export interface FormInventoryProps {
  inventory: InventoryProps;
}

export const FormInventory: FC<FormInventoryProps> = ({ inventory }) => {

  const [errors, setErrors] = useState([]);
  const [errorsCreate, setErrorsCreate] = useState([]);
  const [lockForm, setLockForm] = useState(false);
  const [needValid, setNeedValid] = useState(false);
  const [fields, setFields] = useState(inventory);
  const [categories, setCategories] = useState([]);
  const [subCategories, setSubCategories] = useState([]);
  const [categoryName, setCategoryName] = useState('');
  const [loading, setLoading] = useState(false);
  const [editorLoaded, setEditorLoaded] = useState(false);

  useEffect(() => {
    getCategory()
      .then(setCategories);
    setEditorLoaded(true);
  }, []);

  useEffect(() => {
    const category = categories.find(c => c.value === fields['category']);
    setCategoryName(category ? category.label : '');

    const selected = categories.find(c => c.value == fields['category']);
    if(selected && selected.parent && selected.parent.id) {

      const f: InventoryProps = { ...fields };
      f.parentCategory = selected.parent.id;
      setFields(f);
      const sub = categories.filter(c => c.parent && c.parent.id === selected.parent.id);
      setSubCategories(sub);
    }
  }, [categories]);

  useEffect(() => {
    if(needValid) {
      const validation = ['name'];
      const { errors } = validHandler(fields, validation);
      setErrors(errors);
    }

    const category = categories.find(c => c.value === fields['category']);
    setCategoryName(category ? category.label : '');

  }, [fields]);

  const changeCategoryHandler = (name, value) => {
    const f = { ...fields };
    f[name] = value;
    const sub = categories.filter(c => c.parent && c.parent.id === value);
    setSubCategories(sub);
    setFields(f);
  };

  const changeHandler = e => {
    const f = { ...fields };
    f[e.target.name] = e.target.value;
    setFields(f);
  };

  const changeHandler2 = (name, value) => {
    const f = { ...fields };
    f[name] = value;
    setFields(f);
  };

  const checkboxHandler = e => {
    const f = { ...fields };
    f[e.target.name] = e.target.checked;
    setFields(f);
  };


  const saveInventory = e => {
    e.preventDefault();
    setNeedValid(true);
    const validation = [];
    const { valid, errors } = validHandler(fields, validation);
    setErrors(errors);

    if(!valid) return false;
    setLockForm(true);

    if(!inventory.id) {

      const f = { ...fields };

      for(const name in f) {
        const arr = name.match(/_\d+_/g);
        if(!arr) continue;
        const n = name.split(arr[0]);
        const i = name.match(/\d+/g)![0];
        if(!n[0] || !i || !n[1]) continue;
        if(!f[n[0]][i]) f[n[0]][i] = {};
        f[n[0]][i][n[1]] = f[name];
      }

      createInventory(f)
        .then((res) => {
          setErrorsCreate([]);
        })
        .catch((e) => {
          setErrorsCreate(e.response.data.error);
        })
        .finally(() => {
          setLockForm(false);
        });

    } else {

      setInventoryUpdate(fields)
        .then((res) => {

          setLockForm(false);
        });
    }

  };

  return (
    <div className={styles['form-inventory']}>
      {loading && <Spinner overlay />}
      <form onSubmit={saveInventory}>
        <div className={styles['form-inventory__wrapper']}>
          {!fields.id &&
            <MetrcInfo
              fields={fields}
              errors={errors}
              lockForm={lockForm}
              checkboxHandler={checkboxHandler}
              changeHandler={changeHandler}
              changeHandler2={changeHandler2}
              styles={styles}
            />
          }

          <GeneralInfo
            fields={fields}
            errors={errors}
            lockForm={lockForm}
            checkboxHandler={checkboxHandler}
            changeHandler={changeHandler}
            changeHandler2={changeHandler2}
            styles={styles}
          />
          <CategoryInfo
            categories={categories}
            subCategories={subCategories}
            fields={fields}
            errors={errors}
            lockForm={lockForm}
            changeCategoryHandler={changeCategoryHandler}
            changeHandler2={changeHandler2}
            styles={styles}
          />
          <CoaInfo
            fields={fields}
            styles={styles}
            setLoading={setLoading}
            setLockForm={setLockForm}
          />
          <CharacteristicsInfo
            fields={fields}
            errors={errors}
            lockForm={lockForm}
            changeHandler={changeHandler}
            styles={styles}
          />
        </div>

        <div className={styles['form-inventory__title']}>
          Description
        </div>

        {/* <TextEditor
          id="description"
          fieldName=""
          value={fields['description']}
          onChange={changeHandler2}
          disabled={lockForm}
          editorLoaded={editorLoaded}
        /> */}

        {errorsCreate.map((err: any) => <React.Fragment key={err.message}><span className="error">{err.message}</span><br /></React.Fragment>)}

        <div className={styles['form-inventory__bottom']}>
          <Button
            type="submit"
            color="green"
            width="width"
            disabled={lockForm}
          >
            Save
          </Button>
        </div>
      </form>
    </div>
  );
};