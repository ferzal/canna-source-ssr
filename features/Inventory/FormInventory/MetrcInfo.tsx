import { Fragment, FC, useState, useEffect, ChangeEvent, ChangeEventHandler } from 'react';

import { Checkbox } from "components/Checkbox";
import { TextField } from "components/TextField";
import { SelectField } from "components/SelectField";
import { UNITS } from "utils/constants";
import { DateField } from "components/DateField";
import { Button } from "components/Button";
import { CleanButton } from "components/CleanButton";
import { DeleteIcon } from "assets/img/icons";

export interface MetrcInfoProps {
  fields: any;
  errors: any;
  lockForm: boolean;
  checkboxHandler: (e: ChangeEvent<HTMLInputElement>) => void;
  changeHandler: (e: ChangeEvent<HTMLInputElement>) => void;
  styles: any;
  changeHandler2: (name: string, value: string) => void;
}

export const MetrcInfo: FC<MetrcInfoProps> = ({ fields, errors, lockForm, checkboxHandler, changeHandler, changeHandler2, styles }) => {
  const [countPackage, setCountPackage] = useState(1);

  const renderPackagesForm = () => {

    return Array.apply(null, { length: countPackage }).map((_, i) =>
      <Fragment key={i}>
        <div className={styles['form-inventory__title']}>
          Package #{i + 1}
          {i > 0 &&
            <CleanButton
              icon={DeleteIcon}
              onClick={() => setCountPackage(prev => prev == 1 ? prev : prev - 1)}
              color="red"
            />
          }
        </div>

        <TextField
          id={`ingredients_${i}_package`}
          type="text"
          fieldName="Package"
          value={fields[`ingredients_${i}_package`]}
          errors={errors}
          onChange={changeHandler}
          disabled={lockForm}
          className={styles.formfield}
        />

        <div className="row">
          <div className="col6">
            <TextField
              id={`ingredients_${0}_quantity`}
              type="text"
              fieldName="Quantity"
              value={fields[`ingredients_${0}_quantity`]}
              errors={errors}
              onChange={changeHandler}
              disabled={lockForm}
              className={styles.formfield}
            />
          </div>
          <div className="col6">
            <SelectField
              id={`ingredients_${0}_unitOfMeasure`}
              fieldName="Unit of measure"
              value={fields[`ingredients_${0}_unitOfMeasure`]}
              errors={errors}
              options={UNITS}
              onChange={changeHandler2}
              disabled={lockForm}
              className={styles.formfield}
            />
          </div>
        </div>
      </Fragment>
    );
  };

  return (
    <>
      <TextField
        id="tag"
        type="text"
        fieldName="New Tag"
        value={fields.tag}
        errors={errors}
        onChange={changeHandler}
        disabled={lockForm}
        className={styles.formfield}
      />

      <TextField
        id="item"
        type="text"
        fieldName="Item"
        value={fields.item}
        errors={errors}
        onChange={changeHandler}
        disabled={lockForm}
        className={styles.formfield}
      />

      <div className="row">
        <div className="col6">
          <TextField
            id="quantity"
            type="text"
            fieldName="Quantity"
            value={fields.quantity}
            errors={errors}
            onChange={changeHandler}
            disabled={lockForm}
            className={styles.formfield}
          />
        </div>
        <div className="col6">
          <SelectField
            id="unitOfMeasure"
            fieldName="Unit of measure"
            value={fields.unitOfMeasure}
            errors={errors}
            options={UNITS}
            onChange={changeHandler2}
            disabled={lockForm}
            className={styles.formfield}
          />
        </div>
      </div>

      <TextField
        id="note"
        type="text"
        fieldName="Note"
        value={fields.note}
        errors={errors}
        onChange={changeHandler}
        disabled={lockForm}
        className={styles.formfield}
      />


      <DateField
        id="actualDate"
        fieldName="Actual date"
        value={fields.actualDate}
        errors={errors}
        onChange={changeHandler2}
        disabled={lockForm}
        mask="99/99/9999"
        className={styles.formfield}
      />


      <div className={styles['form-inventory__package']}>
        {renderPackagesForm()}
        <Button
          color="green"
          width="width"
          disabled={lockForm}
          onClick={() => setCountPackage(prev => prev + 1)}
        >
          Add Package
        </Button>
      </div>

    </>
  );
};