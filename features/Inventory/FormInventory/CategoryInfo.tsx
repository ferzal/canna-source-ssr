import { SelectField } from "components/SelectField";
import { FC, useState, useEffect, ChangeEvent } from 'react';
import { GROW_TYPE, SIZE_CATEGORY } from "utils/constants";

export interface CategoryInfoProps {
  categories: any;
  subCategories: any;
  fields: any;
  errors: any;
  lockForm: boolean;
  changeCategoryHandler: (name: string, value: string) => void;
  changeHandler2: (name: string, value: string) => void;
  styles: any;
}

export const CategoryInfo: FC<CategoryInfoProps> = ({ categories, subCategories, fields, errors, lockForm, changeCategoryHandler, changeHandler2, styles }) => {

  
  return (
    <>
      {/* <div className="row">
        <div className={styles['formfield']}>
          Category name: {categoryName}
        </div>
      </div> */}

      <div className="row">
        <div className="col6">
          <SelectField
            id="parentCategory"
            fieldName="Product category"
            value={fields['parentCategory']}
            errors={errors}
            options={categories.filter(c => !c.parent)}
            onChange={changeCategoryHandler}
            disabled={lockForm}
            className={styles.formfield}
          />
        </div>
        <div className="col6">
          <SelectField
            id="category"
            fieldName="Product subcategory"
            value={fields['category'] ? fields['category'] : ''}
            errors={errors}
            options={subCategories}
            onChange={changeHandler2}
            disabled={fields['parentCategory'] ? lockForm : true}
            className={styles.formfield}
          />
        </div>
      </div>

      {fields['parentCategory'] === 'ac5baee4-4056-4f36-8df1-e6f86f31ef24' &&
        <div className="row">
          <div className="col6">
            <SelectField
              id="growType"
              fieldName="Flower Growing Operation"
              value={fields['growType']}
              errors={errors}
              options={GROW_TYPE}
              onChange={changeHandler2}
              disabled={lockForm}
              className={styles.formfield}
            />
          </div>
        </div>
      }

      <div className="row">
        <div className="col6">
          <SelectField
            id="sizeCategory"
            fieldName="Size Category"
            value={fields['sizeCategory'] ? fields['sizeCategory'] : ''}
            errors={errors}
            options={SIZE_CATEGORY}
            onChange={changeHandler2}
            disabled={lockForm}
            className={styles.formfield}
          />
        </div>
      </div>
    </>
  );
};