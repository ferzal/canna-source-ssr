import { FC, useState, useEffect, ChangeEvent, ChangeEventHandler } from 'react';

import { Checkbox } from "components/Checkbox";
import { TextField } from "components/TextField";
import { SelectField } from "components/SelectField";
import { UNITS } from "utils/constants";
import { DateField } from "components/DateField";

export interface GeneralInfoProps {
  fields: any;
  errors: any;
  lockForm: boolean;
  checkboxHandler: (e: ChangeEvent<HTMLInputElement>) => void;
  changeHandler: (e: ChangeEvent<HTMLInputElement>) => void;
  styles: any;
  changeHandler2: (name: string, value: string) => void;
}

export const GeneralInfo: FC<GeneralInfoProps> = ({ fields, errors, lockForm, checkboxHandler, changeHandler, changeHandler2, styles }) => {
  return (
    <>
      <Checkbox
        title="Published"
        id="isPublished"
        onChange={checkboxHandler}
        checked={fields['isPublished']}
        disabled={lockForm}
        className={styles.formfield}
      />


      {fields.id &&
        <TextField
          id="name"
          type="text"
          fieldName="Metrc name"
          value={fields['name']}
          errors={errors}
          onChange={changeHandler}
          disabled={lockForm}
          className={styles.formfield}
        />
      }
      {/* <div className={styles['form-inventory__text']}>
        *Updates will reflect in Metrc
      </div> */}

      <TextField
        id="marketplaceName"
        type="text"
        fieldName="Marketplace name"
        value={fields['marketplaceName']}
        errors={errors}
        onChange={changeHandler}
        disabled={lockForm}
        className={styles.formfield}
      />



      <div className="row">
        <div className="col4">
          <TextField
            id="price"
            type="text"
            fieldName="Price"
            value={String(fields['price'])}
            errors={errors}
            onChange={changeHandler}
            disabled={lockForm}
            className={styles.formfield}
          />
        </div>
        <div className="col4">
          <TextField
            id="oldPrice"
            type="text"
            fieldName="Old Price"
            value={String(fields['oldPrice'])}
            errors={errors}
            onChange={changeHandler}
            disabled={lockForm}
            className={styles.formfield}
          />
        </div>
        <div className="col4">
          <Checkbox
            title="Negotiable"
            id="negotiable"
            checked={fields['negotiable']}
            onChange={checkboxHandler}
            disabled={lockForm}
            className={styles.formfield}
          />
        </div>
      </div>

      <div className="row">

        <div className={styles['formfield']}>
          Quantity: {fields.quantity} / {fields.unitOfMeasureName} Available

          <Checkbox
            title="Show Inventory Quantity to Buyers"
            id="showQuantity"
            onChange={checkboxHandler}
            checked={fields.showQuantity}
            disabled={lockForm}
            className={styles.formfield}
          />
          <div className={styles['form-inventory__text']}>
            Allow retailers to see this product’s quantity
          </div>
        </div>
      </div>
    </>
  );
};