import { FC, useState, useEffect, useCallback, SetStateAction } from "react";
import { useDropzone } from "react-dropzone";
import classNames from "classnames";
import Lightbox from "react-image-lightbox";
import update from "immutability-helper";
import { DndProvider } from "react-dnd";
import { HTML5Backend } from "react-dnd-html5-backend";

import {
  getUrlForUpload,
  putFileOnAmazon,
  setMedia,
  removeMedia,
  sortMedia,
  uploadMedia,
} from "services/api";
import { Button } from "components/Button";
import { Spinner } from "components/Spinner";
import { TextField } from "components/TextField";
import imgResize from "utils/imgResizer";

import { MediaItem } from "./MediaItem";
import FileIcon from "./icon/file.svg";
import styles from "./mediaInventory.module.scss";

export interface MediaInventoryProps {
  id: string;
  media: {
    preview: string;
    previewBig: string;
    type: string;
  }[];
  getAndSetInventory: () => void;
}

export const MediaInventory: FC<MediaInventoryProps> = ({
  id,
  media,
  getAndSetInventory,
}) => {
  const [loadingImage, setLoadingImage] = useState(false);
  const [isOpen, setIsOpen] = useState(false);
  const [photoIndex, setPhotoIndex] = useState(0);
  const [mediaItems, setMediaItems] = useState([]);
  const [videoItems, setVideoItems] = useState([]);
  const [video, setVideo] = useState("");

  const onOpen = (i) => {
    setPhotoIndex(i);
    setIsOpen(true);
  };

  useEffect(() => {
    setMediaItems(media || [].filter((item) => item.type === "Amazon S3"));
  }, [media]);

  const remove = (mediaId) => {
    setLoadingImage(true);
    removeMedia(mediaId)
      .then(() => {
        getAndSetInventory();
      })
      .catch((e) => {
        console.error(e);
      })
      .finally(() => setLoadingImage(false));
  };

  const uploadFiles = (files) => {
    if (!files.length) return false;

    const asyncFunc = async () => {
      const unresolvedPromises = [...files].map(async (file) => {
        return await imgResize(file);
      });
      const results = await Promise.all(unresolvedPromises);

      uploadMedia(results, id)
        .then((media) => {
          setMediaItems(
            media || [].filter((item) => item.type === "Amazon S3")
          );
          getAndSetInventory();
        })
        .finally(() => setLoadingImage(false));
    };

    asyncFunc();
  };

  const handleUpload = (e) => {
    setLoadingImage(true);
    const files = e.target.files;
    uploadFiles(files);
  };

  const onDrop = useCallback((acceptedFiles) => {
    setLoadingImage(true);
    uploadFiles(acceptedFiles);
  }, []);

  const { getRootProps, getInputProps, isDragActive } = useDropzone({ onDrop });

  const moveMediaItems = useCallback(
    (dragIndex, hoverIndex) => {
      const dragCard = mediaItems[dragIndex];
      const sorted = update(mediaItems, {
        $splice: [
          [dragIndex, 1],
          [hoverIndex, 0, dragCard],
        ],
      });

      setMediaItems(sorted);
      setLoadingImage(true);
      sortMedia(sorted.map((media) => media.id))
        .then(() => {
          setLoadingImage(false);
        })
        .catch((e) => {
          console.error(e);
        });
    },
    [mediaItems]
  );

  const addVideo = () => {
    setMedia({ fileName: video, type: "video", inventoryId: id });
  };

  return (
    <>
      {loadingImage && <Spinner overlay />}
      <div className={styles["media-inventory__title"]}>Product Images</div>
      <div className={styles["media-inventory__row"]}>
        <div
          className={classNames(styles["media-inventory__upload"], {
            [styles.active]: isDragActive,
          })}
          {...getRootProps()}
        >
          <input
            {...getInputProps()}
            multiple
            type="file"
            id="uploadImage"
            name="uploadImage"
            onChange={handleUpload}
          />
          <div className={styles["media-inventory__label"]}>
            <FileIcon />
            <div>Drag & Drop your file here</div>
            <div>or</div>
            <Button width="width">Upload File</Button>
          </div>
        </div>
        <div className={styles["media-inventory__images"]}>
          <DndProvider backend={HTML5Backend}>
            {mediaItems &&
              mediaItems.length > 0 &&
              mediaItems.map((media, i) => (
                <MediaItem
                  onClick={() => onOpen(i)}
                  key={media.id}
                  index={i}
                  id={media.id}
                  text={media.preview}
                  moveCard={moveMediaItems}
                  remove={remove}
                />
              ))}
          </DndProvider>
        </div>
        <div className={styles["media-inventory__content"]}>
          <div>Your Images must be:</div>
          <p>700 x 700 minimum dimensions</p>
          <p>10 MB maximum file size</p>
          <p>Avoid using logos for product images</p>
          <p>Rearrange images by draging photo</p>
        </div>
      </div>
      <div className={styles["media-inventory__title"]}>Video</div>
      <div className={styles["media-inventory__video"]}>
        {videoItems &&
          videoItems.map((item) => (
            <div
              className={styles["media-inventory__video-item"]}
              key={item.file}
            >
              <a href={item.file} target="_blank" rel="noreferrer">
                {item.file}
              </a>
              <button
                type="button"
                className={styles["button-delete"]}
                onClick={() => remove(item.id)}
              ></button>
            </div>
          ))}
      </div>

      <div className={styles["media-inventory__wrap"]}>
        <div className={styles["media-inventory__input"]}>
          <TextField
            id="video"
            type="text"
            fieldName="Video"
            value={video}
            onChange={(e) => setVideo(e.target.value)}
            className={styles.formfield}
          />
        </div>
        <Button width="width" onClick={addVideo}>
          Add video
        </Button>
      </div>

      {isOpen && (
        <Lightbox
          mainSrc={mediaItems[photoIndex].file}
          nextSrc={mediaItems[(photoIndex + 1) % mediaItems.length].file}
          prevSrc={
            mediaItems[(photoIndex + mediaItems.length - 1) % mediaItems.length]
              .file
          }
          onCloseRequest={() => setIsOpen(false)}
          onMovePrevRequest={() =>
            setPhotoIndex(
              (photoIndex + mediaItems.length - 1) % mediaItems.length
            )
          }
          onMoveNextRequest={() =>
            setPhotoIndex((photoIndex + 1) % mediaItems.length)
          }
        />
      )}
    </>
  );
};
