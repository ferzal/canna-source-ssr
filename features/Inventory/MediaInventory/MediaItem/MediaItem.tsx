import React, { FC, MouseEvent, useCallback, useEffect, useRef, useState } from 'react';
import { useDrag, useDrop } from 'react-dnd';

import styles from './mediaItem.module.scss';

export interface MediaItemProps {
  id: string;
  text: string;
  index: number;
  moveCard: (dragIndex: number, hoverIndex: number) => void;
  remove: (id: string) => void;
  onClick: (e: MouseEvent<HTMLElement>) => void;
}

export const MediaItem: FC<MediaItemProps> = ({ id, text, index, moveCard, remove, onClick }) => {
  const [opacity, setOpacity] = useState(1);
  const ref = useRef(null);
  const [{ handlerId }, drop] = useDrop({
    accept: 'card',
    collect(monitor) {
      return {
        handlerId: monitor.getHandlerId()
      };
    },
    hover(item, monitor) {
      if(!ref.current) {
        return;
      }
      const dragIndex = item.index;
      const hoverIndex = index;
      if(dragIndex === hoverIndex) {
        return;
      }
      const hoverBoundingRect = ref.current.getBoundingClientRect();
      const hoverMiddleX = (hoverBoundingRect.right - hoverBoundingRect.left) / 2;
      const clientOffset = monitor.getClientOffset();
      const hoverClientX = clientOffset.x - hoverBoundingRect.left;

      if(dragIndex < hoverIndex && hoverClientX < hoverMiddleX) {
        return;
      }
      if(dragIndex > hoverIndex && hoverClientX > hoverMiddleX) {
        return;
      }
      moveCard(dragIndex, hoverIndex);
      item.index = hoverIndex;
    }
  });

  const [{ isDragging }, drag] = useDrag({
    type: 'card',
    item: () => {
      return { id, index };
    },
    collect: (monitor) => ({
      isDragging: monitor.isDragging()
    })
  });

  useEffect(() => {
    setOpacity(isDragging ? 0 : 1);
    drag(drop(ref));
  }, [isDragging]);

  return (
    <div key={id} ref={ref} className={styles['media-item']} style={{ opacity }} data-handler-id={handlerId} >
      <img src={text} alt="" onClick={onClick} />
      <button className={styles['button-delete']} onClick={() => remove(id)}></button>
    </div>
  );
};