export interface InventoryProps {
  id: string;
  name: string;
  marketplaceName: string;
  price: number;
  thc: number;
  category: string;
  isPublished: boolean;
  description: string;
  showQuantity: boolean;
  negotiable: boolean;
  oldPrice: number;
  strainClassification: string;
  growType: string;
  sizeCategory: string;
  parentCategory?: string;
}