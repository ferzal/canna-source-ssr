import { FC } from 'react';
import Link from "next/link";

import { TableItem } from 'components/Table/TableItem';
import { Checkbox } from 'components/Checkbox';
import { EditablePrice } from 'features/Inventory/components/EditablePrice';
import { TableMedia } from 'features/Inventory/TableMedia';
import { EditIcon } from 'assets/img/icons';
import { dateFormat } from 'utils/format';
import classes from 'utils/classNames';

import ViewIcon from './view.svg';
import styles from './inventoryItem.module.scss';

interface InventoryItemProps {
  item: any;
  selectedItems: any;
  onSelectItem: any;
  setInventoryPublished: any;
  onSavedMedia: any;
}

export const InventoryItem: FC<InventoryItemProps> = ({ item, selectedItems, onSelectItem, setInventoryPublished, onSavedMedia }) => {
  return (
    <TableItem
      columnsWidth={[25, 8, 12, 15, 23, 8,7]}
      className={styles['inventory-item']}
    >

      <>
        <Checkbox
          id={item.id}
          checked={selectedItems.find(i => i === item.id) ? true : false}
          onChange={e => onSelectItem(e, item.id)}
        />
        <Link href={`/inventory/${item.id}`} passHref>
          <a>
            {item.name}<br />
            <span className={styles['small-text']}>{item.marketplaceName}</span>
          </a>
        </Link>
      </>

      <EditablePrice
        id={item.id}
        value={item.price}
      />

      <>
        {item.quantity} / {item.unitOfMeasureName}
      </>

      {item.category}

      <TableMedia
        id={item.id}
        media={item.media}
        onSavedMedia={onSavedMedia}
        classNameLink={styles['media-link']}
      />

      {dateFormat(item.lastModified)}

      <>
        <button className={classes(
          styles['published-button'],
          !item.isPublished && styles['hidden']
        )}
          onClick={() => setInventoryPublished(item.id, item.isPublished)}>
          <ViewIcon />
        </button>
        <Link href={`/inventory/${item.id}`} passHref>
          <a className={styles['edit-button']}>
            <EditIcon />
          </a>
        </Link>
      </>

    </TableItem>
  );
};