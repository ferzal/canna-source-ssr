import { FC } from 'react';
import classNames from "classnames";

import styles from './contactInfo.module.scss';

export interface ContactInfoProps {
	web: string;
	phone: string;
	location: string;
}

export const ContactInfo: FC<ContactInfoProps> = ({ web, phone, location }) =>
	<div className={styles['contact-info']}>
		<a href={web} target="_blank" className={classNames(styles['contact-info__str'], styles['web'])} rel="noreferrer">{web}</a>
		<div className={classNames(styles['contact-info__str'], styles['phone'])}>{phone}</div>
		<div className={classNames(styles['contact-info__str'], styles['location'])}>{location}</div>
	</div>;