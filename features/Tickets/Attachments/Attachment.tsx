import { FC } from 'react';
import { fileSizeType } from "utils/fileSizeType";

import styles from './attachment.module.scss';
import FileGreen from "./FileGreen.svg";

export interface AttachmentProps {
  file: any,
  removeFile?: () => void
}

export const Attachment: FC<AttachmentProps> = ({ file, removeFile }) => {
  return (
    <div key={file.token} className={styles.file}>
      <div className={styles.icon}><FileGreen /></div>
      <div className={styles.text}>
        <div>{file.name}</div>
        <div className={styles.size}>{fileSizeType(file.size)}</div>
      </div>
      {removeFile &&
        <button type="button" onClick={removeFile}></button>
      }
    </div>
  );
};