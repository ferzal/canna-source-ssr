import { FC, useEffect, useState } from 'react';
import {
  Accordion,
  AccordionItem,
  AccordionItemHeading,
  AccordionItemButton,
  AccordionItemPanel,
} from 'react-accessible-accordion';

import { Panel2 } from 'components/Panel2';
import { getQuestions } from "services/api";

import styles from './faqCategory.module.scss';

export interface FaqCategoryProps {
  category?: string;
}

export const FaqCategory: FC<FaqCategoryProps> = ({ category }) => {
  const [questions, setQuestions] = useState([]);
  const [loading, setLoading] = useState(false);

  const getAndSetQuestions = () => {
    setLoading(true);
    getQuestions(category)
      .then((res: any) => setQuestions(res))
      .catch(e => console.error(e))
      .finally(() => setLoading(false));
  };

  useEffect(() => {
    getAndSetQuestions();
  }, [category]);

  const renderFaq = () => {
    return questions.map((item: any) => <AccordionItem key={item.id} uuid={item.id} className={styles.item}>
      <AccordionItemHeading className={styles.heading}>
        <AccordionItemButton className={styles.button}>
          {item.question}
        </AccordionItemButton>
      </AccordionItemHeading >
      <AccordionItemPanel className={styles.panel}>
        <p>
          {item.answer}
        </p>
      </AccordionItemPanel>
    </AccordionItem>
    );
  };

  return (
    <Panel2 className={styles.category} loading={loading}>
      <h2>F.A.Q. (Frequently Asked Questions):</h2>
      <Accordion
        preExpanded={[1]}
        allowZeroExpanded
      >
        {renderFaq()}
      </Accordion>
    </Panel2>
  );
};