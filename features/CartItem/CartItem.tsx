import React, { FC } from "react";
import { useDispatch } from "react-redux";

import { NoPhotoIcon } from "assets/img/icons";
import { QuantityChange } from "components/QuantityChange";
import { changeCart, removeFromCart } from "redux/actions/cartActions";

import styles from "./cartItem.module.scss";

export interface CartItemProps {
  item: {
    inventory: {
      name: string;
      quantity: number;
      unitOfMeasureName: string;
      categoryName: string;
      coa: string;
      thc: string;
      preview: string;
    };
    quantity: number;
    oldPrice: string;
    price: string;
    id: string;
    total: string;
    seller: string;
  };
}

export const CartItem: FC<CartItemProps> = ({ item }) => {
  const dispatch = useDispatch();

  const remove = () => {
    dispatch(removeFromCart({ id: item.id }));
  };

  const setQuantity = (quantity: number) => {
    dispatch(changeCart({ id: item.id, quantity }));
  };

  return (
    <div className={styles["cart-item"]}>
      <div className={styles["cart-item__image"]}>
        {item.inventory.preview ? (
          <img src={item.inventory.preview} />
        ) : (
          <span className={styles["cart-item__nophoto"]}>
            <NoPhotoIcon />
            No Photo
          </span>
        )}
      </div>
      <div className={styles["cart-item__info"]}>
        <div className={styles['cart-item__seller']}>{item.seller}</div>
        <div className={styles["cart-item__top"]}>
          <div className={styles["cart-item__top-title"]}>
            {item.inventory.name}
          </div>
          <div className={styles["cart-item__top-stock"]}>
            <span>In Stock:</span> {item.inventory.quantity} /{" "}
            {item.inventory.unitOfMeasureName}
          </div>
        </div>
        <div className={styles["cart-item__characteristics"]}>
          <ul>
            <li>Category: {item.inventory.categoryName}</li>
            {item.inventory.coa && <li>COA: Yes</li>}
            <li>THC: {item.inventory.thc}%</li>
          </ul>
        </div>
      </div>
      <div className={styles["cart-item__quantity"]}>
        <div className={styles["cart-item__top"]}>Quantity:</div>
        <div className={styles["cart-item__change"]}>
          <QuantityChange quantity={item.quantity} setQuantity={setQuantity} />
        </div>
      </div>
      <div className={styles["cart-item__price"]}>
        <div className={styles["cart-item__top"]}>Price:</div>
        <div className={styles["cart-item__price-one"]}>
          {item.oldPrice ? (
            <>
              <span className={styles["old_price"]}>
                $ {item.oldPrice.toLocaleString("en-US")}
              </span>
              $ {item.price.toLocaleString("en-US")}
            </>
          ) : (
            <>$ {item.price.toLocaleString("en-US")}</>
          )}
        </div>
        <div className={styles["cart-item__price-total"]}>
          x {item.quantity} = $ {item.total.toLocaleString("en-US")}
        </div>
      </div>
      <button className={styles["cart-item__remove"]} onClick={() => remove()}>
        &times;
      </button>
    </div>
  );
};
