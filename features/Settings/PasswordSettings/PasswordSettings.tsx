import { FC, useState, useEffect, ChangeEvent } from 'react';

import { Button } from 'components/Button';
import { Spinner } from 'components/Spinner';
import { TextField } from 'components/TextField';
import { SettingTitle } from 'features/Settings/SettingTitle';
import { validHandler } from 'utils/validation';
import { setUserUpdate } from 'services/api';
import classNames from "classnames";

import styles from './passwordSettings.module.scss';

export interface UserType {
  id: string
}

export const PasswordSettings: FC<UserType> = ({ user }: any) => {
  const [errors, setErrors] = useState<any>([]);

  const [lockForm, setLockForm] = useState(false);
  const [needValid, setNeedValid] = useState(false);
  const [loading, setLoading] = useState(false);

  const [fields, setFields] = useState({
    password: '',
    repeatPassword: ''
  });

  useEffect(() => {
    if(needValid) {
      const validation = ['password', 'repeatPassword'];
      const { errors } = validHandler(fields, validation);
      setErrors(errors);
    }
  }, [fields]);

  const changeHandler = (e: ChangeEvent<HTMLInputElement>) => {
    const f: any = { ...fields };
    f[e.target.name] = e.target.value;
    setFields(f);
  };

  const savePassword = (e: ChangeEvent<HTMLFormElement>) => {
    e.preventDefault();
    setNeedValid(true);
    const validation = ['password', 'repeatPassword'];
    const { valid, errors } = validHandler(fields, validation);
    setErrors(errors);

    if(valid) {
      setLockForm(true);
      setLoading(true);
      setUserUpdate({ id: user.id, ...fields })
        .then(() => {
          setLockForm(false);
          setLoading(false);
        })
        .catch(e => {
          console.error(e);
        });
    }
  };

  return (
    <form onSubmit={savePassword}>

      <div className={classNames(
        styles['password-settings__license'],
        styles['overall-design']
      )}>
        <SettingTitle>
          Change password
        </SettingTitle>
        {loading && <Spinner overlay />}
        <div className={styles['password-settings__wrapper']}>
          <div className={styles['row']}>
            <div className={styles['col6']}>
              <TextField
                id="password"
                type="password"
                fieldName="Password"
                value={fields['password']}
                errors={errors}
                onChange={changeHandler}
                disabled={lockForm}
                className={styles['password-settings__formfield']}
              />
            </div>
            <div className={styles['col6']}>
              <TextField
                id="repeatPassword"
                type="password"
                fieldName="Repeat password"
                value={fields['repeatPassword']}
                errors={errors}
                onChange={changeHandler}
                disabled={lockForm}
                className={styles['password-settings__formfield']}
              />
            </div>
          </div>
          <div className={styles['rigth']}>
            <Button type="submit" color="green" width="width">Change</Button>
          </div>
        </div>
      </div>
    </form>
  );
};