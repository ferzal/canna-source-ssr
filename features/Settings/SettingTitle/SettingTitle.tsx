import { FC } from 'react';
import styles from './settingTitle.module.scss';

export const SettingTitle: FC = ({ children }) => {
  return (
    <h2 className={styles['setting-title']}>
			{children}
		</h2>
  );
};