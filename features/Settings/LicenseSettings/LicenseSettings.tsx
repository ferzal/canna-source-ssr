import React, { FC, useState, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import classNames from "classnames";

import { Button } from 'components/Button';
import { TextField } from 'components/TextField';
//import FileField from "../ui/FileField";
import { DateField } from 'components/DateField';
import { SelectField } from 'components/SelectField';
import { validHandler } from 'utils/validation';
import { getLicenseTypes, getUrlForUpload, putFileOnAmazon, setUserUpdate } from 'services/api';
import { SettingTitle } from 'features/Settings/SettingTitle';
import { FileIcon, PdfIcon } from 'assets/img/icons';
import { Spinner } from 'components/Spinner';
import { UserType } from 'pages/settings';
import { userGet } from 'redux/actions/userActions';

import styles from './licenseSettings.module.scss';


export const LicenseSettings: FC<UserType> = ({ user }) => {
	const dispatch = useDispatch();
	const [errors, setErrors] = useState([]);
	const [lockForm, setLockForm] = useState(false);
	const [needValid, setNeedValid] = useState(false);
	const [fields, setFields] = useState({ licenseNumber: user.licenseNumber, licenseType: user.licenseType, issueDate: user.issueDate, expirationDate: user.expirationDate });
	const [loading, setLoading] = useState(false);
	const [licenseTypes, setLicenseTypes] = useState([]);

	useEffect(() => {
		getLicenseTypes(user.state)
			.then(res => {
				setLicenseTypes(res);
			})
			.catch(e => {
				console.error(e);
			});
	}, []);


	useEffect(() => {
		if(needValid) {
			const validation = ['email'];
			const { errors } = validHandler(fields, validation);
			setErrors(errors);
		}
	}, [fields]);

	const fileInputHandler = e => {
		e.preventDefault();
		setLoading(true);
		const onChange = async (file) => {
			try {
				let fileName;
				getUrlForUpload({
					contentType: file.type,
					fileName: file.name
				})
					.then(res => {
						fileName = res.fileName;
						return putFileOnAmazon({ url: res.uploadUrl, file });
					})
					.then(() => {
						return setUserUpdate({ licenseFile: fileName });
					})
					.then(() => {
						return dispatch(userGet());
					})
					.then(() => {
						setLoading(false);
					})
					.catch(e => {
						console.error(e);
					});
			} catch(err) {
				console.error(err);
			}
		};
		onChange(e.target.files[0]);
	};

	const changeHandler = e => {
		const f = { ...fields };
		f[e.target.name] = e.target.value;
		setFields(f);
	};

	const selectHandler = (name, value) => {
		const f = { ...fields };
		f[name] = value;
		setFields(f);
	};

	const changeDateHandler = (name, value) => {
		const f = { ...fields };
		f[name] = value;
		setFields(f);
	};

	const saveCompanyInformation = e => {
		e.preventDefault();
		setNeedValid(true);
		setLockForm(true);
		setLoading(true);
		if(!errors.length) {
			setUserUpdate({ id: user.id, ...fields })
				.then(() => {
					setLockForm(false);
					setLoading(false);
				})
				.catch(e => {
					console.error(e);
				});
		}
	};

	return (
		<div className={classNames(
			styles['license-settings'],
			styles['overall-design']
		)}>
			<SettingTitle>
				License Info
			</SettingTitle>
			{loading && <Spinner overlay />}
			<div className={styles['license-settings__wrapper']}>
				<form onSubmit={saveCompanyInformation}>

					<div className={styles['row']}>
						<div className={styles['col6']}>
							<TextField
								id="licenseNumber"
								type="text"
								fieldName="License number"
								value={fields['licenseNumber']}
								errors={errors}
								onChange={changeHandler}
								disabled={lockForm}
								mask="a99-9999999-LIC"
								className={styles['license-settings__formfield']}
							/>
							<SelectField
								id="licenseType"
								fieldName="License type"
								value={fields['licenseType']}
								options={licenseTypes}
								errors={errors}
								onChange={selectHandler}
								disabled={lockForm}
								className={styles['license-settings__formfield']}
							/>
						</div>
						<div className={styles['col6']}>
							<DateField
								id="issueDate"
								fieldName="Issue date"
								value={fields['issueDate']}
								errors={errors}
								onChange={changeDateHandler}
								disabled={lockForm}
								mask="99/99/9999"
								className={styles['license-settings__formfield']}
							/>
							<DateField
								id="expirationDate"
								fieldName="Expiration date"
								value={fields['expirationDate']}
								errors={errors}
								onChange={changeDateHandler}
								disabled={lockForm}
								mask="99/99/9999"
								className={styles['license-settings__formfield']}
							/>
						</div>
					</div>
					<div className={styles['input-file']}>
						<input type="file" id="licenseFile" onChange={fileInputHandler} />
						<label htmlFor="licenseFile">
							<div className={styles['input-file__title']}>License file</div>
							<div className={styles['input-file__text']}>
								<div className={styles['input-file__img']}>
									{user.licenseFile ? <PdfIcon /> : <FileIcon />}
									<div className={styles['input-file__name']}>
										{user.licenseFile &&
											<a
												href={user.licenseFile}
												target="_blank"
												rel="noreferrer"
											>
												{user.licenseFile.split('/')[4]}
											</a>
										}
									</div>
								</div>
								Choose license file
							</div>
						</label>
					</div>
					<div className={styles['rigth']}>
						<Button type="submit" color="green" width="width">Save Changes</Button>
					</div>
				</form>
			</div>
		</div>
	);
};