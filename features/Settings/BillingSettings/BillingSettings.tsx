import { FC, useState, useEffect } from 'react';
import classNames from "classnames";
import Link from "next/link";

import { SettingTitle } from 'features/Settings/SettingTitle';
import { getCards, getPlan } from 'services/api';
import { MastercardIcon } from 'assets/img/icons';
import { Spinner } from 'components/Spinner';
//import useModalAddCard from '../../hooks/useModalAddCard';
import { dateFormatTimestmp, priceFormat } from 'utils/format';
import { UserType } from 'pages/settings';

import styles from './billingSettings.module.scss';

export const BillingSettings: FC<UserType> = ({ user }) => {
	const [cards, setCards] = useState([]);
	const [plan, setPlan] = useState({});
	const [loading, setLoading] = useState(true);

	const getAndSetCards = () => {
		getCards(user.id)
			.then(res => {
				setCards(res.data);
			})
			.catch(e => {
				console.error(e);
			})
			.finally(() => setLoading(false));
	};

	useEffect(() => {

		getAndSetCards();

		getPlan(user.id)
			.then(res => setPlan(res))
			.catch(e => {
				console.error(e);
			})
			.finally(() => setLoading(false));
	}, []);

	//	const { openModalAddCard, ModalAddCard } = useModalAddCard();


	return (
		<div className={classNames(
			styles['billing-settings'],
			styles['overall-design'],
		)}>
			<SettingTitle>
				Subscription
			</SettingTitle>
			{plan &&
				<>
					<p>Current Plan:{' '}
						{plan.name}  (${priceFormat(plan.amount) / 100} / {plan.interval})
					</p>
					<div>
						<div>
							Period start: {dateFormatTimestmp(plan.currentPeriodStart)}
						</div>
						<div>
							Period end: {dateFormatTimestmp(plan.currentPeriodEnd)}
						</div>
					</div>
				</>
			}


			<p className="billing-settings__small">Your plan will automatically renew monthly. To prevent renewal notify us by the 15th of the month to cancel for the subsequent month.</p>
			<Link href="/plans" passHref><a>Change Plan</a></Link>



			<div className={styles['payment-methods']}>
				<div className={styles['payment-methods__title']}>Payment methods</div>
				{loading
					? <Spinner />
					: cards && cards.map(c =>
						<div key={c.id} className={styles['c-card']} >
							<div className={styles['c-card__icon']}>
								<MastercardIcon />
							</div>
							<div className={styles['c-card__info']}>
								<div className={styles['c-card__brand']}>{c.brand} •••• {c.last4} {c.default && <span className={styles['c-card__default']}>Default</span>}</div>
								<div className={styles['c-card__exp']}>Expires {c.exp_month} / {c.exp_year}</div>
							</div>
						</div>
					)}
				{/* <a href="#" onClick={openModalAddCard}>+ Add card</a> */}
			</div>
			{/* <ModalAddCard /> */}
		</div >
	);
};