import { FC, useState, useEffect } from 'react';

import { Button } from 'components/Button';
import { TextField } from 'components/TextField';
import { validHandler } from 'utils/validation';
import { setUserUpdate } from 'services/api';
import { SettingTitle } from 'features/Settings/SettingTitle';
import { Spinner } from 'components/Spinner';
import { UserType } from 'pages/settings';
import classNames from "classnames";

import styles from './apiSettings.module.scss';

export const ApiSettings: FC<UserType> = ({ user }) => {
  const [errors, setErrors] = useState([]);
  const [lockForm, setLockForm] = useState(false);
  const [needValid, setNeedValid] = useState(false);
  const [fields, setFields] = useState({ metrcApiKey: user.metrcApiKey });
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    if(needValid) {
      const validation = ['metrcApiKey'];
      const { errors } = validHandler(fields, validation);
      setErrors(errors);
    }
  }, [fields]);


  const changeHandler = e => {
    const f = { ...fields };
    f[e.target.name] = e.target.value;
    setFields(f);
  };


  const saveMetrcApiKey = e => {
    e.preventDefault();
    setNeedValid(true);
    setLockForm(true);
    setLoading(true);
    if(!errors.length) {
      setUserUpdate({ id: user.id, ...fields })
        .then(() => {
          setLockForm(false);
          setLoading(false);
        })
        .catch(e => {
          console.error(e);
        });
    }
  };

  return (
    <form onSubmit={saveMetrcApiKey}>
      <div className={classNames(
        styles['api-settings'],
        styles['overall-design'],
      )}>
        <SettingTitle>
          Api Metrc Key
        </SettingTitle>
        {loading && <Spinner overlay />}
        <div className="api-settings__wrapper">
          <div className="row">
            <div className="col8">
              <TextField
                id="metrcApiKey"
                type="text"
                fieldName="Metrc Api key"
                value={fields['metrcApiKey']}
                errors={errors}
                onChange={changeHandler}
                disabled={lockForm}
                className={styles['api-settings__formfield']}
              />
            </div>
            <div className="col4">
              <Button type="submit" color="" width="width">Save</Button>
            </div>
          </div>
        </div>
      </div>
    </form>
  );
};