import { FC, useState, useEffect } from "react";
import { useDispatch } from "react-redux";
import classNames from "classnames";

import { Button } from "components/Button";
import { TextField } from "components/TextField";
import { Spinner } from "components/Spinner";
import { validHandler } from "utils/validation";
import { CleanButton } from "components/CleanButton";
import {
  setUserUpdate,
  uploadAvatar,
} from "services/api";
import { SettingTitle } from "features/Settings/SettingTitle";
import { AddMediaIcon } from "assets/img/icons";
import { DefaultAvatarIcon } from "assets/img/icons";
import { UserType } from "pages/settings";
import { userGet, userPut } from "redux/actions/userActions";
import imgResize from "utils/imgResizer";

import DeleteIcon from "../icons/delete.svg";
import styles from "./userSettings.module.scss";
import { Panel2 } from "components/Panel2";

export const UserSettings: FC<UserType> = ({ user }) => {
  const dispatch = useDispatch();
  const [errors, setErrors] = useState([]);
  const [lockForm, setLockForm] = useState(false);
  const [needValid, setNeedValid] = useState(false);
  const [loading, setLoading] = useState(false);
  const [avatarLoading, setAvatarLoading] = useState(false);
  const [avatarError, setAvatarError] = useState("");
  const [fields, setFields] = useState({
    firstName: user.firstName,
    lastName: user.lastName,
    email: user.email,
  });

  useEffect(() => {
    if (needValid) {
      const validation = ["email"];
      const { errors } = validHandler(fields, validation);
      setErrors(errors);
    }
  }, [fields]);

  const changeHandler = (e) => {
    const f = { ...fields };
    f[e.target.name] = e.target.value;
    setFields(f);
  };

  const removeAvatar = () => {
    setAvatarLoading(true);
    setUserUpdate({ avatar: "" })
      .then(() => {
        return dispatch(userGet());
      })
      .then(() => {
        setAvatarLoading(false);
      })
      .catch((e) => {
        console.error(e);
      });
  };

  const saveUserInformation = (e) => {
    e.preventDefault();
    setNeedValid(true);

    setLockForm(true);
    setLoading(true);
    if (!errors.length) {
      setUserUpdate({ id: user.id, ...fields })
        .then(() => {
          return dispatch(userGet());
        })
        .then(() => {
          setLockForm(false);
          setLoading(false);
        })
        .catch((e) => {
          console.error(e);
        });
    }
  };

  const fileInputHandler = (e) => {
    e.preventDefault();
    const file = e.target.files[0];
    if (!file) return false;

    if (
      !file.type.includes("jpeg") &&
      !file.type.includes("jpg") &&
      !file.type.includes("png")
    ) {
      setAvatarError("File type must be jpeg or png");
      return false;
    }

    setAvatarLoading(true);
    setAvatarError("");
    const onChange = async (file) => {
      try {
        const resizedFile = await imgResize(file);
        const reader = new FileReader();
        reader.onloadend = () => {
          uploadAvatar(file, user.id)
            .then((res) => {
              dispatch(userPut(res));
            })
            .finally(() => {
              setAvatarLoading(false);
            });
        };
        reader.readAsDataURL(resizedFile);
      } catch (err) {
        console.error(err);
      }
    };

    onChange(e.target.files[0]);
  };

  return (
    <form onSubmit={saveUserInformation} className={styles["user-settings"]}>
      <div className={styles["user-settings__row"]}>
        <Panel2
          loading={avatarLoading}
          className={styles["user-settings__user-photo"]}
        >
          <div className={styles["user-settings__user-photo__name"]}>
            {fields["firstName"]} {fields["lastName"]}
          </div>
          <label className={styles["user-settings__user-photo__img"]}>
            <input type="file" onChange={fileInputHandler} />
            <div className={styles["user-settings__add-avatar"]}>
              <AddMediaIcon />
              Add avatar
            </div>
            {user.avatar ? (
              <img src={user.avatar} alt="" />
            ) : (
              <div className={styles["user-settings__default-avatar"]}>
                <DefaultAvatarIcon />
              </div>
            )}
          </label>
          {avatarError && <div className={styles["error"]}>{avatarError}</div>}
          {user.avatar && (
            <CleanButton icon={DeleteIcon} onClick={removeAvatar} color="red">
              Delete
            </CleanButton>
          )}
        </Panel2>

        <Panel2
          loading={loading}
          className={styles["user-settings__user-data"]}
        >
          <SettingTitle>User’s Info</SettingTitle>
          <div className={styles["user-settings__user-data__wrapper"]}>
            <TextField
              id="firstName"
              type="text"
              fieldName="First Name"
              value={fields["firstName"]}
              errors={errors}
              onChange={changeHandler}
              disabled={lockForm}
              className={styles["user-settings__user-data__formfield"]}
            />
            <TextField
              id="lastName"
              type="text"
              fieldName="Last Name"
              value={fields["lastName"]}
              errors={errors}
              onChange={changeHandler}
              disabled={lockForm}
              className={styles["user-settings__user-data__formfield"]}
            />
            <TextField
              id="email"
              type="email"
              fieldName="E-mail"
              value={fields["email"]}
              errors={errors}
              onChange={changeHandler}
              disabled={lockForm}
              className={styles["user-settings__user-data__formfield"]}
            />
            <Button type="submit" color="green" width="width">
              Save Changes
            </Button>
          </div>
        </Panel2>
      </div>
    </form>
  );
};
