import { FC, useState, useEffect } from "react";
import { useDispatch } from "react-redux";
import classNames from "classnames";

import { Button } from "components/Button";
import { TextField } from "components/TextField";
import { validHandler } from "utils/validation";
import { getUrlForUpload, putFileOnAmazon, setUserUpdate, uploadCompanyAvatar } from "services/api";
import { SelectField } from "components/SelectField";
import { STATES } from "utils/constants";
import { SettingTitle } from "features/Settings/SettingTitle";
import { CleanButton } from "components/CleanButton";
//import { TextEditor } from 'components/TextEditor';
import { Spinner } from "components/Spinner";
import { userGet, userPut } from "redux/actions/userActions";
import imgResize from "utils/imgResizer";
import { AddMediaIcon, DefaultAvatarIcon } from "assets/img/icons";
import { UserType } from "pages/settings";

import styles from "./companySettings.module.scss";
import DeleteIcon from "../icons/delete.svg";

export const CompanySettings: FC<UserType> = ({ user }) => {
  const dispatch = useDispatch();
  const [errors, setErrors] = useState([]);
  const [lockForm, setLockForm] = useState(false);
  const [needValid, setNeedValid] = useState(false);
  const [loading, setLoading] = useState(false);

  const [fields, setFields] = useState({
    companyName: user.companyName,
    position: user.position,
    companyAddress: user.companyAddress,
    city: user.city,
    state: user.state,
    zipCode: user.zipCode,
    phone: user.phone,
    ein: user.ein,
    tagline: user.tagline,
    companyDescription: user.companyDescription,
  });

  const [avatarLoading, setAvatarLoading] = useState(false);
  const [avatarError, setAvatarError] = useState("");

  useEffect(() => {
    if (needValid) {
      const validation = ["email"];
      const { errors } = validHandler(fields, validation);
      setErrors(errors);
    }
  }, [fields]);

  const changeHandler = (e) => {
    const f = { ...fields };
    f[e.target.name] = e.target.value;
    setFields(f);
  };

  const changeTextHandler = (name, value) => {
    const f = { ...fields };
    f[name] = value;
    setFields(f);
  };

  const saveCompanyInformation = (e) => {
    e.preventDefault();
    setNeedValid(true);
    setLockForm(true);
    setLoading(true);
    if (!errors.length) {
      setUserUpdate({ id: user.id, ...fields })
        .then(() => {
          setLockForm(false);
          setLoading(false);
        })
        .catch((e) => {
          console.error(e);
        });
    }
  };

  const removeAvatar = () => {
    setAvatarLoading(true);
    setUserUpdate({ companyAvatar: "" })
      .then(() => {
        return dispatch(userGet());
      })
      .then(() => {
        setAvatarLoading(false);
      })
      .catch((e) => {
        console.error(e);
      });
  };

  const fileInputHandler = (e) => {
    e.preventDefault();
    const file = e.target.files[0];
    if (!file) return false;

    if (
      !file.type.includes("jpeg") &&
      !file.type.includes("jpg") &&
      !file.type.includes("png")
    ) {
      setAvatarError("File type must be jpeg or png");
      return false;
    }

    setAvatarLoading(true);
    setAvatarError("");
    const onChange = async (file) => {
      try {
        const resizedFile = await imgResize(file);
        const reader = new FileReader();
        reader.onloadend = () => {
          uploadCompanyAvatar(file, user.id)
            .then((res) => {
              dispatch(userPut(res));
            })
            .finally(() => {
              setAvatarLoading(false);
            });
        };
        reader.readAsDataURL(resizedFile);
      } catch (err) {
        console.error(err);
      }
    };
    onChange(e.target.files[0]);
  };

  return (
    <form
      onSubmit={saveCompanyInformation}
      className={styles["company-settings"]}
    >
      <div className={styles["company-settings__row"]}>
        <div
          className={classNames(
            styles["company-settings__user-photo"],
            styles["overall-design"]
          )}
        >
          {avatarLoading && <Spinner overlay />}

          <div className={styles["company-settings__user-photo__name"]}>
            {fields["companyName"]}
          </div>
          <label className={styles["company-settings__user-photo__img"]}>
            <input type="file" onChange={fileInputHandler} />
            <div className={styles["company-settings__add-avatar"]}>
              <AddMediaIcon />
              Add avatar
            </div>
            {user.companyAvatar ? (
              <img src={user.companyAvatar} alt="" />
            ) : (
              <div className={styles["company-settings__default-avatar"]}>
                <DefaultAvatarIcon />
              </div>
            )}
          </label>
          {avatarError && <div className={styles["error"]}>{avatarError}</div>}
          {user.avatar && (
            <CleanButton icon={DeleteIcon} onClick={removeAvatar} color="red">
              Delete
            </CleanButton>
          )}
        </div>

        <div
          className={classNames(
            styles["company-settings__user-data"],
            styles["overall-design"]
          )}
        >
          <SettingTitle>Company Info</SettingTitle>
          {loading && <Spinner overlay />}
          <div className={styles["company-settings__wrapper"]}>
            <div className={styles["row"]}>
              <div className={styles["col6"]}>
                <TextField
                  id="companyName"
                  type="text"
                  fieldName="Company name"
                  value={fields["companyName"]}
                  errors={errors}
                  onChange={changeHandler}
                  disabled={lockForm}
                  className={styles["company-settings__user-data__formfield"]}
                />
                <TextField
                  id="position"
                  type="text"
                  fieldName="Position"
                  value={fields["position"]}
                  errors={errors}
                  onChange={changeHandler}
                  disabled={lockForm}
                  className={styles["company-settings__user-data__formfield"]}
                />
                <TextField
                  id="companyAddress"
                  type="text"
                  fieldName="Company address"
                  value={fields["companyAddress"]}
                  errors={errors}
                  onChange={changeHandler}
                  disabled={lockForm}
                  className={styles["company-settings__user-data__formfield"]}
                />
                <TextField
                  id="city"
                  type="text"
                  fieldName="City"
                  value={fields["city"]}
                  errors={errors}
                  onChange={changeHandler}
                  disabled={lockForm}
                  className={styles["company-settings__user-data__formfield"]}
                />
              </div>
              <div className={styles["col6"]}>
                <SelectField
                  id="state"
                  fieldName="State"
                  value={fields["state"]}
                  options={STATES}
                  errors={errors}
                  onChange={changeHandler}
                  disabled={lockForm}
                  className={styles["company-settings__user-data__formfield"]}
                />
                <TextField
                  id="zipCode"
                  type="text"
                  fieldName="Zip code"
                  value={fields["zipCode"]}
                  errors={errors}
                  onChange={changeHandler}
                  disabled={lockForm}
                  className={styles["company-settings__user-data__formfield"]}
                />
                <TextField
                  id="phone"
                  type="text"
                  fieldName="Phone"
                  value={fields["phone"]}
                  errors={errors}
                  onChange={changeHandler}
                  disabled={lockForm}
                  mask="+1 (999) 999-99-99"
                  className={styles["company-settings__user-data__formfield"]}
                />
                <TextField
                  id="ein"
                  type="text"
                  fieldName="EIN"
                  value={fields["ein"]}
                  errors={errors}
                  onChange={changeHandler}
                  disabled={lockForm}
                  mask="99-9999999"
                  className={styles["company-settings__user-data__formfield"]}
                />
              </div>
            </div>

            <TextField
              id="tagline"
              type="text"
              fieldName="Tagline"
              value={fields["tagline"]}
              errors={errors}
              onChange={changeHandler}
              disabled={lockForm}
              className={styles["company-settings__user-data__formfield"]}
            />

            <div className={styles["form-inventory__title"]}>Description</div>
            {/* <TextEditor
							id="companyDescription"
							fieldName=""
							value={fields['companyDescription']}
							onChange={changeTextHandler}
							disabled={lockForm}
						/> */}

            <div className={styles["rigth"]}>
              <Button type="submit" color="green" width="width">
                Save
              </Button>
            </div>
          </div>
        </div>
      </div>
    </form>
  );
};
