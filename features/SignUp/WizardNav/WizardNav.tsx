import { FC } from 'react';

import classes from 'utils/classNames';

import Checked from './Checked.svg';
import styles from "./wizardNav.module.scss";

export interface NavProps {
  icon: string,
  id: number;
  tab: number;
  tabValid: boolean[];
  setTab: (e: number) => void;
}

export const Nav: FC<NavProps> = ({ children, icon, id, tab, tabValid, setTab }) => {
  const Icon = icon;

  return (
    <li className={
      classes(
        styles['nav-item'],
        tab == id && styles.active
      )
    }
      onClick={() => setTab(id)}
    >
      <span className={styles["nav-title"]}>{children}</span>
      {tabValid[id - 1] &&
        <span className={styles["nav-checked"]}>
          <Checked />
        </span>
      }
      <span className={styles["nav-icon"]}>
        <Icon />
      </span>
    </li>
  );
};
