import { FC, useState, useEffect } from 'react';
import {
  CSSTransition,
  TransitionGroup
} from 'react-transition-group';
import classNames from "classnames";

import { TextField } from "components/TextField";
import { SelectField } from "components/SelectField";
import { FileField } from "components/FileField";
import { Button } from "components/Button";
import { Checkbox } from 'components/Checkbox';
import { Spinner } from "components/Spinner";
import { COMPANY_TYPES, LICENSE_DESIGNATIONS } from 'utils/constants';
import { PassDescription } from 'components/PassDescription';
import { DateField } from 'components/DateField';
import { getStates, verifyApiKey } from 'services/api';

import styles from "./wizardForm.module.scss";

export interface WizardFormProps {
  submitForm: (e: React.FormEvent<HTMLFormElement>) => void;
  tab: any;
  fields: any;
  errors: any;
  selectHandler: any;
  selectStateHandler: any;
  licenseTypes: string[];
  changeHandler: any;
  changeDateHandler: any;
  fileInputHandler: any;
  lockForm: boolean;
  setRegisterDisabled: (registerDisabled: boolean) => void;
}

export const WizardForm: FC<WizardFormProps> = ({
  submitForm,
  tab,
  fields,
  errors,
  selectHandler,
  selectStateHandler,
  licenseTypes,
  changeHandler,
  changeDateHandler,
  fileInputHandler,
  lockForm,
  setRegisterDisabled
}) => {

  const [states, setStates] = useState([]);
  const [verifyProcess, setVerifyProcess] = useState(false);
  const [verifySuccess, setVerifySuccess] = useState(false);
  const [verifyComplete, setVerifyComplete] = useState(false);
  const [agreement, setAgreement] = useState(true);
  const [apiKeyError, setApiKeyError] = useState([]);
  const [needValid, setNeedValid] = useState(false);

  useEffect(() => {
    getStates()
      .then(res => {
        setStates(res);
      })
      .catch(e => {
        console.error(e);
      });
  }, []);

  useEffect(() => {
    if(!agreement || !verifyProcess) {
      setRegisterDisabled(true);
    } else {
      setRegisterDisabled(false);
    }
  }, [agreement]);

  const validApiKeyField = () => {
    if(!fields.metrcApi.length) {
      setApiKeyError({ metrcApi: 'This field is required' });
      return false;
    }

    setApiKeyError({});
    return true;
  };

  useEffect(() => {
    if(needValid) validApiKeyField();
  }, [fields.metrcApi]);

  const verifyApiKeyHandler = () => {

    setNeedValid(true);

    if(!validApiKeyField()) return;

    setVerifyProcess(true);

    verifyApiKey(fields.metrcApi)
      .then(res => {
        setVerifySuccess(true);
        setRegisterDisabled(false);
      })
      .catch(err => {
        if(err.response.data.type == 'errorAutorization') {
          setVerifySuccess(false);
          setRegisterDisabled(true);
        }
      })
      .finally(() => {
        setVerifyComplete(true);
        setVerifyProcess(false);
      });
  };

  const renderVerifyResult = () => {
    if(!verifyComplete) return;
    if(verifySuccess) return <div>Api Key successfully verified!</div>;
    return <div>Api Key did not pass verification, check again</div>;
  };

  const transitionClasses = {
    enter: styles['fade-enter'],
    enterActive: styles['fade-enter-active'],
    enterDone: styles['fade-enter-done'],
    exit: styles['fade-exit'],
    exitActive: styles['fade-exit-active'],
    exitDone: styles['fade-exit-done']
  };

  return (
    <form onSubmit={submitForm} className={styles['wizard-form']}>
      <TransitionGroup>
        <CSSTransition
          key={tab}
          classNames={transitionClasses}
          addEndListener={(node, done) => {
            node.addEventListener('transitionend', done, false);
          }}
        >
          <div className={classNames(
            styles["tab-content"],
            { [styles['metrc-api']]: tab == 4 }
          )}>
            {tab == 1 &&
              <>
                <TextField
                  id="firstName"
                  type="text"
                  fieldName="First name"
                  value={fields['firstName']}
                  errors={errors}
                  onChange={changeHandler}
                  disabled={lockForm}
                />
                <TextField
                  id="lastName"
                  type="text"
                  fieldName="Last name"
                  value={fields['lastName']}
                  errors={errors}
                  onChange={changeHandler}
                  disabled={lockForm}
                />
                <TextField
                  id="email"
                  type="email"
                  fieldName="E-mail"
                  value={fields['email']}
                  errors={errors}
                  onChange={changeHandler}
                  disabled={lockForm}
                />
                <TextField
                  id="phone"
                  type="text"
                  fieldName="Phone number"
                  value={fields['phone']}
                  errors={errors}
                  onChange={changeHandler}
                  disabled={lockForm}
                  mask="+1 (999) 999-99-99"
                />
                <TextField
                  id="password"
                  type="password"
                  fieldName="Password"
                  value={fields['password']}
                  errors={errors}
                  onChange={changeHandler}
                  disabled={lockForm}
                />
                <TextField
                  id="repeatPassword"
                  type="password"
                  fieldName="Repeat password"
                  value={fields['repeatPassword']}
                  errors={errors}
                  onChange={changeHandler}
                  disabled={lockForm}
                />
                <PassDescription />
              </>
            }

            {tab == 2 &&
              <>
                <TextField
                  id="companyName"
                  type="text"
                  fieldName="Company name"
                  value={fields['companyName']}
                  errors={errors}
                  onChange={changeHandler}
                  disabled={lockForm}
                />
                <TextField
                  id="position"
                  type="text"
                  fieldName="Position"
                  value={fields['position']}
                  errors={errors}
                  onChange={changeHandler}
                  disabled={lockForm}
                />
                <div className={`${styles["field-group"]} ${styles.stuck}`}>
                  <TextField
                    id="companyAddress"
                    type="text"
                    fieldName="CompanyAddress"
                    value={fields['companyAddress']}
                    errors={errors}
                    onChange={changeHandler}
                    disabled={lockForm}
                  />
                  <TextField
                    id="city"
                    type="text"
                    fieldName="Сity"
                    value={fields['city']}
                    errors={errors}
                    onChange={changeHandler}
                    disabled={lockForm}
                  />
                </div>
                <div className={styles["field-group"]}>
                  <SelectField
                    id="state"
                    fieldName="State"
                    value={fields['state']}
                    options={states}
                    errors={errors}
                    onChange={selectStateHandler}
                    disabled={lockForm}
                  />
                  <TextField
                    id="zipCode"
                    type="text"
                    fieldName="ZipCode"
                    value={fields['zipCode']}
                    errors={errors}
                    onChange={changeHandler}
                    disabled={lockForm}
                  />
                </div>
                <TextField
                  id="ein"
                  type="text"
                  fieldName="EIN"
                  value={fields['ein']}
                  errors={errors}
                  onChange={changeHandler}
                  disabled={lockForm}
                  mask="99-9999999"
                />
                <SelectField
                  id="companyType"
                  isMulti
                  fieldName="Company type details"
                  multiValue={fields['companyType']}
                  options={COMPANY_TYPES}
                  errors={errors}
                  onChange={selectHandler}
                  disabled={lockForm}
                />
              </>
            }
            {tab == 3 &&
              <>
                <TextField
                  id="licenseNumber"
                  type="text"
                  fieldName="License number"
                  value={fields['licenseNumber']}
                  errors={errors}
                  onChange={changeHandler}
                  disabled={lockForm}
                  mask="a99-9999999-LIC"
                />
                <SelectField
                  id="licenseDesignation"
                  fieldName="License Designation"
                  value={fields['licenseDesignation']}
                  options={LICENSE_DESIGNATIONS}
                  errors={errors}
                  onChange={selectHandler}
                  disabled={lockForm}
                />
                <SelectField
                  id="licenseType"
                  fieldName="License type"
                  value={fields['licenseType']}
                  options={licenseTypes}
                  errors={errors}
                  onChange={selectHandler}
                  disabled={lockForm}
                />
                <DateField
                  id="issueDate"
                  fieldName="Issue date"
                  value={fields['issueDate']}
                  errors={errors}
                  onChange={changeDateHandler}
                  disabled={lockForm}
                  mask="99/99/9999"
                />
                <DateField
                  id="expirationDate"
                  fieldName="Expiration date"
                  value={fields['expirationDate']}
                  errors={errors}
                  onChange={changeDateHandler}
                  disabled={lockForm}
                  mask="99/99/9999"
                />
                <FileField
                  id="licenseFile"
                  fieldName="License file"
                  file={fields['licenseFile']}
                  errors={errors}
                  onChange={fileInputHandler}
                  disabled={lockForm}
                />
              </>
            }
            {tab == 4 &&
              <div className={styles['metrc-block']}>
                {verifyProcess && <Spinner overlay />}
                <div className={styles['metrc-block__title']}>Please verify  your account to get started!</div>
                <div className={styles['metrc-block__description']}>Wholesale requires all clients  to verify  with metrc to confirm license information</div>

                <TextField
                  id="metrcApi"
                  type="text"
                  fieldName="Metrc API Key"
                  value={fields['metrcApi']}
                  errors={apiKeyError}
                  onChange={changeHandler}
                  disabled={lockForm}
                />

                {renderVerifyResult()}

                <Button
                  width="width"
                  color="green"
                  onClick={verifyApiKeyHandler}
                >
                  Verify
                </Button>

                <Checkbox
                  id="agreement"
                  title="By continuing, you agree to the terms of the agreement"
                  checked={agreement}
                  onChange={e => setAgreement(e.target.checked)}
                  className={styles['tab-content__agreement']}
                />

              </div>
            }
          </div>
        </CSSTransition>
      </TransitionGroup>
    </form>
  );
};
