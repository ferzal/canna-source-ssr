module.exports = {
  webpack(config) {
    config.module.rules.push({
      test: /\.svg$/,
      issuer: {
        and: [/\.(js|ts)x?$/],
      },

      use: ["@svgr/webpack"],
    });

    return config;
  },
  images: {
    domains: ["lh3.googleusercontent.com", "csdir-resized.s3.us-east-1.amazonaws.com"],
  },
  typescript: {
    ignoreBuildErrors: true
  }
};
