import { useState, useEffect } from "react";
import { useRouter } from "next/router";

import { getInventoryOne } from "services/api";
import { Tabs } from "components/Tabs";
import { BreadCrumbs } from "components/BreadCrumbs";
import { Panel2 } from "components/Panel2";
import { FormInventory } from "features/Inventory/FormInventory";
import { MediaInventory } from "features/Inventory/MediaInventory";

import styles from "./inventoryOne.module.scss";
import { GoBack } from "components/GoBack";
import { Content } from "layout/Content";

const InventoryOne = () => {
  const router = useRouter();

  const { id } = router.query;

  const [inventory, setInventory] = useState({});
  const [loading, setLoading] = useState(true);

  const [selectedTab, setSelectedTab] = useState(0);

  const breadcrumbs = [
    {
      title: "Inventory",
      link: "/inventory",
    },
    {
      title: inventory.name,
    },
  ];

  const getAndSetInventory = () => {
    const unmounted = false;
    getInventoryOne(id)
      .then((inventory) => {
        if (!unmounted) {
          setInventory(inventory);
        }
      })
      .catch((e) => {
        console.error(e);
      })
      .finally(() => {
        setLoading(false);
      });
  };

  useEffect(() => {
    if (!id) return;
    getAndSetInventory();
  }, [id]);

  const onTabChange = (tab: number) => {
    setSelectedTab(tab);
  };

  return (
    <Content>
      <div className={styles["inventory-one"]}>
        <BreadCrumbs breadcrumbs={breadcrumbs} />

        <GoBack />

        <div className="block_top">
          <div className="block_right">
            <div>
              <h1>{inventory.name}</h1>
            </div>
          </div>
        </div>
        <Tabs tabs={["General Info", "Media"]} onChangeTab={onTabChange} selectedTabId={selectedTab}>
          <Panel2 loading={loading}>
            <FormInventory inventory={inventory} />
          </Panel2>
          <Panel2 className={styles["inventory-one__media"]}>
            <MediaInventory
              id={id}
              media={inventory.media}
              getAndSetInventory={getAndSetInventory}
            />
          </Panel2>
        </Tabs>
      </div>
    </Content>
  );
};

export default InventoryOne;
