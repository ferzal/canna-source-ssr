import { useState, useEffect } from "react";

import { BreadCrumbs } from "components/BreadCrumbs";
import { GoBack } from "components/GoBack";
import { Content } from "layout/Content";

import styles from "./inventoryOne.module.scss";
import { Panel2 } from "components/Panel2";
import { FormInventory } from "features/Inventory/FormInventory";

const inventory = {
  id: '',
  name: '',
  marketplaceName: '',
  price: 0,
  thc: 0,
  category: '',
  isPublished: false,
  description: '',
  showQuantity: true,
  negotiable: false,
  oldPrice: 0,
  strainClassification: '',
  growType: '',
  sizeCategory: '',
  ingredients: []
};

const InventoryCreate = () => {

  const breadcrumbs = [
    {
      title: "Inventory",
      link: "/inventory",
    },
    {
      title: 'Create new Inventory',
    },
  ];

  return (
    <Content>
      <div className={styles["inventory-one"]}>
        <BreadCrumbs breadcrumbs={breadcrumbs} />

        <GoBack />

        <div className="block_top">
          <div className="block_right">
            <div>
              <h1>Create new Inventory</h1>
            </div>
          </div>
        </div>

        <Panel2>
          <FormInventory inventory={inventory} />
        </Panel2>

      </div>
    </Content>
  );
};

export default InventoryCreate;
