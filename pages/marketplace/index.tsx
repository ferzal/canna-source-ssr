import React, { useState, useEffect } from "react";
import { useDispatch } from "react-redux";
import { useRouter } from "next/router";
import { wrapper } from 'redux/store';
import { END } from 'redux-saga';
import InfiniteScroll from "react-infinite-scroll-component";
import { GetServerSideProps, InferGetServerSidePropsType } from "next";

import { userGet } from "redux/actions/userActions";
import { getMarketplace, getCategory } from "services/api";
import { MarketplaceGrid } from "features/Marketplace/MarketplaceGrid";
import { MarketplaceList } from "features/Marketplace/MarketplaceList";
import { Spinner } from "components/Spinner";
import { SwitchingView } from "components/SwitchingView";
//import classes from "../../utils/classNames";
import { addToCart } from "redux/actions/cartActions";
import { Container } from "reactstrap";
import { TextField } from "components/TextField";
import { RangeSlider } from "components/RangeSlider";
import { Checkbox } from "components/Checkbox";
import { SearchIcon } from "assets/img/icons";
import { SelectFieldSmall } from "components/SelectFieldSmall";
import { useLocalStorage } from "utils/useLocalStorage";

import styles from "./marketplace.module.scss";
import classes from "utils/classNames";
import { Content } from "layout/Content";

const PAGE_SIZE_MARKETPLACE = 10;

const SORT_OPTIONS = [
  { value: "name||ASC", label: "Name A-Z" },
  { value: "name||DESC", label: "Name Z-A" },
  { value: "price||ASC", label: "Price: Low to High" },
  { value: "price||DESC", label: "Price: High to Low" },
];

const MarketplacePage: InferGetServerSidePropsType<GetServerSideProps> = ({ query }) => {
  const router = useRouter();
  const params = { filter: {} };

  for(const p of Object.entries(query)) {
    const key = p[0].split('_');
    if(key[0] === 'filter') params.filter[key[1]] = p[1];
  }

  const dispatch = useDispatch();
  const [type, setType] = useState("list");
  const [sortBy, setSortBy] = useState(query.sortby ? query.sortby : "");
  const [sortDir, setSortDir] = useState(query.sortdir ? query.sortdir : "");
  const [loading, setLoading] = useState(false);
  const [marketplace, setMarketplace] = useState([]);
  const [marketplaceCount, setMarketplaceCount] = useState(0);
  const [hasMore, setHasMore] = useState(true);
  const [categories, setCategories] = useState([]);
  const [extreme, setExtreme] = useState([]);

  const [viewType, setViewType] = useLocalStorage('viewType', 'list');

  const [subCategories, setSubCategories] = useState([]);
  console.log(subCategories);
  const [typingTimeout, setTypingTimeout] = useState(0);

  const [filter, setFilter] = useState(params?.filter || {});
  const [search, setSearch] = useState(
    params.filter && params.filter.search ? params.filter.search : ""
  );

  useEffect(() => {
    if(!viewType) return;

    setType(viewType);
  }, []);

  useEffect(() => {
    getCategory().then((res) => {
      setCategories(res);
      const selectedParent = res.find((c) => c.id == params.filter["category"]);
      if(selectedParent && selectedParent.parent && selectedParent.parent.id) {
        //		setParentCategory(selectedParent.parent.id);
        const sub = res.filter(
          (c) => c.parent && c.parent.id === selectedParent.parent.id
        );
        setSubCategories(sub);
      }
    });
  }, []);

  const getAndSetMarketplace = (setLoading = null, reset = null) => {
    if(reset) {
      setHasMore(true);
    }

    getMarketplace({
      paginationCount: PAGE_SIZE_MARKETPLACE,
      paginationOffset: reset ? 0 : marketplace.length,
      sortBy,
      sortDir,
      filter,
    })
      .then(({ items, totalItems, extreme }) => {
        if(reset) {
          setMarketplace(items);
        } else {
          setMarketplace((prev) => [...prev, ...items]);
        }
        setMarketplaceCount(totalItems);
        setExtreme(extreme);
        if(setLoading) {
          setLoading(false);
        }
      })
      .catch(e => {
        console.error(e);
      });
  };

  useEffect(() => {

    const query = {};

    for(const f of Object.entries(filter)) {
      if(f[1]) query[`filter_${f[0]}`] = f[1];
    }

    if(sortBy) {
      query.sortby = sortBy;
      query.sortdir = sortDir;
    }

    if(query) {
      router.push({
        pathname: router.pathname,
        query
      });
    } else {
      router.push(router.pathname);
    }

    setLoading(true);
    getAndSetMarketplace(setLoading, true);
  }, [sortBy, sortDir, filter]);

  const onSort = (_, data) => {
    const sort = data.split("||");
    setSortBy(sort[0]);
    setSortDir(sort[1]);
  };

  const infiniteMoreHandler = () => {
    if(marketplace.length >= marketplaceCount) {
      setHasMore(false);
    }
    getAndSetMarketplace();
  };

  useEffect(() => {
    if(!type) return;

    setViewType(type);
  }, [type]);

  const addToCardHandler = (inventoryId: string, quantity: number) => {
    dispatch(addToCart({ inventoryId, quantity }));
  };

  const searchHandler = (e) => {
    if(typingTimeout) {
      clearTimeout(typingTimeout);
    }
    setSearch(e.target.value);
    setTypingTimeout(
      setTimeout(function() {
        const f = { ...filter };
        f[e.target.name] = e.target.value;
        setFilter(f);
      }, 600)
    );
  };

  const selectFilterHandler = (name, value) => {
    const f = { ...filter };
    if(value) {
      f[name] = value;
    } else {
      delete f[name];
    }
    setFilter(f);
  };

  const changeCategoryHandler = (name, value) => {
    const f = { ...filter };
    if(value) {
      f[name] = value;
    } else {
      delete f[name];
    }
    setFilter(f);
    setSubCategories(
      categories.filter((c) => c.parent && c.parent.id === value)
    );
  };

  const resetFilter = () => {
    setFilter({});
    setSubCategories([]);
    setSearch("");
    setSortBy("");
    setSortDir("");
    router.push(router.pathname);
  };

  return (
    <Content>
      <div className={styles.block_top}>
        <div className={styles.block_right}>
          <div>
            <h1>Marketplace</h1>
            <p className={styles.subtitle}></p>
          </div>
        </div>
      </div>

      <div className={classes(styles.marketplace, styles.marketplace__title)}>
        <span>Available for purchase</span>
        <div className={styles.marketplace__line}></div>
        <SwitchingView type={type} setType={setType} />
      </div>

      <div className={styles.marketplace__header}>
        <div className={styles.marketplace__filter}>
          <div className={styles["marketplace__filter-wrapper"]}>
            <div className={styles["marketplace__filter-search"]}>
              <TextField
                Icon={SearchIcon}
                id="search"
                type="text"
                fieldName="Search..."
                value={search}
                onChange={searchHandler}
              />
            </div>
            <div className={styles["marketplace__filter-fields"]}>
              <SelectFieldSmall
                id="category"
                width={210}
                fieldName="Category"
                value={filter["category"]}
                options={categories.filter((c) => !c.parent)}
                onChange={changeCategoryHandler}
              />
              {/* <SelectField
								id="category"
								fieldName="Category"
								value={filter['category']}
								options={subCategories}
								onChange={changeFilter}
							/> */}

              <RangeSlider
                id="price"
                fieldName="Price"
                unitsBefore="$"
                extreme={extreme.price}
                value={filter.price && filter.price.split(',')}
                onChange={selectFilterHandler}
              />
              <RangeSlider
                id="thc"
                fieldName="THC"
                unitsAfter="%"
                extreme={extreme.thc}
                value={filter.thc && filter.thc.split(',')}
                onChange={selectFilterHandler}
              />

              <Checkbox
                id="my"
                value="1"
                title="Show my inventory"
                onChange={(e) => selectFilterHandler("my", e.target.checked)}
                checked={filter["my"]}
              />

              {Object.entries(filter).length > 0 && (
                <div className={styles.reset} onClick={resetFilter}>
                  Reset filter
                </div>
              )}
            </div>
          </div>
          <div className={styles["marketplace__filter-found"]}>
            Found {marketplaceCount} items
          </div>
        </div>

        <div className={styles.marketplace__sort}>
          <SelectFieldSmall
            id="sort"
            width={210}
            fieldName="Sort"
            value={sortBy ? `${sortBy}||${sortDir}` : ""}
            options={SORT_OPTIONS}
            onChange={onSort}
          />
        </div>
      </div>

      {loading && <Spinner overlay />}
      <InfiniteScroll
        dataLength={marketplace.length}
        next={infiniteMoreHandler}
        hasMore={hasMore}
        loader={marketplace.length > 0 ? <Spinner /> : ""}
        className={styles.marketplace__wrap}
      >
        {marketplace.map((item) =>
          type === "list" ? (
            <MarketplaceList
              key={item.id}
              item={item}
              addToCard={addToCardHandler}
            />
          ) : (
            <MarketplaceGrid
              key={item.id}
              item={item}
              addToCard={addToCardHandler}
            />
          )
        )}
      </InfiniteScroll>
    </Content>
  );
};

export const getServerSideProps = wrapper.getServerSideProps(store => async ({ query }) => {

  if(!store.getState().user.id) {
    store.dispatch(userGet());
    store.dispatch(END);
  }

  await store.sagaTask.toPromise();

  return {
    props: { query }
  };
});


export default MarketplacePage;
