import { FC, useState, useEffect, ReactChildren } from 'react';
import { useRouter } from 'next/router';
import Link from "next/link";
import Lightbox from "react-image-lightbox";
import 'react-image-lightbox/style.css';
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

import { getInventoryOne } from 'services/api';
import { BreadCrumbs } from "components/BreadCrumbs";
import { Panel2 } from "components/Panel2";
import { GoBack } from "components/GoBack";
import { InventoryProps } from "features/Inventory/types";
import { AddToCartIcon, ArrowLeftIcon, ArrowRightIcon, NoPhotoIcon } from "assets/img/icons";
import { useDispatch } from "react-redux";
import { addToCart } from "redux/actions/cartActions";
import { Spinner } from "components/Spinner";
import { Button } from "components/Button";

import styles from './marketplaceOne.module.scss';
import { Content } from "layout/Content";

export interface SlickButtonFixType {
  children: ReactChildren;
  currentSlide: number;
  slideCount: number;
}

const SlickButtonFix: FC<SlickButtonFixType> = ({ children, currentSlide, slideCount, ...props }) => (
  <span {...props} currentslide={currentSlide} slidecount={slideCount}>{children}</span>
);

const SLIDER_SETTINGS = {
  dots: false,
  infinite: false,
  speed: 500,
  slidesToShow: 1,
  slidesToScroll: 1,
  prevArrow: (
    <SlickButtonFix>
      <ArrowLeftIcon />
    </SlickButtonFix >
  ),
  nextArrow: (
    <SlickButtonFix>
      <ArrowRightIcon />
    </SlickButtonFix >
  )
};

const SLIDER_SETTINGS2 = {
  dots: false,
  infinite: false,
  slidesToShow: 4,
  swipeToSlide: true,
  focusOnSelect: true,
  prevArrow: (
    <SlickButtonFix>
      <ArrowLeftIcon />
    </SlickButtonFix >
  ),
  nextArrow: (
    <SlickButtonFix>
      <ArrowRightIcon />
    </SlickButtonFix >
  )
};


const MarketplaceOne = () => {
  const router = useRouter();
  const dispatch = useDispatch();
  const { id } = router.query;
  const [inventory, setInventory] = useState<InventoryProps>({});
  const [loading, setLoading] = useState(true);
  const [nav1, setNav1] = useState(null);
  const [nav2, setNav2] = useState(null);
  const [slider1, setSlider1] = useState(null);
  const [slider2, setSlider2] = useState(null);
  const [isOpen, setIsOpen] = useState(false);
  const [photoIndex, setPhotoIndex] = useState(0);

  const onOpen = i => {
    setPhotoIndex(i);
    setIsOpen(true);
  };

  const getAndSetInventory = (callback = null) => {
    const unmounted = false;
    getInventoryOne(id)
      .then(inventory => {
        if(!unmounted) {
          setInventory(inventory);
          setLoading(false);
        }
      })
      .catch(e => {
        console.error(e);
      })
      .finally(() => {
        if(callback) {
          callback();
        }
      });
  };

  useEffect(() => {
    if(!id) return false;
    getAndSetInventory();
  }, [id]);

  useEffect(() => {
    setNav1(slider1);
    setNav2(slider2);
  }, [slider1, slider2]);

  const breadcrumbs = [
    {
      title: 'Marketplace',
      link: '/marketplace'
    },
    {
      title: inventory.name
    },
  ];

  const addToCardHandler = () => {
    dispatch(
      addToCart({ inventoryId: id, quantity: 1 })
    );
  };

  return (
    <Content>
      <div className={styles.product}>

        <BreadCrumbs
          breadcrumbs={breadcrumbs}
        />

        <GoBack />

        {inventory.my &&
          <Link href={`/inventory/${inventory.id}`} passHref><a>Edit inventory</a></Link>
        }

        <Panel2
          loading={loading}
        >

          <div className={styles['product__main']}>
            <div className={styles['product__left']}>

              <Slider
                {...SLIDER_SETTINGS}
                asNavFor={nav2}
                ref={slider => setSlider1(slider)}
                className={styles['product__slider']}
              >
                {inventory.media && inventory.media.map((image, i) =>
                  <img key={image.preview2} src={image.preview2} alt="" onClick={() => onOpen(i)} />
                )}
              </Slider>
              {inventory.media && inventory.media.length > 1 &&
                <Slider
                  {...SLIDER_SETTINGS2}
                  asNavFor={nav1}
                  ref={slider => setSlider2(slider)}
                  className={styles['product__slider-nav']}
                >
                  {inventory.media && inventory.media.map(image =>
                    <img key={image.preview} src={image.preview} alt="" />
                  )}
                </Slider>
              }
            </div>
            <div className={styles['product__right']}>
              <Link href={`/sellers/${inventory.user && inventory.user.id}`} passHref>
                <a className={styles['product__company-name']}>
                  {inventory.user && inventory.user.companyAvatar
                    ? <img src={inventory.user && inventory.user.companyAvatar} />
                    : <span className={styles['product__company-nophoto']}><NoPhotoIcon /></span>
                  } {inventory.user && inventory.user.companyName}
                </a>
              </Link>
              <h1 className={styles['product__name']}>
                {inventory.marketplaceName ? inventory.marketplaceName : inventory.name}
              </h1>
              <div className={styles['product__category']}>
                {inventory.categoryName}
              </div>
              {inventory.growType &&
                <div className={styles['product__category']}>
                  Flower Growing Operation: {inventory.growType}
                </div>
              }

              {inventory.sizeCategory &&
                <div className={styles['product__category']}>
                  Size Category: {inventory.sizeCategory}
                </div>
              }
              {inventory.showQuantity &&
                <div className={styles['product__quantity']}>
                  {inventory.quantity} / {inventory.unitOfMeasureName} Available
                </div>
              }
              <div className={styles['product__price']}>

                {inventory.oldPrice
                  ? <>
                    Price: <span className={styles['old_price']}>$ {inventory.oldPrice}</span>
                    $ {inventory.price}
                  </>
                  : <>
                    Price: $ {inventory.price}
                  </>
                } / {inventory.unitOfMeasureName == 'LBs' ? 'LB' : inventory.unitOfMeasureName} {inventory.negotiable && <>Negotiable</>}

              </div>
              <div className={styles['product__caracteristics']}>
                <ul>
                  <li>THC: {inventory.thc}%</li>
                  {inventory.coa &&
                    <li>COA: Yes</li>
                  }
                  {inventory.strainClassification &&
                    <li>Strain Classification: {inventory.strainClassification}</li>
                  }
                </ul>
              </div>

              <Button width="width" color="green" onClick={addToCardHandler} className={styles['product__add-cart']}>
                <AddToCartIcon />Add to Cart
              </Button>

              <div className={styles['product__description']}>
                <div dangerouslySetInnerHTML={{ __html: inventory.description }} />
              </div>
            </div>
          </div>

        </Panel2>

        {isOpen && (
          <Lightbox
            mainSrc={inventory.media[photoIndex].file}
            nextSrc={inventory.media[(photoIndex + 1) % inventory.media.length].file}
            prevSrc={inventory.media[(photoIndex + inventory.media.length - 1) % inventory.media.length].file}
            onCloseRequest={() => setIsOpen(false)}
            onMovePrevRequest={() => setPhotoIndex((photoIndex + inventory.media.length - 1) % inventory.media.length)}
            onMoveNextRequest={() => setPhotoIndex((photoIndex + 1) % inventory.media.length)}
          />
        )}

      </div>
    </Content>
  );
};

export default MarketplaceOne;