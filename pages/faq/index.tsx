import { FC, useEffect } from "react";
import { useRouter } from 'next/router';
import { ParsedUrlQuery } from "querystring";
import { GetServerSideProps } from "next";

interface FaqProps {
  query: ParsedUrlQuery;
}

const Faq: FC<FaqProps> = () => {
  const router = useRouter();

  useEffect(() => {
    router.push({
      pathname: 'faq/[id]',
      query: { id: 1 }
    });
  }, []);

  return <></>;
};

export const getServerSideProps: GetServerSideProps = async ({ query }: any) => {
  return {
    props: { query }
  };
};


export default Faq;