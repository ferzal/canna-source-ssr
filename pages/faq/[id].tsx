import { FC, useEffect, useState } from "react";
import { useRouter } from "next/router";
import { ParsedUrlQuery } from "querystring";
import { GetServerSideProps } from 'next';

import { Spinner } from "components/Spinner";
import { Tabs } from "components/Tabs";
import { FaqCategory } from "features/Faq/FaqCategory";
import { Content } from "layout/Content";
import { MobileTitle } from "components/MobileTitle/MobileTitle";
import { BreadCrumbs } from "components/BreadCrumbs";
import { useBreakpoint } from "utils/useBreakpoint";
import { getFaqCategories } from "services/api";

interface FaqProps {
  query: ParsedUrlQuery;
}
interface CategoryProps {
  id: string;
  name: string;
}

const Faq: FC<FaqProps> = ({ query }) => {
  const router = useRouter();
  const { isMobile } = useBreakpoint();
  const [loading, setLoading] = useState(false);
  const [categories, setCategories] = useState([]);
  const [selectedTab, setSelectedTab] = useState<number>(0);

  const getAndSetCategories = () => {
    setLoading(true);
    getFaqCategories()
      .then((res: any) => {
        setCategories(res);
        setSelectedTab(res.findIndex((item: CategoryProps) => item.id == query.id));
      })
      .catch(e => console.error(e))
      .finally(() => setLoading(false));
  };

  useEffect(() => {
    getAndSetCategories();
  }, []);

  const onTabChange = (tab: number) => {
    if(!categories[tab]) return;
    router.push({
      pathname: "[id]",
      query: { id: categories[tab].id },
    });
    setSelectedTab(tab);
  };

  return (
    <Content>
      {isMobile && (
        <BreadCrumbs
          breadcrumbs={[
            {
              title: "F.A.Q",
            },
          ]}
        />
      )}
      <div className="block_top">
        <div className="block_right">
          <div>
            <MobileTitle backRoute={"/"}>How can we help you?</MobileTitle>
          </div>
        </div>
      </div>

      {loading ? (
        <Spinner />
      ) : (
        <Tabs
          tabs={categories.map((item: CategoryProps) => item.name)}
          selectedTabId={selectedTab}
          onChangeTab={onTabChange}
        >
          <FaqCategory category={categories[selectedTab] && categories[selectedTab].id} />
        </Tabs>
      )}
    </Content>
  );
};

export const getServerSideProps: GetServerSideProps = async ({ query }) => {
  return {
    props: { query }
  };
};

export default Faq;
