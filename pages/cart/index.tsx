import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";

import { CartItem } from "features/CartItem";
import { Button } from "components/Button";
import { Spinner } from "components/Spinner";
import { CleanButton } from "components/CleanButton";
import { Panel } from "components/Panel";
import BasketIcon from "./Basket.svg";
import { cleanCart } from "redux/actions/cartActions";
import { createOrder } from "services/api";

import styles from "./cart.module.scss";
import { Content } from "layout/Content";

const Cart = () => {
  const dispatch = useDispatch();
  const user = useSelector((state) => state.user);
  const cart = useSelector((state) => state.cart);
  const [loading, setLoading] = useState(false);
  const [orderSuccess, setOrderSuccess] = useState(false);

  const cleanCartHandler = () => {
    dispatch(cleanCart());
  };

  const createOrderHandler = () => {
    setLoading(true);
    createOrder({ inventory: cart.items })
      .then(() => {
        setOrderSuccess(true);
        setLoading(false);
        dispatch(cleanCart());
      })
      .catch(e => {
        console.error(e);
      });
  };

  return (
    <Content>
      <div>
        <div className={styles["block_top"]}>
          <div className={styles["block_right"]}>
            <div>
              <h1>Shopping Cart</h1>
              <p className={styles["subtitle"]}></p>
            </div>
          </div>
        </div>

        <div className={styles["cart"]}>
          {(cart.cartLoading || loading) && <Spinner overlay />}

          {orderSuccess && <h3>Your order sucessfully created!</h3>}

          {cart.totalQuantity > 0 && !orderSuccess && (
            <>
              <div className={styles["cart__left-block"]}>
                <CleanButton
                  icon={BasketIcon}
                  onClick={cleanCartHandler}
                  className={styles["clean-button"]}
                >
                  Clean Cart
                </CleanButton>

                {cart.items.map((item) => (
                  <CartItem key={item.id} item={item} />
                ))}

                <div className={styles["cart__total"]}>
                  <span>Subtotal ({cart.totalQuantity} items): </span>${" "}
                  {cart.totalCost.toLocaleString("en-US")}
                </div>
              </div>
              <div className={styles["cart__right-block"]}>
                <Panel className={styles.panel}>
                  <div className={styles["cart__order-title"]}>
                    Delivery Address:
                  </div>
                  <div className={styles["cart__order-data"]}>
                    {" "}
                    {user.companyAddress} {user.city} {user.state}{" "}
                    {user.zipCode}{" "}
                  </div>
                </Panel>
                <Panel>
                  <div className={styles["cart__order-title"]}>Your Order:</div>
                  <div className={styles["cart__order-data bottom-line"]}>
                    {cart.items.map((item) => (
                      <div key={item.id} className={styles["cart__order-item"]}>
                        {item.inventory.name} x {item.quantity}
                      </div>
                    ))}
                  </div>
                  <div className={styles["cart__order-subtotal"]}>
                    <span>SubTotal:</span> ${" "}
                    {cart.totalCost.toLocaleString("en-US")}
                  </div>
                  <div className={styles["cart__order-button"]}>
                    <Button color="green" onClick={createOrderHandler}>
                      Order Now
                    </Button>
                  </div>
                </Panel>
              </div>
            </>
          )}

          {!cart.cartLoading && cart.totalQuantity == 0 && !orderSuccess && (
            <div className={styles["cart__empty"]}>Your cart is empty!</div>
          )}
        </div>
      </div>
    </Content>
  );
};

export const getServerSideProps = async ({ query }) => {
  return {
    props: { query }
  };
};

export default Cart;
