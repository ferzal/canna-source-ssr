import React from "react";
import { END } from "redux-saga";
import { wrapper } from "redux/store";

import { userGet } from "redux/actions/userActions";
import { Plans } from "features/Plans";
import { GoBack } from "components/GoBack";
import { Content } from "layout/Content";

const PlansPage = () => {
  return (
    <Content>
      <div>
        <div className="block_top">
          <div className="block_right">
            <div>
              <h1>Upgrade your plan</h1>
              <p className="subtitle">
                Get more out of Wholesale by switching to the right plan for
                you.
              </p>
            </div>
          </div>
        </div>

        <GoBack />

        <Plans />
      </div>
    </Content>
  );
};

export const getServerSideProps = wrapper.getServerSideProps(
  (store) =>
    async ({ query }) => {
      if (!store.getState().user.id) {
        store.dispatch(userGet());
        store.dispatch(END);
      }

      await store.sagaTask.toPromise();

      return {
        props: { query },
      };
    }
);

export default PlansPage;
