import { useEffect } from "react";
import { useDispatch } from "react-redux";
import { useRouter } from "next/router";
import { GetServerSideProps, InferGetServerSidePropsType } from "next";

import { Chat, ChatsList } from "features/Messages";
import { addChat } from "services/api";
import { messagesSetChatId } from "redux/actions/messagesActions";

import styles from "./messages.module.scss";


const Messages: InferGetServerSidePropsType<GetServerSideProps> = ({ query }) => {

  const dispatch = useDispatch();

  const router = useRouter();

  useEffect(() => {
    if(!query.userId) return;
    addChat({ interlocutorId: query.userId })
      .then(res => {
        dispatch(messagesSetChatId(res.chatId));
        router.push(`messages?chatId=${res.chatId}`);
      })
      .catch(e => console.error(e));

  }, [query.userId]);

  useEffect(() => {
    if(!query.chatId) return;
    dispatch(messagesSetChatId(query.chatId));
  }, [query.chatId]);

  return (
    <div className={styles.wrapper}>
      <ChatsList />
      <Chat />
    </div>
  );
};

export const getServerSideProps = ({ query, req }: any) => {

  const isFirstServerCall = req?.url?.indexOf('/_next/data/') === -1;

  if(!isFirstServerCall) {
    return {
      props: {
        query: { ...query, chatId: null }
      }
    };
  }
  return {
    props: { query }
  };
};

export default Messages;
