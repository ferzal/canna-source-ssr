import { FC } from "react";
import { useState, useEffect } from "react";
import Link from 'next/link';

import { resetPassword } from "services/api";
import { validHandler } from "utils/validation";
import { Button } from "components/Button";
import { PassDescription } from "components/PassDescription";
import { TextField } from "components/TextField";

import styles from "./resetPassword.module.scss";

interface SetPasswordProps {
  code: string;
}

const SetPassword: FC<SetPasswordProps> = ({ code }) => {
  const [errors, setErrors] = useState([]);
  const [errorReset, setErrorReset] = useState(false);
  const [password, setPassword] = useState('');
  const [repeatPassword, setRepeatPassword] = useState('');
  const [lockForm, setLockForm] = useState(false);
  const [needValid, setNeedValid] = useState(false);
  const [success, setSuccess] = useState(false);
  useEffect(() => {
    if(needValid) {
      const validation = ['password', 'repeatPassword'];
      const { errors } = validHandler({ password, repeatPassword }, validation);
      setErrors(errors);
    }
  }, [password, repeatPassword]);

  const submitForm = e => {
    e.preventDefault();
    const { valid, errors } = validHandler({ password, repeatPassword }, ['password', 'repeatPassword']);
    setErrors(errors);
    if(!valid) {
      setNeedValid(true);
      return;
    }

    if(code) {
      setLockForm(true);
      resetPassword({ code, password })
        .then(res => {
          if(res == 400) {
            setNeedValid(false);
            setErrorReset(true);
            setPassword('');
            setRepeatPassword('');
          } else {
            setSuccess(true);
          }
        })
        .catch(e => {
          console.error(e);
        }).finally(() => {
          setLockForm(false);
        });
    }

  };

  if(success)
    return <h3>Your password is sucessfully changed. please <Link href="/sign-in" passHref><a>sign in</a></Link></h3>;

  return (
    <div className={styles['form-block']}>
      <form action="" onSubmit={submitForm}>
        <TextField
          id="password"
          type="password"
          fieldName="Password"
          value={password}
          errors={errors}
          disabled={lockForm}
          onChange={e => setPassword(e.target.value)}
        />
        <TextField
          id="repeatPassword"
          type="password"
          fieldName="Repeat Password"
          value={repeatPassword}
          errors={errors}
          disabled={lockForm}
          onChange={e => setRepeatPassword(e.target.value)}
        />
        <PassDescription />
        {errorReset && <span className="error">User is not found or activation code not valid</span>}
        <div className={styles['signin__button-group']}>
          <Button
            type="submit"
            color="green"
            disabled={lockForm}
          >
            Set password
          </Button>
        </div>
      </form>
    </div>
  );

};

export default SetPassword;