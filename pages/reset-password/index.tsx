import { FC } from "react";
import { ParsedUrlQuery } from "querystring";

import { Main } from "layout/main";
import { Header } from "layout/header";

import styles from "./resetPassword.module.scss";
import SendLinkResetPassword from './sendLinkResetPassword';
import SetPassword from './setPassword';

interface ResetPasswordProps {
  query: ParsedUrlQuery;
}

const ResetPassword: FC<ResetPasswordProps> = ({ query }) => {
  return (
    <div className={styles["reset-password"]}>
      <Header page="signin" />
      <Main className={styles.main}>
        <div className={styles.container}>
          <h1>Reset password</h1>


          <p className="subtitle">Enter your email to reset your password</p>
          {/* {renderSuccess()} */}

          {query.code
            ? <SetPassword code={query.code} />
            : <SendLinkResetPassword />
          }
        </div>
      </Main>
    </div>
  );
};

export const getServerSideProps = ({ query }) => {
  return {
    props: { query }
  };
};

export default ResetPassword;
