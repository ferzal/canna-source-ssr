import { Button } from "components/Button";
import { TextField } from "components/TextField";
import { useState, useEffect } from "react";

import { sendLinkResetPassword } from "services/api";
import { validHandler } from "utils/validation";

import styles from "./resetPassword.module.scss";

const SendLinkResetPassword = () => {
  const [errors, setErrors] = useState([]);
  const [email, setEmail] = useState("");
  const [lockForm, setLockForm] = useState(false);
  const [needValid, setNeedValid] = useState(false);
  const [success, setSuccess] = useState(false);

  useEffect(() => {
    if (needValid) {
      const validation = ["email"];
      const { errors } = validHandler({ email }, validation);
      setErrors(errors);
    }
  }, [email]);

  const submitForm = (e) => {
    e.preventDefault();
    const { valid, errors } = validHandler({ email }, ["email"]);
    setErrors(errors);
    if (!valid) {
      setNeedValid(true);
    } else {
      setLockForm(true);
      sendLinkResetPassword({ email }).then(() => {
        setSuccess(true);
      });
    }
  };

  if (success)
    return <h3>We have send on e-mail a link to reset your password</h3>;

  return (
    <div className={styles["form-block"]}>
      <form action="" onSubmit={submitForm}>
        <TextField
          id="email"
          type="email"
          fieldName="E-mail"
          value={email}
          errors={errors}
          disabled={lockForm}
          onChange={(e) => setEmail(e.target.value)}
        />

        <div className={styles["reset-password__button-group"]}>
          <Button type="submit" color="green" disabled={lockForm}>
            Reset password
          </Button>
        </div>
      </form>
    </div>
  );
};

export default SendLinkResetPassword;
