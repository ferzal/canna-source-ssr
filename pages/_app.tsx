import type { AppProps } from "next/app";
import { useEffect } from "react";
import { wrapper } from "redux/store";
import Head from "next/head";
import { useDispatch } from "react-redux";

import { Dashboard } from "layout/Dashboard";
import { userGet } from "redux/actions/userActions";
import { cartGet } from "redux/actions/cartActions";

import "./global.scss";

const WITHOUT_DASHBOARD: string[] = [
  "/sign-in",
  "/sign-up",
  "/reset-password",
  "/select-plan",
];

function App({ Component, pageProps, router }: AppProps) {
  if (typeof window !== "undefined") {
    if (!localStorage.getItem("access_token")) {
      //  router.push('/sign-in');
      //return null;
    }
  }

  const dispatch = useDispatch();
  useEffect(() => {
    if (!WITHOUT_DASHBOARD.find((v) => v === router.route)) {
      dispatch(userGet(router));
      dispatch(cartGet());
    }
  }, []);
  const withLayout = () => {
    if (WITHOUT_DASHBOARD.find((v) => v === router.route))
      return <Component {...pageProps} />;

    return (
      <Dashboard path={router.route}>
        <Component {...pageProps} />
      </Dashboard>
    );
  };

  return (
    <>
      <Head>
        <link rel="stylesheet" href="https://use.typekit.net/rxz4lnp.css" />
        <link rel="icon" href="/favicon.svg" />
      </Head>
      {withLayout()}
    </>
  );
}

export default wrapper.withRedux(App);
