import { useState, useEffect, MouseEvent } from "react";
import { useRouter } from "next/router";

import { getUsersOne } from "services/api";
import { Spinner } from "components/Spinner";
import { GoBack } from "components/GoBack";
import { Button } from "components/Button";
import { BreadCrumbs } from "components/BreadCrumbs";
import { ContactInfo } from "features/Sellers/ContactInfo/ContactInfo";

import styles from "./sellerOne.module.scss";
import { Content } from "layout/Content";

const SellerOne = () => {
  const router = useRouter();
  const { id } = router.query;
  const [user, setUser] = useState<any>({});
  const [loading, setLoading] = useState(true);

  const breadcrumbs = [
    {
      title: "Sellers",
      link: "/sellers",
    },
    {
      title: user.companyName,
    },
  ];

  const getAndSetUser = () => {
    const unmounted = false;
    getUsersOne(id)
      .then((user) => {
        if(!unmounted) {
          setUser(user);
          setLoading(false);
        }
      })
      .catch(e => {
        console.error(e);
      });
  };

  useEffect(() => {
    if(!id) return;
    getAndSetUser();
  }, [id]);

  const copyLink = (e: MouseEvent<HTMLElement>) => {
    e.preventDefault();
    navigator.clipboard.writeText(window.location.href);
    alert("Link copied successfully! You can send it to your clients");
  };

  return (
    <Content>
      <div className={styles["invoice"]}>
        <BreadCrumbs breadcrumbs={breadcrumbs} />

        <GoBack />

        {loading ? (
          <Spinner />
        ) : (
          <>
            <div className={styles["seller-one"]}>
              <div className={styles["seller-one_wrap"]}>
                <div className={styles["seller-one_left"]}>
                  <div className={styles["seller-one_image"]}>
                    <img src={user.companyAvatar} alt="" />
                  </div>
                </div>
                <div className={styles["seller-one_center"]}>
                  <a
                    href="#"
                    onClick={copyLink}
                    className={styles["seller-one_link"]}
                  >
                    Copy your link
                  </a>
                  <div className={styles["seller-one_top"]}>
                    <span>com</span> {user.licenseNumber}
                  </div>

                  <h1 className={styles["seller-one_title"]}>
                    {user.companyName}
                  </h1>
                  {user.tagline && (
                    <p className={styles["seller-one_subtitle"]}>
                      “{user.tagline}”
                    </p>
                  )}
                  {user.companyDescription && (
                    <div
                      className={styles["seller-one_content"]}
                      dangerouslySetInnerHTML={{
                        __html: user.companyDescription,
                      }}
                    />
                  )}
                </div>
                <div className={styles["seller-one_right"]}>
                  <ContactInfo
                    web="presidentialca.com"
                    phone={user.phone}
                    location={user.companyAddress}
                  />
                  <Button color="" type="button" width="">
                    Message
                  </Button>
                </div>
              </div>
            </div>
            <div className={styles["featured-products"]}>
              <div className={styles["featured-products__left"]}>
                <div className={styles["featured-products__top"]}>
                  <span>Featured products</span>
                  <div className={styles["featured-products__line"]}></div>
                  <button className={styles["button-arrow"]}></button>
                </div>
              </div>
              <div className={styles["featured-products__right"]}></div>
            </div>
          </>
        )}
      </div>
    </Content>
  );
};

export default SellerOne;
