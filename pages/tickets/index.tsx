import React, { useState, useEffect, ChangeEvent } from 'react';
import { GetServerSideProps, InferGetServerSidePropsType } from "next";
import { useRouter } from "next/router";
import Link from 'next/link';

import { getTickets } from "services/api";
import { Spinner } from "components/Spinner";
import { TableItem } from "components/Table/TableItem";
import { Button } from "components/Button";
import { Pagination } from "components/Pagination";
import { Table } from "components/Table";
import { dateFormat } from "utils/format";
import { Content } from "layout/Content";

const Tickets: InferGetServerSidePropsType<GetServerSideProps> = ({ query }) => {
  const [tickets, setTickets] = useState([]);
  const [ticketsCount, setTicketsCount] = useState(0);
  const [currentPage, setCurrentPage] = useState(query.page ? Number(query.page) : 1);
  const [pageSize, setPageSize] = useState(10);
  const [loading, setLoading] = useState(false);
  const router = useRouter();

  const getAndSetTickets = () => {
    getTickets({
      paginationCount: pageSize,
      paginationOffset: (currentPage - 1) * pageSize,
      page: currentPage
    })
      .then(({ items, totalItems }: any) => {
        setTickets(items);
        setTicketsCount(totalItems);
      })
      .catch((e: any) => {
        console.error(e);
      }).
      finally(() => {
        setLoading(false);
      });
  };

  useEffect(() => {
    const query: any = {};

    if(currentPage !== 1) {
      query.page = currentPage;
    }

    if(query) {
      router.push({
        pathname: router.pathname,
        query
      });
    } else {
      router.push(router.pathname);
    }

    setLoading(true);
    getAndSetTickets();

  }, [currentPage, pageSize]);

  return (
    <Content>
      <div className="block_top">
        <div className="block_right">
          <div>
            <h1>Tickets</h1>
            <p className="subtitle">Tickets list</p>
          </div>
        </div>

        <div className="block_right">
          <Button width="width" onClick={() => router.push('/tickets/new')}>New ticket</Button>
        </div>
      </div>

      {loading && <Spinner overlay={true} />}

      <Table
        columns={[
          { title: 'Number' },
          { title: 'Subject' },
          { title: 'Created at' },
          { title: 'Last update' },
          { title: 'Department' },
          { title: 'Status' }
        ]}
        columnsWidth={[12, 28, 12, 12, 12, 12]}
      >
        {tickets!.map((item: any) =>
          <TableItem
            columnsWidth={[12, 28, 12, 12, 12, 24]}
            key={item.id}
          >
            <Link href={`${router.pathname}/${item.id}`} passHref><a>#{item.id}</a></Link>
            <Link href={`${router.pathname}/${item.id}`} passHref><a>{item.subject}</a></Link>
            {dateFormat(item.createdAt)}
            {dateFormat(item.updatedAt)}
            {item.department}
            {item.status}
          </TableItem>
        )}

      </Table>

      <Pagination
        totalCount={ticketsCount}
        currentPage={currentPage}
        pageSize={pageSize}
        setCurrentPage={setCurrentPage}
        onChangePageSize={(e: ChangeEvent<HTMLSelectElement>) => setPageSize(Number(e.target.value))}
      />
    </Content>
  );
};

export const getServerSideProps = ({ query }) => {
  return {
    props: { query }
  };
};


export default Tickets;