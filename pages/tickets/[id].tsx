import { useState, useEffect } from "react";
import { useRouter } from "next/router";
import classNames from "classnames";
import Link from "next/link";

import { getTicket, getTicketComments, addTicketComment } from "services/api";
import { BreadCrumbs } from "components/BreadCrumbs";
import { Panel2 } from "components/Panel2";
import { GoBack } from "components/GoBack";
import { Content } from "layout/Content";
import { dateTimeFormat } from "utils/format";
import { Button } from "components/Button";
import { Spinner } from "components/Spinner";
import { TextArea } from "components/TextArea";

import styles from "./tickets.module.scss";
import { Attachment } from "features/Tickets/Attachments";

const TicketOne = () => {
  const router = useRouter();

  const { id } = router.query;

  const [ticket, setTicket] = useState<any>({});
  const [comment, setComment] = useState<any>("");
  const [ticketComments, setTicketComments] = useState<any>([]);
  const [loading, setLoading] = useState(true);
  const [loadingComments, setLoadingComments] = useState(true);

  const breadcrumbs = [
    {
      title: "Tickets",
      link: "/tickets",
    },
    {
      title: `Ticket #${ticket.id || ""}`,
    },
  ];

  const getAndSetTicket = () => {
    const unmounted = false;
    getTicket(id)
      .then((ticket: any) => {
        if (!unmounted) {
          setTicket(ticket);
        }
      })
      .catch((e) => {
        console.error(e);
      })
      .finally(() => setLoading(false));
  };

  const getAndSetTicketComments = () => {
    setLoadingComments(true);
    const unmounted = false;
    getTicketComments(id)
      .then((comments: any) => {
        console.log(comments);

        if (!unmounted) {
          setTicketComments(comments.items);
        }
      })
      .catch((e) => {
        console.error(e);
      })
      .finally(() => {
        setLoadingComments(false);
      });
  };

  useEffect(() => {
    if (!id) return;
    getAndSetTicket();
    getAndSetTicketComments();
  }, [id]);

  const isMine = (authorId: any) => {
    return ticket.requesterId === authorId;
  };

  const commentHandler = () => {
    if (!comment) return;
    setLoadingComments(true);
    addTicketComment({ id, comment, authorId: ticket.requesterId })
      .then((res) => {
        console.log(res);
        getAndSetTicketComments();
      })
      .catch((e) => {
        console.error(e);
        setLoadingComments(false);
      })
      .finally(() => setComment(""));
  };

  const renderAttachments = (attachments: any) => {
    return attachments.map((file: any, i: number) => (
      <Link href={file.url} key={i} passHref>
        <a target="_blank" rel="noreferrer">
          <Attachment key={file.id} file={file} />
        </a>
      </Link>
    ));
  };

  const renderTicketComments = () => {
    return ticketComments.map((item: any) => (
      <div key={item.id}>
        <div
          className={classNames(styles.wrapper, {
            [styles.mine]: isMine(item.authorId),
          })}
        >
          <div
            dangerouslySetInnerHTML={{
              __html: item.htmlBody,
            }}
          />

          {ticket.requesterId === item.authorId && "isMine"}
          {dateTimeFormat(item.createdAt)}

          {renderAttachments(item.attachments)}
        </div>
      </div>
    ));
  };

  return (
    <Content>
      <div>
        <BreadCrumbs breadcrumbs={breadcrumbs} />

        <GoBack />

        <div className="block_top">
          <div className="block_right">
            <div>
              <h1>Ticket #{ticket.id}</h1>
            </div>
          </div>
        </div>

        <Panel2 loading={loading}>
          <h3>Subject : {ticket.subject}</h3>
          <div>Status : {ticket.status}</div>
          <div>Department : {ticket.department}</div>
          <div>Description : {ticket.description}</div>

          <div>Attachments : {ticket.description}</div>
        </Panel2>

        <div>
          <TextArea
            id="comment"
            fieldName="Comment"
            onChange={(e) => setComment(e.target.value)}
            value={comment}
            className={styles.formfield}
          />

          <Button color="green" width="width" onClick={commentHandler}>
            Add comment
          </Button>
        </div>

        <div className={styles.messages}>
          {loadingComments && <Spinner overlay />}
          {renderTicketComments()}
        </div>
      </div>
    </Content>
  );
};

export default TicketOne;
