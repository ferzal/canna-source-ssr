import React, { useState, useEffect, ChangeEvent } from 'react';
import router from "next/router";

import { Button } from "components/Button";
import { TextField } from "components/TextField";
import { validHandler } from "utils/validation";
import { createTicket, uploadAttachments } from "services/api";
import { SelectField } from "components/SelectField";
import { TextArea } from "components/TextArea";
import { Content } from "layout/Content";
import { Spinner } from "components/Spinner";
import { fileSizeType } from "utils/fileSizeType";

import styles from './createTicket.module.scss';
import { DEPARTMENTS } from "features/Tickets/constants";
import AttachGreen from "./AttachGreen.svg";
import { Attachment } from "features/Tickets/Attachments";


const CreateTicket = () => {
  const [lockForm, setLockForm] = useState(false);
  const [needValid, setNeedValid] = useState(false);
  const [errors, setErrors] = useState([]);
  const [fields, setFields] = useState({ subject: '', groupId: '', comment: '' });
  const [fileError, setFileError] = useState('');
  const [attachments, setAttachments] = useState([]);
  const [loadingFiles, setLoadingFiles] = useState(false);

  const ticketHandler = e => {
    e.preventDefault();
    setNeedValid(true);
    const validation = ['subject', 'groupId', 'comment'];
    const { valid, errors } = validHandler(fields, validation);
    setErrors(errors);

    if(!valid) return false;
    setLockForm(true);
    createTicket({ ...fields, attachments })
      .then((res: any) => {
        setNeedValid(false);
        setFields({ subject: '', groupId: '', comment: '' });
        router.push(`/tickets/${res.id}`);
      })
      .catch(e => {
        console.error(e);
        setLockForm(false);
      });
  };

  useEffect(() => {
    if(needValid) {
      const validation = ['title', 'groupId', 'comment'];
      const { errors }: any = validHandler(fields, validation);
      setErrors(errors);
    }
  }, [fields]);

  const changeHandler = (e: ChangeEvent<HTMLInputElement> | ChangeEvent<HTMLTextAreaElement>) => {
    const f: any = { ...fields };
    f[e.target.name] = e.target.value;
    setFields(f);
  };

  const changeHandler2 = (name: string, value: string) => {
    const f: any = { ...fields };
    f[name] = value;
    setFields(f);
  };

  const uploadFiles = (files: any) => {
    setLoadingFiles(true);
    const formData = new FormData();
    files.forEach((file: File, i: number) => {
      if(file.size > 5000000) {
        setFileError('File must be less than 5 MB');
        return;
      }
      formData.append('attachments[]', file);
    });

    uploadAttachments(formData)
      .then((res: any) => {
        console.log(res);
        setAttachments(res);
      })
      .catch(e => {
        console.error(e);
      })
      .finally(() => setLoadingFiles(false));
  };

  const handleUpload = (e: ChangeEvent<HTMLInputElement>) => {
    const fileList: FileList = e.target.files;
    uploadFiles([...fileList]);
  };

  const removeFile = (i: number) => {
    setAttachments(prev => prev.filter((_, index) => index !== i));
  };

  const renderFiles = () => {
    return attachments.map((file: any, i: number) => <Attachment key={file.token} file={file} removeFile={() => removeFile(i)} />
    );
  };

  return (
    <Content>
      <div className={styles.createticket}>
        <div className="block_top">
          <div className="block_right">
            <div>
              <h1>New Ticket</h1>
              <p className="subtitle">Create new ticket</p>
            </div>
          </div>
        </div>

        <form onSubmit={ticketHandler}>

          {(lockForm || loadingFiles) &&
            <Spinner overlay />
          }

          <TextField
            id="subject"
            type="text"
            fieldName="Subject"
            value={fields.subject}
            errors={errors}
            onChange={changeHandler}
            disabled={lockForm}
            className={styles.formfield}
          />

          <SelectField
            id="groupId"
            fieldName="Department"
            value={fields.groupId}
            errors={errors}
            options={DEPARTMENTS}
            onChange={changeHandler2}
            disabled={lockForm}
            className={styles.formfield}
          />

          <TextArea
            id="comment"
            fieldName="Comment"
            errors={errors}
            onChange={changeHandler}
            value={fields.comment}
            className={styles.formfield}
          />

          <label className={styles.label}>
            <input
              type="file"
              multiple
              id="uploadFiles"
              name="uploadFiles"
              onChange={handleUpload}
            />
            <AttachGreen />
            Attach File
          </label>

          <div className={styles.files}>
            {renderFiles()}
          </div>

          {fileError && <span className="error">{fileError}</span>}

          <div className={styles.bottom}>
            <Button
              type="submit"
              color="green"
              width="width"
            >
              Create ticket
            </Button>
          </div>
        </form>
      </div>
    </Content>
  );
};

export default CreateTicket;