import React, { useEffect } from "react";
import { useRouter } from 'next/router';

import { Plans } from "features/Plans";
import { useLocalStorage } from "utils/useLocalStorage";

import styles from './selectPlan.module.scss';

const SelectPlan = () => {

  const router = useRouter();
  const [notShowPlans, setNotShowPlans] = useLocalStorage('notShowPlans', false);

  useEffect(() => {
    if(notShowPlans) router.push('/');
  }, []);

  const laterHandler = () => {
    setNotShowPlans(true);
    router.push('/');
  };

  return (
    <div className={styles['select-plan']}>

      <h1>Select Plan</h1>
      <p>After registration, you have 2 weeks of free use</p>

      <Plans />

      <span className={styles['select-plan__later']} onClick={laterHandler}>maybe later</span>
    </div >
  );
};
export default SelectPlan;