import { useState, useEffect } from 'react';
import { useRouter } from 'next/router';
import Link from 'next/link';
import { END } from 'redux-saga';

import { Button } from "components/Button";
import { userGet } from "redux/actions/userActions";
import { wrapper, AppState } from "redux/store";
import { Content } from "layout/Content";

import styles from './home.module.scss';


const ACTIONS = [
  {
    title: 'Refer a Cultivator',
    button: 'Refer Cultivator',
    description: 'Know any farms that need help finding buyers for their bulk product? Refer a cultivator and get a $500 credit.'
  },
  {
    title: 'Refer a Distributor',
    button: 'Refer Distributor',
    description: 'Know any brands or distributors in need of product? Refer a distributor and get a $500 credit.'
  }
];

const renderActions = () => ACTIONS.map(v => <div key={v.title} className={styles['actions__card']}>
  <div className={styles['actions__card-title']}>
    {v.title}
  </div>
  <div className={styles['actions__card-description']}>
    {v.description}
  </div>
  <Button color="green">{v.button}</Button>
</div>);

const Home = () => <Content>
  <div className={styles.dashboard}>
    <div>
      <h1>Dashboard</h1>
      <p className="subtitle">Welcome to Canna Source Direct!</p>
    </div>
    <div className={styles.actions}>
      <div className={styles['actions__title-block']}>
        <div className={styles['actions__title']}>Actions</div>
      </div>
      <div className={styles['actions__content-block']}>
        {renderActions()}
      </div>
    </div>
  </div>
</Content>;

export const getServerSideProps = wrapper.getServerSideProps(store => async ({ query, req, res }) => {
  return {
    props: { query }
  };
});

export default Home;