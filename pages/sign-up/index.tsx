import React, { useState, useEffect } from "react";
import { CSSTransition, TransitionGroup } from "react-transition-group";
import Link from "next/link";
import classNames from "classnames";

import { Main } from "layout/main";
import { Header } from "layout/header";
import ArrowRightIcon from "assets/img/icons/ArrowRight.svg";
import classes from "utils/classNames";
import { validHandler } from "utils/validation";
import { WizardForm } from "features/SignUp/WizardForm";
import { Nav } from "features/SignUp/WizardNav";
import { getLicenseTypes, register } from "services/api";
import { CircleButton } from "components/CircleButton";
import { useLocalStorage } from "utils/useLocalStorage";

import { UserIcon, LicenseIcon, RefferalIcon, CompanyIcon } from "features/SignUp/icons";
import styles from "./signup.module.scss";

const INITIAL_FIELDS = {
  firstName: "",
  lastName: "",
  email: "",
  password: "",
  repeatPassword: "",
  companyName: "",
  position: "",
  companyAddress: "",
  city: "",
  state: "",
  zipCode: "",
  phone: "",
  ein: "",
  licenseNumber: "",
  licenseType: "",
  issueDate: "",
  expirationDate: "",
  licenseFile: "",
  hearAboutUs: "",
  referral: "",
  metrcApi: "",
};

const SignUp = () => {
  const [tab, setTab] = useState(1);
  const [successRegistered, setSuccessRegistered] = useState(false);
  const [lockForm, setLockForm] = useState(false);
  const [needValid, setNeedValid] = useState(false);
  const [tabValid, setTabValid] = useState([false, false, false, false]);
  const [errors, setErrors] = useState({});
  const [fields, setFields] = useState(INITIAL_FIELDS);
  const [registerDisabled, setRegisterDisabled] = useState(true);
  const [signUpFields, setLocalData] = useLocalStorage("signUpFields", fields);
  const [licenseTypes, setLicenseTypes] = useState([]);

  const changeTab = (id: number) => {
    let validation: string[] = [];

    if(id >= 2) {
      validation = [
        "firstName",
        "lastName",
        "email",
        "password",
        "repeatPassword",
      ];
    }
    if(id >= 3) {
      validation = [
        ...validation,
        "companyName",
        "position",
        "companyAddress",
        "city",
        "state",
        "zipCode",
        "phone",
        "ein",
      ];
    }

    if(id >= 4) {
      validation = [
        ...validation,
        "licenseNumber",
        "licenseType",
        "issueDate",
        "expirationDate",
        "licenseFile",
      ];
    }

    if(id > 4 && tabValid.map) {
      validation = [...validation, "metrcApi"];
    }

    const { valid, errors } = validHandler(fields, validation);

    if(!valid) {
      setNeedValid(true);
    }
    setErrors(errors);
    if(valid) {
      setTab(id);
      const tv = [...tabValid];
      if(id - 2 >= 0) {
        tv[id - 2] = true;
        setTabValid(tv);
      }
      setNeedValid(false);
    } else {
      const tv = [...tabValid];
      if(id - 2 >= 0) {
        tv[id - 2] = false;
        setTabValid(tv);
      }
    }
  };

  const submitForm = (e: React.MouseEvent<HTMLElement>) => {
    e.preventDefault();

    if(registerDisabled) return;

    setLockForm(true);
    register(fields)
      .then(() => {
        setSuccessRegistered(true);
      })
      .catch((err) => {
        console.error(err);
        console.error(err.response);
        console.error(err.response.data);
      })
      .finally(() => {
        setLockForm(false);
      });
  };

  const fileInputHandler = (e) => {
    const f = { ...fields };
    f[e.target.name] = e.target.files[0];
    setFields(f);
  };

  const changeHandler = (e) => {
    const f = { ...fields };
    f[e.target.name] = e.target.value;
    setFields(f);
  };

  const selectHandler = (name, value) => {
    const f = { ...fields };
    f[name] = value;
    setFields(f);
  };

  const getAndSetLicenseTypes = (value) => {
    getLicenseTypes(value)
      .then((res) => {
        setLicenseTypes(res);
      })
      .catch(e => {
        console.error(e);
      });
  };

  const selectStateHandler = (name, value) => {
    getAndSetLicenseTypes(value);
    const f = { ...fields };
    f[name] = value;
    setFields(f);
  };

  const changeDateHandler = (name, value) => {
    const f = { ...fields };
    f[name] = value;
    setFields(f);
  };

  useEffect(() => {
    if(needValid) {
      const validation = [
        "firstName",
        "lastName",
        "email",
        "password",
        "repeatPassword",
        "companyName",
        "position",
        "companyAddress",
        "city",
        "state",
        "zipCode",
        "phone",
        "ein",
        "licenseNumber",
        "licenseType",
        "issueDate",
        "expirationDate",
        "licenseFile",
        "metrcApi",
      ];
      const { errors } = validHandler(fields, validation);
      setErrors(errors);
    }
    const saveFields = { ...fields };

    saveFields.password = "";
    saveFields.repeatPassword = "";
    saveFields.licenseFile = "";

    setLocalData(saveFields);
  }, [fields]);

  useEffect(() => {
    if(!process.browser || !signUpFields) return;
    setFields(signUpFields);

    if(!signUpFields["state"]) return;
    getAndSetLicenseTypes(signUpFields["state"]);
  }, [process]);

  const transitionClasses = {
    enter: styles["fade-enter"],
    enterActive: styles["fade-enter-active"],
    enterDone: styles["fade-enter-done"],
    exit: styles["fade-exit"],
    exitActive: styles["fade-exit-active"],
    exitDone: styles["fade-exit-done"],
  };

  return (
    <div className={styles.signup}>
      <TransitionGroup className={styles.signup__anim}>
        <CSSTransition
          key={tab}
          classNames={transitionClasses}
          addEndListener={(node, done) => {
            node.addEventListener("transitionend", done, false);
          }}
        >
          <div className={classes(styles["signup__bg"], styles[`bg${tab}`])} />
        </CSSTransition>
      </TransitionGroup>

      <Header page="signup" />
      <Main className={styles.main}>
        <div className={styles.signup__steps}>
          <div className={styles["signup__steps-title"]}>Step {tab}/4</div>
          <ul className={styles.signup__nav}>
            <Nav
              icon={UserIcon}
              id={1}
              tab={tab}
              tabValid={tabValid}
              setTab={changeTab}
            >
              User information
            </Nav>
            <Nav
              icon={CompanyIcon}
              id={2}
              tab={tab}
              tabValid={tabValid}
              setTab={changeTab}
            >
              Company information
            </Nav>
            <Nav
              icon={LicenseIcon}
              id={3}
              tab={tab}
              tabValid={tabValid}
              setTab={changeTab}
            >
              License information
            </Nav>
            <Nav
              icon={RefferalIcon}
              id={4}
              tab={tab}
              tabValid={tabValid}
              setTab={changeTab}
            >
              Metrc API Verification
            </Nav>
          </ul>
        </div>
        <div className={styles.signup__content}>
          <h1>Register</h1>
          <p className="subtitle">
            Follow these 4 simple steps to create your account
          </p>
          <div className={styles["form-block"]}>
            {successRegistered && (
              <div>
                You are been succesfully registered! <br />
                <Link href="/sign-in" passHref>
                  <a>Go to login</a>
                </Link>
              </div>
            )}
            {!successRegistered && (
              <WizardForm
                submitForm={submitForm}
                tab={tab}
                fields={fields}
                errors={errors}
                lockForm={lockForm}
                changeHandler={changeHandler}
                selectHandler={selectHandler}
                selectStateHandler={selectStateHandler}
                changeDateHandler={changeDateHandler}
                fileInputHandler={fileInputHandler}
                licenseTypes={licenseTypes}
                setRegisterDisabled={setRegisterDisabled}
              />
            )}
          </div>
          {!successRegistered && (
            <div
              className={classes(
                styles["signup__nav-right"],
                lockForm && styles.disabled
              )}
            >
              {tab != 1 && (
                <span
                  className={styles.back}
                  onClick={() => changeTab(tab - 1)}
                >
                  Back
                </span>
              )}
              {tab < 4 && (
                <span
                  className={styles.next}
                  onClick={() => changeTab(tab + 1)}
                >
                  Next step
                  <CircleButton color="green" icon={ArrowRightIcon} />
                </span>
              )}
              {tab == 4 && (
                <span
                  className={classNames(styles.next, {
                    [styles.disabled]: registerDisabled,
                  })}
                  onClick={submitForm}
                >
                  Register
                  <CircleButton color="green" icon={ArrowRightIcon} />
                </span>
              )}
            </div>
          )}
        </div>
      </Main>
    </div>
  );
};

export default SignUp;
