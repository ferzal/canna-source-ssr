import { useState, useEffect } from 'react';
import { useSelector } from 'react-redux';
import { useRouter } from 'next/router';
import { GetServerSideProps, InferGetServerSidePropsType } from 'next';

import { AppState } from 'redux/store';
import { getCategory, getInventory, setPublished } from 'services/api';
import { TextField } from 'components/TextField';
import { SelectFieldSmall } from 'components/SelectFieldSmall';
import { SearchIcon } from 'assets/img/icons';
import { Pagination } from 'components/Pagination';
import { PageTop } from 'features/Inventory/PageTop';
import { InventoryItem } from 'features/Inventory/InventoryItem';
import { Spinner } from 'components/Spinner';
import { Table } from 'components/Table';
import { Content } from "layout/Content";

import styles from './userInventory.module.scss';

const UserInventory: InferGetServerSidePropsType<GetServerSideProps> = ({ query }) => {

  //const user = useSelector<AppState>(state => state.user!.user);
  const router = useRouter();

  const { id } = router.query;

  const params = { filter: {} };

  for(const p of Object.entries(query)) {
    const key = p[0].slice(0, -1).split('[');
    if(key[0] === 'filter') params.filter[key[1]] = p[1];
  }

  const [inventory, setInventory] = useState([]);
  const [sortBy, setSortBy] = useState('name');
  const [sortDir, setSortDir] = useState('ASC');
  const [inventoryCount, setInventoryCount] = useState(0);
  const [currentPage, setCurrentPage] = useState(query.page ? Number(query.page) : 1);
  const [pageSize, setPageSize] = useState(10);
  const [loading, setLoading] = useState(false);
  const [selectedItems, setSelectedItems] = useState([]);
  const [categories, setCategories] = useState([]);
  const [subCategories, setSubCategories] = useState([]);
  const [typingTimeout, setTypingTimeout] = useState(0);
  const [filter, setFilter] = useState(params.filter ? params.filter : {});
  const [search, setSearch] = useState(params.filter && params.filter.search ? params.filter.search : '');

  useEffect(() => {
    getCategory()
      .then(res => {
        setCategories(res);
        const selectedParent = res.find(c => c.id == params.filter['category']);
        if(selectedParent && selectedParent.parent && selectedParent.parent.id) {
          const sub = res.filter(c => c.parent && c.parent.id === selectedParent.parent.id);
        }
      })
      .catch(e => {
        console.error(e);
      });
  }, []);


  const getAndSetInventory = () => {
    if(!id) return null;
    getInventory({
      userId: id,
      paginationCount: pageSize,
      paginationOffset: (currentPage - 1) * pageSize,
      sortBy,
      sortDir,
      filter
    })
      .then(({ items, totalItems }: any) => {
        setInventory(items);
        setInventoryCount(totalItems);
        if(setLoading) {
          setLoading(false);
        }
      })
      .catch(e => {
        console.error(e);
      });
  };

  const onSavedMedia = () => {
    setLoading(true);
    getAndSetInventory();
  };

  useEffect(() => {
    const url = new URLSearchParams();
    const query = { id };
    if(currentPage !== 1) {
      //  url.append('page', currentPage);
      query.page = currentPage;
    }

    for(const f of Object.entries(filter)) {
      if(f[1]) {
        query[`filter[${f[0]}]`] = f[1];

      }
    }

    router.push({
      pathname: router.pathname,
      query
    });

    setLoading(true);
    getAndSetInventory();
    setSelectedItems([]);
  }, [currentPage, pageSize, sortBy, sortDir, filter]);


  const onSelectItem = (e, id) => {
    if(e.target.checked) {
      setSelectedItems(prev => [...prev, id]);
    } else {
      setSelectedItems(prev => prev.filter(i => i !== id));
    }
  };

  const onSelectAll = e => {
    if(e.target.checked) {
      setSelectedItems(inventory.map(item => item.id));
    } else {
      setSelectedItems([]);
    }
  };

  const setInventoryPublished = (id, isPublished) => {
    setPublished({
      ids: [id],
      isPublished: !isPublished
    })
      .then(() => {
        getAndSetInventory();
      })
      .catch(e => {
        console.error(e);
      });
  };

  const onSort = (sortBy) => {
    setSortBy(sortBy);
    setSortDir(prev => prev === 'ASC' ? 'DESC' : 'ASC');
  };

  const selectFilterHandler = (name, value) => {
    const f = { ...filter };
    if(value) {
      f[name] = value;
    } else {
      delete f[name];
    }

    setFilter(f);
  };

  const changeCategoryHandler = (name, value) => {
    const f = { ...filter };
    if(value) {
      f[name] = value;
    } else {
      delete f[name];
    }
    setFilter(f);
    setSubCategories(
      categories.filter(c => c.parent && c.parent.id === value)
    );
  };

  const searchHandler = e => {
    if(typingTimeout) {
      clearTimeout(typingTimeout);
    }
    setSearch(e.target.value);
    setTypingTimeout(setTimeout(function() {
      const f = { ...filter };
      f[e.target.name] = e.target.value;
      setFilter(f);
    }, 600));
  };

  const resetFilter = () => {
    setFilter({});
    setSubCategories([]);
    setSearch('');
    router.push(router.pathname);
  };

  return (
    <Content>
      <div className={styles.inventory}>

        <PageTop />
        <div className={styles['inventory__header']}>
          <div className={styles['inventory__filter']}>
            <div className={styles['inventory__filter-wrapper']}>
              <div className={styles['inventory__filter-search']}>
                <TextField
                  Icon={SearchIcon}
                  id="search"
                  type="text"
                  fieldName="Search..."
                  value={search}
                  onChange={searchHandler}
                />
              </div>
              <div className={styles['inventory__filter-fields']}>
                <SelectFieldSmall
                  id="status"
                  width={140}
                  fieldName="Status"
                  value={filter['status']}
                  options={[{ value: '1', label: 'Published' }, { value: '0', label: 'Unpublished' }]}
                  onChange={selectFilterHandler}
                  className={styles['filter-item']}
                />
                <SelectFieldSmall
                  id="category"
                  width={210}
                  fieldName="Category"
                  value={filter['category']}
                  options={categories.filter(c => !c.parent)}
                  onChange={changeCategoryHandler}
                  className={styles['filter-item']}
                />
                {Object.entries(filter).length > 0 && <div className="reset" onClick={resetFilter}>Reset filter</div>}
              </div>

            </div>
            <div className={styles['inventory__filter-found']}>
              Found {inventoryCount} items
            </div>
          </div>

          {/* {selectedItems.length > 0 &&
            <BulkUpdates
              categories={categories}
              selectedItems={selectedItems}
              setSelectedItems={setSelectedItems}
              setLoading={setLoading}
              setInventoryPublished={setInventoryPublished}
              getAndSetInventory={getAndSetInventory}
              userId={user.id}
            />
          } */}
        </div>

        {loading && <Spinner overlay={true} />}

        <Table
          columns={[
            { title: 'Product', sortable: true, sortBy: 'name' },
            { title: 'Price' },
            { title: 'Quantity' },
            { title: 'Category' },
            { title: 'Media' },
            { title: 'Last update', sortable: true, sortBy: 'name' }
          ]}
          columnsWidth={[25, 8, 12, 15, 23, 8, 7]}
          selectedItems={selectedItems}
          onSelectAll={onSelectAll}
          sortBy={sortBy}
          sortDir={sortDir}
          onSort={onSort}
        >
          <>
            {inventory.map(item =>
              <InventoryItem
                key={item.id}
                item={item}
                selectedItems={selectedItems}
                onSelectItem={onSelectItem}
                onSavedMedia={onSavedMedia}
                setInventoryPublished={setInventoryPublished}
              />
            )}
          </>
        </Table>

        <Pagination
          totalCount={inventoryCount}
          currentPage={currentPage}
          pageSize={pageSize}
          setCurrentPage={setCurrentPage}
          onChangePageSize={e => setPageSize(Number(e.target.value))}
        />

      </div>
    </Content>
  );
};

export const getServerSideProps = async ({ query }) => {
  return {
    props: {
      query
    }
  };
};

export default UserInventory;
