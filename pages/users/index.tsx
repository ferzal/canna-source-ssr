import { useState, useEffect } from 'react';
import { GetServerSideProps, InferGetServerSidePropsType } from 'next';
import { useRouter } from 'next/router';
import { useSelector } from 'react-redux';
import Link from 'next/link';

import { AppState, wrapper } from 'redux/store';
import { Spinner } from 'components/Spinner';
import { ROLE_ADMIN } from "utils/constants";
import { getUsers } from "services/api";
import { Pagination } from "components/Pagination";
import { Table } from "components/Table";
import { dateFormat } from "utils/format";
import { TableItem } from "components/Table/TableItem";
import { EditIcon } from "assets/img/icons";
import { Content } from "layout/Content";

import styles from './users.module.scss';
export interface UserType {
  id: string,
  firstName: string,
  lastName: string,
  email: string,
  avatar: string,
  phone: string,
  createdAt: string,
  countInventory: {
    all: number,
    published: number
  }
}

export type QueryType = {
  page?: number,
  sortby?: string,
  sortdir?: string
};

const Users: InferGetServerSidePropsType<GetServerSideProps> = ({ query }: any) => {
  const [loading, setLoading] = useState(false);
  const user = useSelector<AppState>(state => state.user);
  const [users, setUsers] = useState([]);
  const [sortBy, setSortBy] = useState(query.sortby);
  const [sortDir, setSortDir] = useState(query.sortdir);
  const [usersCount, setUsersCount] = useState(0);
  const router = useRouter();
  const [currentPage, setCurrentPage] = useState(query.page ? Number(query.page) : 1);
  const [pageSize, setPageSize] = useState(10);

  const getAndSetUsers = () => {
    getUsers({
      paginationCount: pageSize,
      paginationOffset: (currentPage - 1) * pageSize,
      sortBy,
      sortDir
    })
      .then(({ items, totalItems }: any) => {
        setUsers(items);
        setUsersCount(totalItems);
        if(setLoading) {
          setLoading(false);
        }
      })
      .catch(e => {
        console.error(e);
      });
  };

  useEffect(() => {
    const query: QueryType = {};

    if(currentPage !== 1) {
      query.page = currentPage;
    }

    if(sortBy) {
      query.sortby = sortBy;
      query.sortdir = sortDir;
    }

    if(query) {
      router.push({
        pathname: router.pathname,
        query
      });
    } else {
      router.push(router.pathname);
    }

    setLoading(true);
    getAndSetUsers();
  }, [currentPage, pageSize, sortBy, sortDir]);


  if(user.roles && !user.roles.find(i => i === ROLE_ADMIN)) {
    return (
      <h3>You do not have access on this page</h3>
    );
  }

  const onSort = (sortBy: string) => {
    setSortBy(sortBy);
    setSortDir(prev => prev === 'ASC' ? 'DESC' : 'ASC');
  };

  return (
    <Content>
      <div className={styles.users}>
        <div className={styles['block_top']}>
          <div className="block_right">
            <div>
              <h1>Users</h1>
              <p className="subtitle">Users list</p>
            </div>
          </div>
        </div>

        {loading && <Spinner overlay={true} />}

        <Table
          columns={[
            { title: 'Name', sortable: true, sortBy: 'lastName' },
            { title: 'Email' },
            { title: 'Phone' },
            { title: 'Date add' },
            { title: 'Inventory', subtitle: 'All / Published' },
            { title: '' }
          ]}
          columnsWidth={[18, 18, 14, 15, 18, 8, 7]}
          sortBy={sortBy}
          sortDir={sortDir}
          onSort={onSort}
        >

          {users!.map((item: UserType) =>
            <TableItem
              columnsWidth={[18, 18, 14, 15, 18, 8, 7]}
              key={item.id}
            >
              <Link href={`${location.pathname}/${item.id}`} passHref><a>{item.lastName} {item.firstName}</a></Link>
              {item.email}
              {item.phone}
              {dateFormat(item.createdAt)}
              <Link href={`${location.pathname}/${item.id}/inventory`} passHref>
                <a>
                  {item.countInventory.all} / {item.countInventory.published}
                </a>
              </Link>
              <Link href={`${location.pathname}/${item.id}`} passHref>
                <a className={styles['users__edit-button']}><EditIcon /></a>
              </Link>
            </TableItem>
          )}

        </Table>

        <Pagination
          totalCount={usersCount}
          currentPage={currentPage}
          pageSize={pageSize}
          setCurrentPage={setCurrentPage}
          onChangePageSize={e => setPageSize(Number(e.target.value))}
        />

      </div>
    </Content>
  );
};

export const getServerSideProps = wrapper.getServerSideProps(store => async ({ query }) => {
  return {
    props: { query }
  };
});


export default Users;