import { useState, useEffect } from 'react';
import { useRouter } from 'next/router';

import { getUsersOne } from 'services/api';
import { Tabs } from "components/Tabs";
import { GoBack } from "components/GoBack";
import { Spinner } from "components/Spinner";
import { UserSettings } from "features/Settings/UserSettings";
import { CompanySettings } from "features/Settings/CompanySettings";
import { LicenseSettings } from "features/Settings/LicenseSettings";
import { BillingSettings } from "features/Settings/BillingSettings";
import { ApiSettings } from "features/Settings/ApiSettings";
import { PasswordSettings } from "features/Settings/PasswordSettings";
import { Content } from "layout/Content";

const UsersOne = () => {
  const router = useRouter();

  const { id } = router.query;
  const [tab, setTab] = useState(0);
  const [user, setUser] = useState({});
  const [loading, setLoading] = useState(true);

  const getAndSetUser = () => {
    const unmounted = false;
    getUsersOne(id)
      .then((user) => {
        if(!unmounted) {
          setUser(user);
          setLoading(false);
        }
      })
      .catch(e => {
        console.error(e);
      });
  };

  useEffect(() => {
    if(!id) return false;
    getAndSetUser();
  }, [id]);


  return (
    <Content>
      <GoBack />
      <div className="block_top">
        <div className="block_right">
          <div>
            <h1>User: {user.firstName} {user.lastName}</h1>
          </div>
        </div>
      </div>

      {loading
        ? <Spinner />
        : <Tabs
          tabs={['User’s Info', 'Company Info', 'License Info', 'Billing', 'Api Metrc key', 'Password change']}
          onChangeTab={setTab}
          selectedTabId={tab}
        >
          <UserSettings user={user} />
          <CompanySettings user={user} />
          <LicenseSettings user={user} />
          <BillingSettings user={user} />
          <ApiSettings user={user} />
          <PasswordSettings user={user} />
        </Tabs>
      }

    </Content>
  );
};

export default UsersOne;