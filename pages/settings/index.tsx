import { useState, useEffect } from 'react';
import { GetServerSideProps, InferGetServerSidePropsType } from 'next';
import { useRouter } from 'next/router';
import { END } from 'redux-saga';
import { useDispatch, useSelector } from 'react-redux';

import { AppState, wrapper } from 'redux/store';
import { Spinner } from 'components/Spinner';
import { TextField } from 'components/TextField';
import { UserSettings } from 'features/Settings/UserSettings';
import { CompanySettings } from 'features/Settings/CompanySettings';
import { LicenseSettings } from 'features/Settings/LicenseSettings';
import { BillingSettings } from 'features/Settings/BillingSettings';
import { ApiSettings } from 'features/Settings/ApiSettings';
import { PasswordSettings } from 'features/Settings/PasswordSettings';
import { Tabs } from 'components/Tabs';
import { userGet } from 'redux/actions/userActions';

import styles from './settings.module.scss';
import { Content } from "layout/Content";

export interface UserType {
  firstName: string;
  lastName: string;
  email: string;
  avatar: string;
}

const Settings: InferGetServerSidePropsType<GetServerSideProps> = ({
  query,
}) => {
  const [loading, setLoading] = useState(false);
  const [tab, setTab] = useState(0);
  const dispatch = useDispatch();
  const user = useSelector<AppState>((state) => state.user);

  useEffect(() => {
    if(user.email) {
      setLoading(false);
    }
  }, [user]);

  return (
    <Content>
      <div className={styles.settings}>
        <div className={styles["block_top"]}>
          <div className="block_right">
            <div>
              <h1>Settings</h1>
              <p className="subtitle">Change your data</p>
            </div>
          </div>
        </div>
      </div>

      {loading ? (
        <Spinner />
      ) : (
        <Tabs
          tabs={[
            "User’s Info",
            "Company Info",
            "License Info",
            "Billing",
            "Api Metrc key",
            "Password change",
          ]}
          onChangeTab={setTab}
          selectedTabId={tab}
        >
          <UserSettings user={user} />
          <CompanySettings user={user} />
          <LicenseSettings user={user} />
          <BillingSettings user={user} />
          <ApiSettings user={user} />
          <PasswordSettings user={user} />
        </Tabs>
      )}
    </Content>
  );
};

export const getServerSideProps = wrapper.getServerSideProps(store => async ({ query }) => {

  if(!store.getState().user.id) {
    store.dispatch(userGet());
    store.dispatch(END);
  }

  await store.sagaTask.toPromise();

  return {
    props: { query }
  };
});

export default Settings;
