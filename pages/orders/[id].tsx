import { useState, useEffect } from "react";
import { useRouter } from "next/router";
import Link from "next/link";

import { getOrder } from "services/api";
import { Spinner } from "components/Spinner";
import { BreadCrumbs } from "components/BreadCrumbs";
import { Panel2 } from "components/Panel2";
import { GoBack } from "components/GoBack";
import { dateFormat, priceFormat } from "utils/format";
import { Status } from "features/Orders/Status";
import { Table } from "components/Table";
import { TableItem } from "components/Table/TableItem";
import { Panel } from "components/Panel";
import { Content } from "layout/Content";

import styles from "./orderOne.module.scss";

const OrderOne = () => {
  const router = useRouter();

  const { id } = router.query;

  const [inventory, setInventory] = useState({});
  const [loading, setLoading] = useState(true);

  const [invoice, setInvoice] = useState({});
  const [ordersInventory, setOrdersInventory] = useState([]);
  const [summ, setSumm] = useState(0);
  const [count, setCount] = useState(0);

  const breadcrumbs = [
    {
      title: "Invoices",
      link: "/invoice",
    },
    {
      title: `#${invoice.number} from ${dateFormat(invoice.createdAt)}`,
    },
  ];

  useEffect(() => {
    if(!id) return false;
    getAndSetInvoice();
  }, [id]);

  const getAndSetInvoice = () => {
    getOrder(id)
      .then((order) => {
        setInvoice(order);
        setOrdersInventory(order.ordersInventory);

        setCount(order.ordersInventory.reduce((acc, v) => acc + v.quantity, 0));
        setSumm(order.ordersInventory.reduce((acc, v) => acc + v.summ, 0));

        if(setLoading) {
          setLoading(false);
        }
      })
      .catch(e => {
        console.error(e);
      });
  };

  return (
    <Content>
      <div className={styles["invoice"]}>
        <BreadCrumbs breadcrumbs={breadcrumbs} />

        <GoBack />

        <div className="block_top">
          <div className="block_right">
            <div>
              <h1>
                Invoice #{invoice.number} from {dateFormat(invoice.createdAt)}
              </h1>
            </div>
          </div>
        </div>

        <Panel className={styles["invoice__information"]}>
          <ul>
            <li>Number: {invoice.number}</li>
            <li>Date: {dateFormat(invoice.createdAt)}</li>
            <li>Status: {<Status status={invoice.status} />}</li>
          </ul>
        </Panel>
        <Panel>
          <Table
            columns={[
              { title: "Product" },
              { title: "Price" },
              { title: "Quantity" },
              { title: "Summ" },
            ]}
            columnsWidth={[25, 8, 12, 15]}
          >
            <>
              {ordersInventory.map((item) => (
                <TableItem key={item.id} columnsWidth={[25, 8, 12, 15]}>
                  <Link href={`/marketplace/${item.inventoryId}`} passHref>
                    <a>{item.inventoryName}</a>
                  </Link>
                  <>$ {priceFormat(item.price)}</>
                  {item.quantity}
                  <>$ {priceFormat(item.summ)}</>
                </TableItem>
              ))}
            </>

            <TableItem columnsWidth={[25, 8, 12, 15]}>
              <>Total:</>
              <></>
              {count}
              <>$ {priceFormat(summ)}</>
            </TableItem>
          </Table>
        </Panel>
      </div>
    </Content>
  );
};

export default OrderOne;
