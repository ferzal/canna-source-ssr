import React, { useState, useEffect, ChangeEvent } from "react";
import { useRouter } from "next/router";
import Link from "next/link";

import { getOrders } from "services/api";
import { Spinner } from "components/Spinner";
import { Pagination } from "components/Pagination";
import { TableItem } from "components/Table/TableItem";
import { Tabs } from "components/Tabs";
import { Table } from "components/Table";
import { dateFormat, priceFormat } from "utils/format";
import { Status } from "features/Orders/Status";
import { Content } from "layout/Content";

import styles from "./orders.module.scss";

const OrdersPage = ({ query }) => {
  const [orders, setOrders] = useState([]);
  const [ordersCount, setOrdersCount] = useState(0);
  const [currentPage, setCurrentPage] = useState(
    query.page ? Number(query.page) : 1
  );
  const [pageSize, setPageSize] = useState(10);
  const [loading, setLoading] = useState(false);
  const [tab, setTab] = useState(0);
  const router = useRouter();

  const getAndSetOrders = () => {
    setLoading(true);
    getOrders({
      paginationCount: pageSize,
      paginationOffset: (currentPage - 1) * pageSize,
      purchases: (tab === 0),
      sales: (tab === 1)
    })
      .then(({ items, totalItems }: any) => {
        setOrders(items);
        setOrdersCount(totalItems);
      })
      .catch((e: any) => {
        console.error(e);
      })
      .finally(() => setLoading(false));
  };

  useEffect(() => {
    const url = new URLSearchParams();
    if(currentPage !== 1) {
      url.append("page", String(currentPage));
    }
    router.push(`${router.pathname}?${url}`);
    getAndSetOrders();
  }, [currentPage, pageSize]);

  useEffect(() => {
    getAndSetOrders();
  }, [tab]);

  return (
    <Content>
      <div className={styles.invoices}>
        <div className={styles.block_top}>
          <div className={styles.block_right}>
            <div>
              <h1>Orders</h1>
              <p className={styles.subtitle}></p>
            </div>
          </div>
        </div>

        {loading && <Spinner overlay={true} />}

        <Tabs
          tabs={['Purchases', 'Sales']}
          onChangeTab={setTab}
          selectedTabId={tab}
        >
          <>
            <Table
              columns={[
                { title: "Number" },
                { title: "Date" },
                { title: tab === 0 ? "Seller" : "Buyer" },
                { title: "Count" },
                { title: "Summ" },
                { title: "Status" },
              ]}
              columnsWidth={[15, 15, 15, 14, 15, 18]}
            >
              {orders!.map((item: any) => (
                <TableItem columnsWidth={[15, 15, 15, 14, 15, 18]} key={item.id}>
                  <Link href={`${location.pathname}/${item.id}`} passHref>
                    <a>{item.number}</a>
                  </Link>

                  {dateFormat(item.createdAt)}

                  {item.seller}

                  {item.quantity}

                  <>$ {priceFormat(item.summ)}</>

                  <Status status={item.status} />
                </TableItem>
              ))}
            </Table>

            <Pagination
              totalCount={ordersCount}
              currentPage={currentPage}
              pageSize={pageSize}
              setCurrentPage={setCurrentPage}
              onChangePageSize={(e: ChangeEvent<HTMLInputElement>) => setPageSize(Number(e.target.value))}
            />
          </>
        </Tabs>

      </div>
    </Content>
  );
};

export const getServerSideProps = async ({ query }) => {
  return {
    props: { query },
  };
};

export default OrdersPage;
