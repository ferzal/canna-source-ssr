import { FC, useState, ChangeEvent } from 'react';
import RModal from 'react-modal';

import { CloseModal } from "components/CloseModal";

export const useModal = () => {

  const [isOpened, setIsOpened] = useState(false);

  const customStyles = {
    overlay: {
      background: 'rgba(0,0,0,.5)'
    },
    content: {
      top: '50%',
      left: '50%',
      right: 'auto',
      bottom: 'auto',
      marginRight: '-50%',
      transform: 'translate(-50%, -50%)',
      padding: '50px',
      background: '#171722'
    },
  };

  const Modal: FC = ({ children }) =>
    <RModal
      isOpen={isOpened}
      onRequestClose={() => setIsOpened(false)}
      ariaHideApp={false}
      style={customStyles}
    >
      {children}
      <CloseModal onClick={() => setIsOpened(false)} />
    </RModal>;

  return [
    setIsOpened,
    Modal
  ];
};