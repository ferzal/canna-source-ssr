function classes(...items) {
  return items
    .filter(Boolean)
    .join(' ');
}

export default classes;
