import { useState } from 'react';

export const useLocalStorage = <T>(key: string, initialValue: T) => {

  const [storedValue, setStoredValue] = useState<T>(() => {
    try {
      const item = window.localStorage.getItem(key);
      return item ? JSON.parse(item) : initialValue;
    } catch(error) {
      console.error(error);
      return initialValue;
    }
  });
  
  const setValue = <T>(value: T) => {
    try {
      window.localStorage.setItem(key, JSON.stringify(value));
    } catch(error) {
      console.error(error);
    }
  };
  return [storedValue, setValue] as const;
};

