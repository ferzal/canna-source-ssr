const format = (unit: number) => {
  return `${unit < 10 ? `0${unit}` : unit}`;
};

export const getMessageTime = (date: string) => {
  const newDate = new Date(date);
  const hours = newDate.getHours();
  const minutes = newDate.getMinutes();
  return `${format(hours)}:${format(minutes)}`;
};
