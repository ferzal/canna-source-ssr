export const fileSizeType = (bytes: number) => {
  if(Math.abs(bytes) < 1024) {
    return bytes + ' B';
  }
  const units = ['kB', 'MB', 'GB'];
  let u = -1;
  do {
    bytes /= 1024;
    ++u;
  } while(Math.abs(bytes) >= 1024 && u < units.length - 1);
  return bytes.toFixed(1) + ' ' + units[u];
};