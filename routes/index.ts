import {
  HomeIcon,
  InventoryIcon,
  MarketplaceIcon,
  SettingsIcon,
  InvoicesIcon,
  MessagesIcon
} from 'assets/img/icons';

const homeRoutes = {
  href: "/",
  name: "Home",
  icon: HomeIcon
};

const inventoryRoutes = {
  href: "/inventory",
  name: "Inventory",
  icon: InventoryIcon
};

const marketplaceRoutes = {
  href: "/marketplace",
  name: "Marketplace",
  icon: MarketplaceIcon
};

const settingsRoutes = {
  href: "/settings",
  name: "Settings",
  icon: SettingsIcon
};

const usersRoutes = {
  href: "/users",
  name: "Users",
  icon: SettingsIcon
};

const ordersRoutes = {
  href: "/orders",
  name: "Orders",
  icon: InvoicesIcon
};

const messagesRoutes = {
  href: "/messages",
  name: "Messages",
  icon: MessagesIcon
};

const sellersRoutes = {
  href: "/sellers",
  name: "Sellers",
  icon: MessagesIcon
};

export const routes = [
  homeRoutes,
  usersRoutes,
  inventoryRoutes,
  sellersRoutes,
  marketplaceRoutes,
  messagesRoutes,
  ordersRoutes,
  settingsRoutes
];