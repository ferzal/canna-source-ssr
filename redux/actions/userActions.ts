import * as types from "../constants";

export function userPut(user) {
  return {
    type: types.USER_PUT,
    payload: user
  };
}

export function userGet(router) {
  return {
    type: types.USER_GET,
    payload: { router }
  };
}

export function userLogin(props, router) {
  return {
    type: types.USER_LOGIN,
    payload: { props, router }
  };
}

export function userErrorAuth(props) {
  return {
    type: types.USER_ERROR_AUTH,
    props
  };
}