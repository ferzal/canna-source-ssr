import * as types from "../constants";

export function messagesSetChatId(chatId: string) {
  return {
    type: types.MESSAGES_SET_CHAT_ID,
    payload: chatId
  };
}