// User
export const USER_PUT = "USER_PUT";
export const USER_GET = "USER_GET";
export const USER_LOGIN = "USER_LOGIN";
export const USER_IS_AUTH = "USER_IS_AUTH";
export const USER_ERROR_AUTH = "USER_ERROR_AUTH";

// Card
export const CART_LOADING = "CART_LOADING";
export const CART_PUT = "CART_PUT";
export const CART_GET = "CART_GET";
export const CART_ADD = "CART_ADD";
export const CART_CHANGE = "CART_CHANGE";
export const CART_REMOVE = "CART_REMOVE";
export const CART_CLEAN = "CART_CLEAN";

// Messages
export const MESSAGES_SET_CHAT_ID = "MESSAGES_SET_CHAT_ID";
