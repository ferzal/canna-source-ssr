import { HYDRATE } from "next-redux-wrapper";
import * as types from "../constants";

const initialState = {
  cartLoading: true,
  items: [],
  totalQuantity: 0,
  totalCost: 0
};

export default function reducer(state = initialState, actions) {
  switch(actions.type) {
    case HYDRATE: {
      return { ...state, ...actions.payload.cart };
    }
    case types.CART_LOADING:
      return { ...state, ...{ cartLoading: actions.payload } };
    case types.CART_PUT:
      return {
        ...state,
        ...actions.payload
      };
    case types.CART_CHANGE:
      return {
        ...state, items: state.items.map(item => {
          if(item.id === actions.payload.id) {
            item.quantity = actions.payload.quantity;
          }
          return item;
        })
      };
    default:
      return state;
  }
}
