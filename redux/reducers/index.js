import { combineReducers } from "redux";

import user from "./userReducer";
import cart from "./cartReducer";
import messages from "./messagesReducer";

// import { reducer as toastr } from "react-redux-toastr";

export default combineReducers({
  user,
  cart,
  messages
});
