import * as types from "../constants";
import { HYDRATE } from 'next-redux-wrapper';

const initialState = {
  chatId: null,
};

export default function reducer(state = initialState, actions: { type: string, payload: string }) {
  switch(actions.type) {
    case HYDRATE: {
      return { ...state, chatId: actions.payload };
    }
    case types.MESSAGES_SET_CHAT_ID:
      return { ...state, chatId: actions.payload };
    default:
      return state;
  }
}
