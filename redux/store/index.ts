import { createStore, applyMiddleware } from "redux";
import { createWrapper } from 'next-redux-wrapper';
import createSagaMiddleware from 'redux-saga';
import { composeWithDevTools } from 'redux-devtools-extension';

import rootReducer from "../reducers/index";
import { rootSaga } from '../store/sagas';

export const makeStore = (context) => {
    const saga = createSagaMiddleware();
    const store = createStore(
        rootReducer,
        composeWithDevTools(
            applyMiddleware(
                saga
            )
        )
    );

    store.sagaTask = saga.run(rootSaga);

    return store;
};

//export default store;

export const wrapper = createWrapper(makeStore, { debug: true });

export type AppState = ReturnType<typeof makeStore.getState>;