// @ts-nocheck

import { all, call, put, takeLatest } from 'redux-saga/effects';

import { getCurrentUser, login, getCart, addToCart, changeCart, removeFromCart, cleanCart } from 'services/api';

import { userPut } from '../actions/userActions';
import { cartLoading, cartPut } from '../actions/cartActions';

import * as types from '../constants';

function* loginWorker({ payload }) {
  try {
    const { router, props } = payload;
    const res = yield call(login, props);
    if(res != 401) {
      localStorage.setItem('access_token', res.access_token);
      localStorage.setItem('refresh_token', res.refresh_token);
      yield put({
        type: types.USER_PUT,
        payload: res.user
      });
      yield put({
        type: types.USER_IS_AUTH,
        payload: true
      });
    } else {
      router.push('/sign-in');
      yield put({
        type: types.USER_ERROR_AUTH,
        payload: true
      });
    }
  } catch(e) {
    console.error(e);
  }
}

function* userWorker({ payload }) {
  try {
    const { router } = payload;
    if(!localStorage.getItem('access_token')) {
      router.push('/sign-in');
    }
    const res = yield call(getCurrentUser);
    if(res === 401) {
      router.push('/sign-in');
    }
    yield put(userPut(res));
  } catch(err) {
    console.error(err);
  }
}

function* cartWorker() {
  try {
    const res = yield call(getCart);
    yield put(cartPut(res));
    yield put(cartLoading(false));
  } catch(err) {
    console.error(err);
  }
}

function* cartAddWorker({ payload }) {
  try {
    const res = yield call(addToCart, payload);
    yield put(cartPut(res));
  } catch(e) {
    console.error(e);
  }
}

function* cartChangeWorker({ payload }) {
  yield put(cartLoading(true));
  try {
    const res = yield call(changeCart, payload);
    yield put(cartPut(res));
    yield put(cartLoading(false));
  } catch(e) {
    console.error(e);
  }
}

function* cartRemoveWorker({ payload }) {
  yield put(cartLoading(true));
  try {
    const res = yield call(removeFromCart, payload);
    yield put(cartPut(res));
    yield put(cartLoading(false));
  } catch(e) {
    console.error(e);
  }
}

function* cartCleanWorker() {
  yield put(cartLoading(true));
  try {
    const res = yield call(cleanCart);
    yield put(cartPut(res));
    yield put(cartLoading(false));
  } catch(e) {
    console.error(e);
  }
}

export function* rootSaga() {
  yield all([
    takeLatest(types.USER_LOGIN, loginWorker),
    takeLatest(types.USER_GET, userWorker),
    takeLatest(types.CART_GET, cartWorker),
    takeLatest(types.CART_ADD, cartAddWorker),
    takeLatest(types.CART_CHANGE, cartChangeWorker),
    takeLatest(types.CART_REMOVE, cartRemoveWorker),
    takeLatest(types.CART_CLEAN, cartCleanWorker)
  ]);
}
