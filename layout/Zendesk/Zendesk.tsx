import { FC } from 'react';
import { useSelector } from 'react-redux';
import ZendeskApp, { ZendeskAPI as zendeskAPI } from 'react-zendesk';
import { AppState } from 'redux/store';

export const Zendesk: FC = () => {

  const user = useSelector<AppState>(state => state.user);

  const SETTINGS = {
    color: {
      theme: "#232334"
    },
    launcher: {
      chatLabel: {
        "en-US": "Need Help"
      }
    },
    contactForm: {
      fields: [
        {
          id: "name",
          prefill: { "*": `${user.firstName} ${user.lastName}` }
        },
        {
          id: "email",
          prefill: { "*": user.email }
        },
        {
          id: "description",
          prefill: { "*": "" }
        }
      ]
    },
    chat: {
      departments: {
        enabled: ['Finance', 'Support'],
        select: 'Support'
      }
    }
  };

  if(!user.email) return null;

  return <ZendeskApp
    defer
    zendeskKey={process.env.NEXT_PUBLIC_ZENDESK_KEY}
    {...SETTINGS}
    onLoaded={() => {
      zendeskAPI("webWidget", "identify", {
        name: `${user.firstName} ${user.lastName}`,
        email: user.email,
        organization: user.complanyName
      });
      zendeskAPI("webWidget", "setLocale", "en");
    }}
  />;
};
