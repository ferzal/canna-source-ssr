import { FC } from "react";
import classNames, { Argument } from "classnames";
import { useRouter } from "next/router";


import { Wrapper } from "layout/Wrapper";
import { Sidebar } from "layout/Sidebar";
import { Navbar } from "layout/Navbar";
import { RightSidebar } from "layout/RightSidebar";
import { Zendesk } from "layout/Zendesk";
import { useLocalStorage } from "utils/useLocalStorage";

import styles from "./dashboard.module.scss";


interface DashboardProps {
  path: string;
}

export const Dashboard: FC<DashboardProps> = ({ children, path }) => {
  const router = useRouter();

  const [notShowPlans, setNotShowPlans] = useLocalStorage(
    "notShowPlans",
    false
  );

  if(!notShowPlans) {
    typeof window !== "undefined" && router.push("/select-plan");
  }

  return (
    <Wrapper>
      <Sidebar />
      <div className={styles.main}>
        <Navbar />
        {children}
      </div>
      {path == "/" && <RightSidebar />}
      <Zendesk />
    </Wrapper>
  );
};
