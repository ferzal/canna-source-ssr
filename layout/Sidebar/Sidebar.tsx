import { Fragment, FC, MouseEvent, useState } from "react";
import { useSelector } from "react-redux";
import { useRouter } from "next/router";
import { AppState } from "redux/store";
import Link from "next/link";
import Image from "next/image";
import classNames from "classnames";

import { routes } from "routes";
import { ROLE_ADMIN } from "utils/constants";
import logo from "assets/img/logo/logotype.png";
import { useBreakpoint } from "utils/useBreakpoint";
import Union from "assets/img/icons/Union.svg";
import Burger from "assets/img/icons/Burger.svg";

import styles from "./sidebar.module.scss";
import { TextField } from "components/TextField";
import { SearchIcon } from "assets/img/icons";

export interface MenuItemProps {
  name: string;
  badgeColor?: "red" | "green";
  badgeText?: string;
  icon: FC;
  href: string;
  indicator?: any;
  access?: string;
  roles?: string[];
}

const MenuItem: FC<MenuItemProps> = ({
  name,
  badgeColor,
  badgeText,
  icon: Icon,
  href,
  indicator,
  access,
  roles,
}) => {
  const router = useRouter();

  if (access) {
    const roleAdmin = roles!.find((r: string) => r === ROLE_ADMIN);
    if (roleAdmin != access) return null;
  }

  const notHomePageAndActive = href !== "/" && router.pathname.startsWith(href);

  const homePageAndActive = router.pathname === "/" && href === "/";

  return (
    <li
      className={classNames(styles["sidebar-item"], {
        [styles["active"]]: notHomePageAndActive || homePageAndActive,
      })}
    >
      <Link href={href} passHref>
        <a className={styles["sidebar-link"]}>
          {indicator && (
            <span className={styles["icon-wrapper-indicator"]}>
              <span className={styles["indicator"]} />
              {Icon && <Icon />}
            </span>
          )}
          {!indicator && Icon && <Icon />}
          {name}
          {badgeColor && badgeText && (
            <span
              className={classNames(styles["sidebar-badge"], {
                [styles[`sidebar-badge-${badgeColor}`]]: badgeColor,
              })}
            >
              {badgeText}
            </span>
          )}
        </a>
      </Link>
    </li>
  );
};

export const Sidebar: FC = () => {
  const { isDesktop } = useBreakpoint();
  const [isDashboardOpened, setDashboardOpened] = useState<boolean>(false);
  const user = useSelector<AppState>((state) => state.user);

  const toggleDashboard = () => setDashboardOpened(!isDashboardOpened);

  const renderDashboardButton = () => {
    if (isDesktop) return;

    return (
      <div className={styles.button} onClick={toggleDashboard}>
        {isDashboardOpened ? <Union /> : <Burger />}
      </div>
    );
  };

  const renderSidebarBottom = () => {
    if (!isDesktop)
      return (
        <TextField id="search" className={styles.search} Icon={SearchIcon} />
      );

    return (
      <>
        <Link href="/faq" passHref>
          <a className={styles["sidebar-bottom-item"]}>Help</a>
        </Link>
        <Link href="/tickets" passHref>
          <a className={styles["sidebar-bottom-item"]}>Support tickets</a>
        </Link>
      </>
    );
  };

  return (
    <>
      <nav className={styles.sidebar}>
        <div
          className={classNames(styles["sidebar-content"], {
            [styles.opened]: isDashboardOpened,
          })}
        >
          <Link href="/" passHref>
            <a className={styles["sidebar-brand"]}>
              <Image
                className={styles["sidebar-brand-img"]}
                src={logo.src}
                alt="Canna Source Direct"
                width={154}
                height={64}
              />
            </a>
          </Link>
          <ul className={styles["sidebar-nav"]}>
            {routes!.map((category: any, index: number) => (
              <Fragment key={index}>
                <MenuItem
                  name={category.name}
                  href={category.href}
                  icon={category.icon}
                  badgeColor={category.badgeColor}
                  badgeText={category.badgeText}
                  indicator={category.indicator}
                  access={category.access}
                  roles={user.roles}
                />
              </Fragment>
            ))}
          </ul>

          <div className={styles["sidebar-bottom"]}>
            {renderSidebarBottom()}
          </div>
        </div>
      </nav>
      {renderDashboardButton()}
    </>
  );
};
