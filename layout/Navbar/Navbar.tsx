import { FC } from 'react';
import { useSelector } from 'react-redux';
import Link from 'next/link';
import { useRouter } from 'next/router';

import classes from 'utils/classNames';
import { AppState } from 'redux/store';

import styles from './navbar.module.scss';
import CartIcon from './Cart.svg';

export const Navbar: FC = () => {
  const router = useRouter();
  const user = useSelector<AppState>(state => state.user);
  const totalQuantity = useSelector<AppState>(state => state.cart!.totalQuantity);

  const logoutHandler = () => {
    localStorage.removeItem('access_token');
    localStorage.removeItem('refresh_token');
    router.push('/sign-in');
  };

  const renderAvatar = () => {
    if(!user.avatar) return <div className={styles['navbar-user_icon-dummy']} />;

    return <img src={user.avatar} alt={user.firstName + ' ' + user.lastName} title={user.firstName + ' ' + user.lastName} />;
  };

  return (
    <nav className={classes(
      styles.navbar,
      styles['navbar-expand']
    )}
    >
      <div className={styles['navbar-content']}>
        <ul className={styles['navbar-menu__block']}>

          <li className={styles['navbar-menu__item']}>
            <Link href="/cart" passHref>
              <a>
                <span className={styles['navbar-menu__cart-indicator']}>{totalQuantity}</span>
                <CartIcon />
              </a>
            </Link>
          </li>
        </ul>
        <div className={styles['navbar-user__block']}>
          <div className={styles['navbar-user__icon']}>
            {renderAvatar()}
          </div>
          <div className={styles['navbar-user__dropdown']}>
            <ul>
              <li onClick={logoutHandler}>Logout</li>
            </ul>
          </div>
        </div>
      </div>
    </nav>
  );
};
