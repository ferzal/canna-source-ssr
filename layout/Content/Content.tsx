import { FC  } from "react";

import styles from './content.module.scss';

export const Content: FC = ({ children }) => <main className={styles.content}>{children}</main>;