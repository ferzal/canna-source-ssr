import { FC } from 'react';

import styles from './report.module.scss';
import { MoneyIcon, BoxIcon, ChartIcon } from './icons';

interface ReportProps {
  icon: FC;
  data: string;
  measure: string;
  description: string;
}

const REPORT_ITEMS = [
  {
    icon: MoneyIcon,
    data: '0.00',
    measure: 'USD',
    description: 'Total revenue generated'
  },
  {
    icon: BoxIcon,
    data: '0',
    measure: 'No',
    description: 'Total units sold'
  },
  {
    icon: ChartIcon,
    data: '0.00',
    measure: '',
    description: '% increase in sales'
  }
];

const ReportItem: FC<ReportProps> = ({ icon: Icon, data, measure, description }) => (
  <div className={styles['report__item']}>
    <div className={styles['report__icon']}>
      <Icon />
    </div>
    <div className={styles['report__data']}>
      <div className={styles['report__data-title']}>
        {data}{' '}
        <span>{measure}</span>
      </div>
      <div className={styles['report__data-desc']}>
        {description}
      </div>
    </div>
  </div>
);

const reportItems = REPORT_ITEMS.map((item, index) => <ReportItem {...item} key={index} />);

export const Report: FC = () => <div className={styles.report}>
  <div className={styles['report__period']}>
    <input type="radio" name="period" id="monthly" defaultChecked />
    <label htmlFor="monthly" className={styles['report__period-button']}>Monthly</label>
    <input type="radio" name="period" id="yearly" />
    <label htmlFor="yearly" className={styles['report__period-button']}>Yearly</label>
  </div>
  <div className={styles['report__block']}>
    {reportItems}
  </div>
</div>;
