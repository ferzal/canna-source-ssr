import { FC, useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import classNames from 'classnames';

import { ArrowLeft2Icon } from 'assets/img/icons';
import { CircleButton } from 'components/CircleButton';
import { AppState } from 'redux/store';
import { useLocalStorage } from "utils/useLocalStorage";

import { Report } from './Report';
import styles from './rightSidebar.module.scss';

export const RightSidebar: FC = () => {
  const [isCollapsed, setCollapsed] = useState(false);
  const user = useSelector<AppState>(state => state.user);

  const [isCollapsedLocal, setCollapsedLocal] = useLocalStorage('isCollapsed', false);

  useEffect(() => {
    setCollapsed(isCollapsedLocal);
  }, []);

  const collapseSidebar = () => {
    setCollapsed(prev => {
      setCollapsedLocal(!prev);
      return !prev;
    });
  };

  return (
    <div className={classNames(
      styles['right-sidebar'],
      { [styles.hidden]: isCollapsed }
    )}
    >
      <div className={styles['right-sidebar__wrapper']}>
        <div className={styles['right-sidebar__collapse-button']}>
          <CircleButton
            onClick={collapseSidebar}
            icon={ArrowLeft2Icon}
            className={styles['circle-button__icon']}
          >
          </CircleButton>
        </div>
        <div className={styles['right-sidebar__user']}>
          <div className={styles['right-sidebar__avatar-circle']}>
            {user.avatar && <img src={user.avatar} alt="" />}
            {!user.avatar && <div className={styles['right-sidebar__avatar-circle-dummy']} />}
          </div>
          <div className={styles['right-sidebar__user-name']}>
            {user.firstName} {user.lastName}
          </div>
          <div className={styles['right-sidebar__user-id']}>
            {user.email}
          </div>
          <div className={styles['right-sidebar__user-balance']}>
            0,00
            <span><span className={styles['right-sidebar__user-usd']} /> USD</span>
          </div>
          <Report />
        </div>
      </div>
    </div>
  );
};