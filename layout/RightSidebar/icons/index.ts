export { default as MoneyIcon } from './Money.svg';
export { default as BoxIcon } from './Box.svg';
export { default as ChartIcon } from './Chart.svg';