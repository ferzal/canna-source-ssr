import { FC } from 'react';

import styles from './wrapper.module.scss';

export const Wrapper: FC = ({ children }) =>
  <div className={styles.wrapper}>
    {children}
  </div>;
