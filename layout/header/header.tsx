import { FC } from 'react';
import Link from "next/link";
import Image from "next/image";

import Logo from 'assets/img/logo/logotype.svg';

import styles from './header.module.scss';

export interface HeaderProps {
  page: 'signin' | 'signup'
}

export const Header: FC<HeaderProps> = ({ page }) => {
  return (
    <div className={styles.header}>
      <Logo alt="Canna Source Direct" />
      <div className={styles.header__nav}>
        {page == 'signin' &&
          <>
            Don’t have an account? -{' '}
            <Link href="/sign-up" passHref>Register here</Link>
          </>
        }
        {page == 'signup' &&
          <>
            Already have an account? -{' '}
            <Link href="/sign-in" passHref>Login</Link>
          </>
        }
      </div>
    </div>
  );
};
