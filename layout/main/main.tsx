import { FC } from "react";
import classNames, { Argument } from "classnames";

import styles from './main.module.scss';

export interface MainProps {
  className?: Argument;
}

export const Main: FC<MainProps> = ({ className, children }) => (
  <div className={classNames(styles.main, className)}>{children}</div>
);
